:- module(alpino_cg, [active_edges/0,
		      inactive_edges/0,
		      update_inactive_edges/0
		     ]).

:- expects_dialect(sicstus).

:- use_module(library(lists)).
:- use_module(hdrug(hdrug_util)).
:- use_module(alpino('src/utils')).

:- dynamic active_edge/7, inactive_edge/3, inactive_edge_frozen/5,
           inactive_edge_ref/2, his/2.
:- dynamic lex/6.          % also used in genlex.pl src/filter_tags.pl
:- dynamic unpack_memo/2.

:- thread_local active_edge/7, inactive_edge/3, inactive_edge_frozen/5,
           inactive_edge_ref/2, his/2.
:- thread_local lex/6.          % also used in genlex.pl src/filter_tags.pl
:- thread_local unpack_memo/2.

:- use_module( [ genlex,
		 bitcode,
		 'fluency/pro_lm',
		 'fluency/maxent'
%%		 optrel   % currently not used!
		 ]).

%%%
%%% FLUENCY
%%%

%% is now called from start_hook in src/start.pl; otherwise cputime of
%% first sentence is too high
%%
%% ...and now it has even moved to the hdrug_initialization hook to get
%% everything loaded correctly for static compilation
:- public load_fluency/0.

load_fluency :-
    hdrug_flag(use_fluency_model,UseFluency),
    hdrug_flag(end_hook,Th),
    load_fluency(UseFluency,Th).

load_fluency(UseFluency,Th) :-
    (   ( UseFluency == on ; Th = train_fluency )
    ->  load_maxent_fluency_weights
    ;   true
    ).

load_maxent_fluency_weights :-
    hdrug_flag(fluency_feature_weights,File),
    (   File==undefined
    ->  alpino_util:inform_undefined_module(fluency_feature_weights,alpino_fluency_weights)
    ;   overrule_module(File)
    ).

extract_and_score_sentences(Fs,BitcodeAll,Obj,Str,Sem,UseFluency,SortedResults) :-
    findall(P-o(Obj,Str,Sem),
            extract_and_score_sentence(Fs,BitcodeAll,o(Obj,Str,Sem),P,UseFluency),
            KeyedResults),
    (	UseFluency == on
    ->	keysort(KeyedResults,SortedResults)
    ;	SortedResults = KeyedResults
    ).
        
extract_and_score_sentence(Fs,BitcodeAll,o(Obj,Str,_Sem),P,UseFluency) :-
    generated_sentence(Str,P,Features,DerivTree,Fs,BitcodeAll,UseFluency),
    alpino_data:robust_list_to_cat([Fs],Cat),
    frames_of_deriv(DerivTree,Frames),
    alpino_data:result_term(p(P,Features),Str,Cat,DerivTree,Frames,Obj).

%%%
%%% GENERATE
%%% 
%%% Hdrug hook

:- public generate/1.
generate(o(Obj,Str,Sem)) :-
    if(generate_one(Obj,Str,Sem),
       true,
       generate_parts(Obj,Str,Sem)
      ).

generate_one(Obj,Str,Sem) :-
    hdrug_flag(use_fluency_model,UseFluency),
    prepare_adt_and_lex(Sem,Fs,BitcodeAll),
    count_edges(lex(_,_,_,_,_,_),Edges),
    debug_message(1,"~w lexical frames for generation~n",[Edges]),
    clean_edges,
    bb_put(edge_count,0),
    bb_put(his_count,0),
    time(1,chart_initialization(Agenda)),
    length(Agenda,Len),
    debug_message(1,"~w lexical attribute value structures for generation~n",[Len]),
    time(1,process_agenda(Agenda)),
    time(1,extract_and_score_sentences(Fs,BitcodeAll,Obj,Str,Sem,UseFluency,SortedResults)),
    member(_-o(Obj,Str,Sem),SortedResults).

generate_parts(Obj,Str,Sem) :-
    format(user_error,"No full generation; attempting to generate parts...~n",[]),
    findall(Part,get_part(Sem,Part),Parts),
    generate_list(Parts,Results),
    findall(Sc-o(Obj,Str), combine_results(Results,Sc,Obj,Str),List),
    keysort(List,List1),
    member(_-o(Obj,Str),List1).

get_part(tree(r(top,p(top)),Ds),tree(r(top,p(top)),[tree(r('--',Cat),XDs)])) :-
    member(tree(r('--',Cat0),DuDs),Ds),
    get_sub_part(DuDs,Cat0,Cat,XDs).

get_sub_part([H|T],_,Cat,XDs) :-
    member(tree(r(dp,Cat),XDs),[H|T]).
get_sub_part(Ds,Cat,Cat,Ds) :-
    \+ member(tree(r(dp,Cat),_),Ds).

generate_list([],[]).
generate_list([Sem|Sems],[Objs|Results]) :-
    findall(Obj,generate_one(Obj,_,Sem),Objs),
    generate_list(Sems,Results).

combine_results(Results,Sc,Obj,Str) :-
    categories(Results,Scs,Strs,Cats,Trees,Frames),
    alpino_data:robust_list_to_cat(Cats,Cat),
    append_all(Strs,Str),
    append_all(Frames,Frame),
    sum_all(Scs,Sc),
    alpino_data:result_term(Sc,Str,Cat,tree(robust,robust,Trees,_),Frame,Obj).

sum_all([p(Sc0,His0)|Tail],p(Sc,[His0|His])) :-
    sum_all(Tail,Sc0,Sc,His).

sum_all([],Sc,Sc,[]).
sum_all([p(Sc0,His0)|Tail],Sc1,Sc,[His0|His]) :-
    Sc2 is Sc0 + Sc1,
    sum_all(Tail,Sc2,Sc,His).

append_all([],[]).
append_all([H|T],S) :-
    append_all(T,H,S).

append_all([],S,S).
append_all([H|T],S0,S) :-
    append(S0,H,S1),
    append_all(T,S1,S).

categories([],[],[],[],[],[]).
categories([Objs|Objss],Scs,Strs,Cats,Trees,Frs) :-
    Objs == [], !,
    categories(Objss,Scs,Strs,Cats,Trees,Frs).
categories([Objs|Objss],[Sc|Scs],[Str|Strs],[Cat|Cats],[Tree|Trees],[Fr|Frs]) :-
    member(Obj,Objs),
    alpino_data:result_term(Sc,Str,Cat0,Tree,Fr,Obj),
    Cat0 = robust([Cat]),
    categories(Objss,Scs,Strs,Cats,Trees,Frs).

%% Hdrug hook
:- public count/0, count/1, clean/0.
count :-
    print_edge_counts.

%% Hdrug hook
count(N) :-
    bb_get(edge_count,N).

active_edge_count(Count) :-
    findall(Id,active_edge(_,_,_,Id,_,_,_),Ids),
    length(Ids,Count).

inactive_edge_count(Count) :-
    findall(Id,inactive_edge(_,_,Id),Ids),
    length(Ids,Count).

print_edge_counts :-
    active_edge_count(ActiveCount),
    format(user_error,"  Active edges: ~w~n",[ActiveCount]),
    inactive_edge_count(InActiveCount),
    format(user_error,"Inactive edges: ~w~n",[InActiveCount]),
    Total is ActiveCount + InActiveCount,
    format(user_error,"         total: ~w~n",[Total]).

%% hdrug hook
clean :-
    clean_edges,
    alpino_adt:clean,
    retractall(lex(_,_,_,_,_,_)).

clean_edges :-
    bb_put(hdrug_gen_sym:generator_values,0),
    retractall(inactive_edge_ref(_,_)),
    retractall(inactive_edge(_,_,_)),
    retractall(inactive_edge_frozen(_,_,_,_,_)),
    retractall(active_edge(_,_,_,_,_,_,_)),
    retractall(his(_,_)).

prepare_adt_and_lex(Adt,Fs,BcAll) :-
    alpino_adt:combine_mwu(Adt,CombinedAdt),
    time(1,alpino_adt:bitcode_lookup_frames_adt(CombinedAdt,BcAdt,0,NBits)),
    give_bits(NBits,BcAll),
    time(1,alpino_adt:adt_to_fs(BcAdt,FsAdt,Fs)),
    alpino_genlex:lex_lookup(FsAdt).

%%%%%%%%%%%%%%%%%%
% Main generator %
%%%%%%%%%%%%%%%%%%

process_agenda([]).
process_agenda([Edge|Edges]) :-
    (   terms:cyclic_term(Edge)
    ->  Edges = NewEdges
    ;   next_step(Edge,Edges,NewEdges)
    ),
    process_agenda(NewEdges).

%% Process the next edge:
%%
%% - Pack the edge if possible, otherwise
%% - find interactions with the chart and grammar rules.
next_step(Edge0,Edges,NewEdges) :-
    freeze_edge(Edge0,EdgeFrozen),
    (   pack_edge(Edge0,EdgeFrozen)
    ->  Edges=NewEdges
    ;   number_inactive_edge(Edge0,Edge1),
        inactive_edge_history(Edge1,Edge),
        findall(NewEdge,gen_edge(Edge,NewEdge),NewEdges,Edges),
        put_on_chart(EdgeFrozen,Edge)
    ).

number_inactive_edge(inactive_edge(SynSem,Bitcode,Ds),
		     inactive_edge(SynSem,Bitcode,Ds,Count)) :-
    !,
    bb_get(edge_count,Count),
    NewCount is Count + 1,
    bb_put(edge_count,NewCount).
number_inactive_edge(E,E).

inactive_edge_history(inactive_edge(SynSem,Bitcode,His,Count),
		      inactive_edge(SynSem,Bitcode,Count)) :-
    !,
    assert_history(Count,His).
inactive_edge_history(E,E).

gen_edge(inactive_edge(Cat,Bc,N),NewEdge) :-
    rule_invocation(inactive_edge(Cat,Bc,N),NewEdge).
gen_edge(Edge,NewEdge) :-
    dot_movement(Edge,NewEdge).

%% For all the signs that appear in the input bag, create inactive edges.
chart_initialization(Agenda) :-
    findall(Item,initial_item(Item),Agenda).

initial_item(inactive_edge(SynSem,Bitcode,l(ref(Class,Tag1,Label,Used,P0,P0,R0,R0,His,N,GuidesTag)))) :-
    syn_sem_lex(SynSem,lex(ref(Class,Tag1,Label,Used,P0,P0,R0,R0,His,N,GuidesTag)),Bitcode).

initial_item(inactive_edge(SynSem,0,gap(ID))) :-
    alpino_genrules:gap(ID,SynSem).

%% Initialize active edges from inactive edges.
rule_invocation(inactive_edge(OldMother,Bitcode,N),EDGE) :-
    %% The mother of an inactive edge is the head daugther of the
    %% rules that we are interested in.
    alpino_genrules:headed_grammar_rule(OldMother,Id,Mother,Rest),
    %% Since we already know the head daughter, we can make a tree node for
    %% the daughter representing the head.
    substitute(OldMother,Rest,N,UDaughters),
    construct_edge(Mother,Bitcode,Id,[],UDaughters,EDGE).

%% Dot movement for imp rules, which handle verb-second word order. imp rules have
%% a second daughter that stores an identifier and vslash, to be retrieved by a
%% vgap lateron. Once we encounter such predicate daughter, we known that an inactive
%% vgap edge should exist as well.
dot_movement(active_edge(Mother,Bitcode,Id,PDtrs,
                         [call(put_val(Identifier,VSlash))|UDtrs]),EDGE) :-
    !,
    gen_sym(Identifier,generator_values),  % GvN: ensure Identifier is grounded,
                                                      % so we cannot later combine wrong
                                                      % get_val / put_val

    % copy_term(VSlash,VSlash1),  perhaps we need this once vpx coordinations work?
    
    alpino_genrules:headed_grammar_rule(call(get_val(Identifier,VSlash)),vgap,VGapMother,_),    
    (   EDGE = inactive_edge(VGapMother,0,vgap)
    ;   EDGE = active_edge(Mother,Bitcode,Id,PDtrs,UDtrs)
    ).

%% Normal dot movement over active edges.
dot_movement(active_edge(AMother,ABitcode,AId,APDtrs,[IMother|AUDtrs]),EDGE) :-
    inactive_edge(IMother,IBitcode,INum),
    dot_movement_aux(IMother,IBitcode,INum,AMother,ABitcode,
		     AId,APDtrs,AUDtrs,EDGE).

%% Normal dot movement over inactive edges.
dot_movement(inactive_edge(IMother,IBitcode,INum),EDGE) :-
    active_edge(IMother,AMother,ABitcode,AId,APDtrs,AUDtrs,_),
    dot_movement_aux(IMother,IBitcode,INum,AMother,ABitcode,
		     AId,APDtrs,AUDtrs,EDGE).

dot_movement_aux(_IMother,IBitcode,INum,AMother,ABitcode,AId,
		 PDtrs,UDtrs,EDGE) :-
    %% The active and inactive edge should not cover the same lexical items.
    combine_bitcodes(ABitcode,IBitcode,NewBitcode),
    construct_edge(AMother,NewBitcode,AId,[INum|PDtrs],UDtrs,EDGE).

construct_edge(Mother,Bitcode,Id,PDtrs0,UDtrs0,EDGE) :-
    \+ \+ check_part_of_dt(Mother),
    skip_head(UDtrs0,PDtrs0,UDtrs,PDtrs),
    active_inactive(UDtrs,Bitcode,Id,Mother,PDtrs,EDGE).

active_inactive(UDtrs,Bitcode,Id,Mother,PDtrs0,EDGE) :-    
    (	UDtrs == []
    ->	reverse(PDtrs0,PDtrs),
        EDGE = inactive_edge(Mother,Bitcode,r(Id,PDtrs))
    ;	EDGE = active_edge(Mother,Bitcode,Id,PDtrs0,UDtrs)
    ).

assert_history(InactiveId,His) :-
    assertz(his(InactiveId,His)).

%% Head skipping: heads were already processed during rule invocation,
%% and have an inactive edge number. We can just move them to the list
%% of processed daughters.
skip_head([],PDtrs,[],PDtrs).
skip_head([Next|UDtrs0],PDtrs,UDtrs,NewPDtrs) :-
    (   number(Next)
    ->  NewPDtrs = [Next|PDtrs],
	UDtrs = UDtrs0
    ;   NewPDtrs = PDtrs,
	UDtrs = [Next|UDtrs0]
    ).

%%%%%%%%%%%%%%%%%
% Edge creation %
%%%%%%%%%%%%%%%%%

%%
%% These predicates make chart edges. An edge can be in two states:
%%
%% 1. the edge is on the agenda
%% 2. the edge is on the chart
%%
%% Agenda edges are processed one by one, attempting rule invocation and dot
%% movement. When we are done with an agenda edge, it is removed from the agenda
%% and put on the chart. An edge that is on the chart is only passively used by
%% the generator: it can be used in dot movement invoked by edges on the agenda
%% as they are processed.
%%

%% Create an edge on the chart.
put_on_chart(fs(Hash,Copy,Bitcode,Cons),inactive_edge(Mother,Bitcode,Count)) :-
    assertz(inactive_edge(Mother,Bitcode,Count),Ref),
    assertz(inactive_edge_ref(Count,Ref)),
    assertz(inactive_edge_frozen(Hash,Copy,Bitcode,Cons,Count)).
put_on_chart(none,active_edge(Mother,Bitcode,Id,PDtrs,[UDtr|UDtrs])) :-
    (   UDtr = call(put_val(_,_))
    ->  true
    ;   bb_get(edge_count,Count),
	assertz(active_edge(UDtr,Mother,Bitcode,Id,PDtrs,UDtrs,Count)),
	NewCount is Count + 1,
	bb_put(edge_count,NewCount)
    ).
    

%%%%%%%%%%%%%%%%%%%%%%%
% Generated sentences %
%%%%%%%%%%%%%%%%%%%%%%%

generated_sentence(Sentence,P,Features,Tree,DtFsAll,BitcodeAll,UseFluency) :-
    hdrug_flag(number_analyses,Number0),
    (	integer(Number0),
	Number0 > 0
    ->	Number0 = Number
    ;	Number = 100000000
    ),
    start_it_deepening(generator,0,10,MaxPunct),
    %% iterative deepening..
    findall(Sentence-DtFsAll-Tree,
            unpack_and_number(BitcodeAll,DtFsAll,MaxPunct,Tree,Sentence),
            Trees),
    length(Trees,NTrees),
    debug_message(1,"~d realization candidates~n",
			     [NTrees]),
    score_trees(Trees,ScoredTrees,UseFluency),
    keysort(ScoredTrees,SortedTrees),
    list_first_n(SortedTrees,BestTrees,Number),
    member(P-Features-Sentence-DtFsAll-Tree,BestTrees),
    alpino_data:deriv_tree_struct(_,DtFsAll,_,Tree),
    end_it_deepening(generator).


%% Iterative deepening - increase the amount of allowed punctuation until
%% we are able to successfully unpack a derivation.

start_it_deepening(Key,Depth0,Depth,CurDepth) :-
    bb_put(Key,failed),
    between(Depth0,Depth,CurDepth),
    bb_get(Key,failed).

end_it_deepening(Key) :-
    bb_put(Key,succeeded).

unpack_and_number(BitcodeAll,DtFsAll,MaxPunct,Tree,Sentence) :-
    unpack_top(BitcodeAll,DtFsAll,MaxPunct,Tree),
    number_terminals(Tree,0,_,Sentence,[]).

score_trees([],[],_).
score_trees([Sentence-Fs-Tree|T],[P-Features-Sentence-Fs-Tree|NewT],UseFluency) :-
    (   UseFluency == on
    ->  sentence_fluency_maxent(Fs,Tree,P,Features)
    ;   Features = [],
	P = 0.0
    ),
    score_trees(T,NewT,UseFluency).

%%%%%%%%%%%%%
% Unpacking %
%%%%%%%%%%%%%

%% XXX - new and experimental

%% Unpack all edges with a top category.
unpack_top(BitcodeAll,DtFsAll,MaxPunct,Tree) :-
    hdrug_flag(generate_bc_check,BcCheck),
    hdrug_flag(fluency_beam,Beam0),
    (   integer(Beam0),
	Beam0 > 0
    ->  Beam0 = Beam
    ;   Beam = 0
    ),	
    retractall(unpack_memo(_,_)),
    top_edge(EdgeId),
    unpack_all(BitcodeAll,DtFsAll,EdgeId,MaxPunct,Tree,BcCheck,Beam).

unpack_all(BitcodeAll,DtFsAll,EdgeId,MaxPunct,Tree,BcCheck,Beam) :-
    (   BcCheck == on
    ->  inactive_edge(DtFsAll,BitcodeAll,EdgeId)
    ;   inactive_edge(DtFsAll,_,EdgeId)
    ),
    unpack(Beam,EdgeId,DtFsAll,+,Tree,0,MaxPunct,MaxPunct).

unpack(0,Id,Fs,_,Tree,Pu0,Pu,Pux) :-
    !,
    his(Id,His),
    unpack_(His,Id,Fs,Tree,Pu0,Pu,Pux,0),
    Pu =< Pux.
unpack(Beam,Id,Fs,Proj,Tree,Pu0,Pu,Pux) :-
    unpack_beam(Proj,Id,Fs,Tree,Pu0,Pu,Pux,Beam).

%% Unpacking with a beam:
%%
%% - Unpack all derivations for non-maximal projections (-).
%% - Unpack the (locally) most probable derivation trees if the history
%%   represents a maximal projection (+).
unpack_beam(-,Id,Fs,Tree,Pu0,Pu,Pux,Beam) :-
    his(Id,His),
    unpack_(His,Id,Fs,Tree,Pu0,Pu,Pux,Beam),
    Pu =< Pux.
unpack_beam(+,Id,Fs,Tree,Pu0,Pu,Pux,Beam) :-
    (   unpack_memo(Id,List)
    ->  memo_members(List,Fs,Tree,Pu0,Pu,Pux)
    ;   unpack_best(Beam,Id,Pu0,Pux,List),
	memoize_unpack(Id,List,Pu0),
	member(g(Fs,Tree,Pu),List),
	Pu =< Pux
    ).

unpack_best(Beam,Id,Pu0,Pux,List) :-
    n_best(Beam,Score,
	   (   his(Id,His),
	       unpack_(His,Id,Fs,Tree,Pu0,Pu,Pux,Beam),
	       sentence_fluency_maxent(Fs,Tree,Score,_)
	   ),
	   g(Fs,Tree,Pu),List
	  ).

%% Calculate the amount of punctuation after using a memoized history.
memo_members(List,Fs,Tree,Pu0,Pu,Pux) :-
    member(g(Fs,Tree,PuDiff),List),
    Pu is Pu0 + PuDiff,
    Pu =< Pux.    

memoize_unpack(Id,List0,Pu0) :-
    punct_diff(List0,List,Pu0),
    assertz(unpack_memo(Id,List)).

%% Calculate the number of local punctuation signs in a history.
punct_diff([],[],_).
punct_diff([g(Fs,Tree,Pu)|T],[g(Fs,Tree,PuDiff)|NewT],Pu0) :-
    PuDiff is Pu - Pu0,
    punct_diff(T,NewT,Pu0).

unpack_(vgap,EdgeId,Fs,Tree,Pu,Pu,_Pux,_Beam) :-
    inactive_edge_ref(EdgeId,Ref),
    clause(inactive_edge(Fs,_,EdgeId),Body,Ref),
    call(Body),
    alpino_data:deriv_tree_struct(vgap,Fs,[],Tree).
unpack_(gap(RuleId),EdgeId,Fs,Tree,Pu,Pu,_Pux,_Beam) :-
    inactive_edge_ref(EdgeId,Ref),
    clause(inactive_edge(Fs,_,EdgeId),Body,Ref),
    call(Body),
    alpino_data:deriv_tree_struct(RuleId,Fs,[],Tree).
unpack_(l(ref(Class,Tag,Label,Used,P0,P0,R0,R0,His,N,GuidesTag)),EdgeId,Fs,Tree,
       Pu0,Pu,_Pux,_Beam) :-
    (   Tag = punct(_)
    ->  Pu is Pu0 + 1
    ;   Pu = Pu0
    ),
    inactive_edge_ref(EdgeId,Ref),
    clause(inactive_edge(Fs,_,EdgeId),Body,Ref),
    call(Body),
    alpino_data:deriv_tree_struct(Used,Fs,
			   lex(ref(Class,Tag,Label,Used,P0,P0,R0,R0,His,N,GuidesTag)),Tree),
    unify_lex(Fs,Tag,Used,P0,R0).
unpack_(r(RuleId,His),_,Mother,Tree,Pu0,Pu,Pux,Beam) :-
    alpino_genrules:grammar_rule_unpack(RuleId,Mother,Fses,Projs),
    unpack_ds(His,Fses0,Projs,Trees,Pu0,Pu,Pux,Beam),
    Fses0 = Fses,
    alpino_data:deriv_tree_struct(RuleId,Mother,Trees,Tree).

unpack_ds([],[],[],[],Pu,Pu,_Pux,_Beam).
unpack_ds([H|T],[FsH|FsT],[ProjH|ProjT],[HDtr|NewT],Pu0,Pu,Pux,Beam) :-
    unpack(Beam,H,FsH,ProjH,HDtr,Pu0,Pu1,Pux),
    unpack_ds(T,FsT,ProjT,NewT,Pu1,Pu,Pux,Beam).

top_edge(EdgeId) :-
    alpino_lc_in:top_category_(Mother),
    inactive_edge(Mother,_BitcodeAll,EdgeId).

%% Unify some lexical information in the dt structure.
unify_lex(Fs,Tag,Used,P,R) :-
    (   alpino_data:punct(Fs)
    ->  true
    ;   % this fails for punctuation, which has no dt att!
	(   Tag = with_dt(_,_)
	->  alpino_data:dt_fwrd_positions(Fs,P,R)
	;   alpino_data:dt_hwrd_positions(Fs,P,R),
	    alpino_data:lexical_node(Fs,_,Used,_,_)
	)
    ).

list_first_n(L,NewL,N) :-
    list_first_n(L,NewL,0,N).

%% steadfast, zonder cuts:
list_first_n([],[],_,_).
list_first_n([H|T],Result,Count,N) :-
    (   Count =:= N
    ->  Result = []
    ;   NewCount is Count + 1,
	Result=[H|NewT],
	list_first_n(T,NewT,NewCount,N)
    ).

%%%%%%%%%%%%%%%%%%%%%
% Terminal handling %
%%%%%%%%%%%%%%%%%%%%%

frames_of_deriv(Tree,Frames) :-
    findall(Frame,frame_of_deriv(Tree,Frame),Frames).

frame_of_deriv(Tree,frame(P0,P,Q0,Q,Label,Tag,Used,His)) :-
    subtree(Tree,tree(_,_,lex(ref(_Class,Tag,Label,Used,P0,P,Q0,Q,His,_N,_GuidesTag)),_)).

subtree(Tree,Tree).
subtree(tree(_,_,Ds,_),Tree) :-
    member(Tree1,Ds),
    subtree(Tree1,Tree).

number_terminals(tree(_,_,Ds,_),N,NextN,Str0,Str) :-
    number_terminals_tree_ds(Ds,N,NextN,Str0,Str).

number_terminals_tree_ds(lex(ref(_,_,_,Terminal,N,_,NextN,_,_,_,_)),N,NextN,
                         Str0,Str) :-
    alpino_genlex:surf_to_list(Terminal,Words),
    length(Words,Len),
    NextN is N + Len,
    append(Words,Str,Str0).
number_terminals_tree_ds([],N,N,Str,Str).
number_terminals_tree_ds([H|T],N,NextN,Str0,Str) :-
    number_terminals_daughters([H|T],N,NextN,Str0,Str).

number_terminals_daughters([],N,N,Str,Str).
number_terminals_daughters([Daughter|Rest],N,NextN,Str0,Str) :-
    number_terminals(Daughter,N,NextN0,Str0,Str1),
    number_terminals_daughters(Rest,NextN0,NextN,Str1,Str).

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Syntax/semantics lookup %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% The syn_sem_lex predicate adds
%% subcategorization information and the word inflection, based on
%% the tag of a lexical item. 
lookup_tag(Tag,Label,Cat,Ref,Dt,Word) :-
    findall(Cat-Constraints1,alpino_lex_types:lex(Cat,Tag,Hwrd,Constraints1),List),
    nth(N,List,Cat-Constraints),
    alpino_genlex:call_constraints(Constraints),
    alpino_data:dt_if_defined(Cat,Dt),
    alpino_data:lexical(Hwrd,Label,Word,_,_,gen,_),
    alpino_tr_tag:tr_tag(Tag,Class),
    Ref=ref(Class,Tag,Label,Word,_,_,_,_,gen,N,_).

%% Add syntax/semantics to the lexical entries.
syn_sem_lex(Cand,His,Bitcode) :-
    lex_or_punct(Tag,Label,Bitcode,Dt,_,Surfs),
    member(Surf,Surfs),
    findall(SynSem-lex(Ref),
            lookup_tag(Tag,Label,SynSem,Ref,Dt,Surf),
            Cands),
    member(Cand-His,Cands),
    debug_message(2,"found att-val structure for lex ~w ~w~n",[Label,Tag]).

lex_or_punct(punct(Type),Comma,0,_,_,[Comma]) :-
    alpino_genlex:punct(Comma,Type).
lex_or_punct(Tag,Label,Bitcode,Dt,Dt2,Surfs) :-
    lex(Tag,Label,Bitcode,Dt,Dt2,Surfs).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% check_part_of_dt %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% a. dt_part/1
%% check that the DT of the node is unifiable with
%% some part of the input semantics
%%
%% b. check_c_list/1
%% for nodes of type clist, take the values of
%% cats and conj, and check that there is a DT
%% such that our cats is a sublist of CNJ and
%% our conj is a sublist of CRD
%%
%% c. dt_check_percolation_features
%%
%% check that all elements on the list-valued
%% features capps, cdets, cmods, cpredms
%% actually occur as app, det, mod, predm
%% in the current DT, in an accessible place.
%%
%% d. check mexs feature
%%
%% e. check tags feature
%%
%% these checks are crucial to keep the number
%% of edges small

check_part_of_dt(Mother) :-
    (   alpino_data:dt(Mother,Dt)
    ->  (   var(Dt)  % e.g. rule imp, imp_imp
        ->  true
        ;   alpino_adt:dt_part(Dt),
            dt_check_percolation_features(Mother,Dt)
        )
    ;   check_clist(Mother)
    ),
    check_mexs(Mother),
    check_tags(Mother).


check_tags(Mother) :-
    alpino_data:tags(Mother,List),
    check_tags_list(List).

check_tags_list(Var):-
    var(Var), !.
check_tags_list([]).
check_tags_list([H|T]) :-
    check_tags_val(H),
    check_tags_list(T).

check_tags_val(Tag) :-
    alpino_data:dt_out(Tag,DT),
    alpino_adt:dt_part(DT).

check_mexs(Mother) :-
    alpino_data:mexs(Mother,List),
    check_mexs_list(List).

check_mexs_list(Var):-
    var(Var), !.
check_mexs_list([]).
check_mexs_list([H|T]) :-
    check_mexs_val(H),
    check_mexs_list(T).

check_mexs_val(H) :-
    alpino_data:mexs_cat_mods(H,Out),
    (  Out = []
    ;  Out = [DT],
       alpino_adt:dt_part(DT)
    ).


dt_check_percolation_features(Mother,Dt) :-
    alpino_data:percolation_features(Mother,Apps,Capps,Dets,Cdets,
                              Mods,Cmods,Predms,Cpredms),
    alpino_wappend:wsublist(Cmods,Mods),
    alpino_wappend:wsublist(Capps,Apps),
    alpino_wappend:wsublist(Cdets,Dets),
    alpino_wappend:wsublist(Cpredms,Predms),
    dt_check_percolation_f(Cmods,mod,Dt),
    dt_check_percolation_f(Capps,app,Dt),
    dt_check_percolation_f(Cdets,det,Dt),
    dt_check_percolation_f(Cpredms,predm,Dt).

dt_check_percolation_f(List,_Att,_Dt) :-
    var(List), !.
dt_check_percolation_f([],_,_).
dt_check_percolation_f([H|T],Att,Dt):-
    dt_check_percolation(H,Att,Dt),
    dt_check_percolation_f(T,Att,Dt).

%% Val must unify with a member of Dt:(vc|body)*:Att
dt_check_percolation(Val,Att,Dt) :-
    hdrug_feature:e(Att,Dt,List),
    member(Val,List).

dt_check_percolation(Val,Att,Dt) :-
    hdrug_feature:e(vc,Dt,VC),
    dt_check_percolation(Val,Att,VC).

dt_check_percolation(Val,Att,Dt) :-
    hdrug_feature:e(cnj,Dt,List),
    member(BODY,List), % typically in each cnj..
    dt_check_percolation(Val,Att,BODY).

dt_check_percolation(Val,Att,Dt) :-
    hdrug_feature:e(cmp,Dt,COMP),
    bridge_comp(COMP),
    hdrug_feature:e(body,Dt,BODY),
    dt_check_percolation(Val,Att,BODY).

bridge_comp(COMP) :-
    alpino_data:dt(COMP,Hwrd,_,_,_),
    alpino_data:label(Hwrd,Bridge,_,_),
    bridge(Bridge).

bridge('aan het').
bridge(dat).
bridge(om).
bridge(te).

check_clist(Mother) :-
    (   nonvar(Mother),
        alpino_data:clist(Mother,CatsList0,ConjList0)
    ->  add_tail(CatsList0,CatsList),
        add_tail(ConjList0,ConjList),
        alpino_data:dt_cnj_crd(DT,CatsList,ConjList),
        alpino_adt:dt_part(DT)
    ;   true
    ).

add_tail(X,_) :-
    var(X), !.
add_tail([],_) :-
    !.
add_tail([H|T],[H|NT]) :-
    add_tail(T,NT).

%%%
%%% Printing and other debugging tools
%%%

:- public active_edges/0, inactive_edges/0,
          print_active_edge_ids/0,
          print_inactive_edge_ids/0.

print_active_edge_ids :-
	findall(Id,active_edge(_,_,_,Id,_,_,_),Ids),
	print_edge_ids_aux(Ids).

print_inactive_edge_ids :-
	findall(Id,inactive_edge(_,_,Id),Ids),
	print_edge_ids_aux(Ids).


print_edge_ids_aux([]).
print_edge_ids_aux([Head|Tail]) :-
	write(Head), nl,
	print_edge_ids_aux(Tail).

active_edges :-
    (   active_edge(_,_,Bc,Id,Dtrs1,Dtrs2,N),
        start_it_deepening(debug_edges,0,10,Cur),
	unpack_ds(Dtrs1,_,_,Trees,0,Cur,Cur,0),
	get_terminals_ds(Trees,Terms,Terms1),
	get_terminals_uds(Dtrs2,Terms1,[]),
        end_it_deepening(debug_edges),
        write(active_edge(N,Bc,Id,Terms)),nl,
        fail
    ;   true
    ).  

get_terminals_uds(Dtrs,Terms0,Terms) :-
    member(Dtr,Dtrs),
    number(Dtr),
    !,
    start_it_deepening(uds,0,10,Cur),
    unpack(0,Dtr,_,_,Tree,0,Cur,Cur),
    end_it_deepening(uds),
    get_terminals(Tree,Terms0,Terms).
get_terminals_uds(_,T,T).


get_terminals(Tree,Terms) :-
    get_terminals(Tree,Terms,[]).

get_terminals(tree(_,_,Ds,_),Terms0,Terms) :-
    get_terminals_ds(Ds,Terms0,Terms).

get_terminals_ds(lex(ref(_,_,_,Word,_,_,_,_,_,_,_)),[Word|Terms],Terms).
get_terminals_ds([],Terms,Terms).
get_terminals_ds([H|T],Terms0,Terms) :-
    get_terminals_daughters([H|T],Terms0,Terms).

get_terminals_daughters([],Terms,Terms).
get_terminals_daughters([Daughter|Rest],Terms0,Terms) :-
    get_terminals(Daughter,Terms0,Terms1),
    get_terminals_daughters(Rest,Terms1,Terms).


inactive_edges :-
    inactive_edge_words(_).

inactive_edge_words(Words) :-
    (	start_it_deepening(debug_edges,0,10,Cur),
	unpack_all(Bc,_,N,Cur,Tree,on,0),
        end_it_deepening(debug_edges),
	get_terminals(Tree,Words),
        get_id(Tree,Id),
        write(inactive_edge(N,Bc,Id,Words)),nl,
        fail
    ;	true
    ).

get_id(Tree,Tag/N) :-
    alpino_data:deriv_tree_struct(_,_,lex(ref(_,Tag,_,_,_,_,_,_,_,N,_)),Tree),
    !.
get_id(Tree,Id) :-
    alpino_data:deriv_tree_struct(Id,_,_,Tree).

simple_inactive_edge(inactive_edge(N,Bc,Id,Words)) :-
        start_it_deepening(debug_edges,0,10,Cur),
	unpack_all(Bc,_,N,Cur,Tree,on,0),
        end_it_deepening(debug_edges),
	get_terminals(Tree,Words),
	get_id(Tree,Id).

update_inactive_edges :-
	findall(Edge,simple_inactive_edge(Edge),Edges),
	hdrug_gui:update_array(Edges,inactive_edges).

:- public show_edge/3, show_gen_fs/2, show_gen_lex/3, gen_init/1.

gen_init(Ref):-
    clean,
    hdrug:a_lf(Ref,Sem),
    prepare_adt_and_lex(Sem,_Fs,_Bitcodeall),
    count_edges(lex(_,_,_,_,_,_),Edges),
    debug_message(1,"~w lexical frames for generation~n",[Edges]).

show_gen_lex(Word,Type0,Output) :-
    (   Type0 == default -> Type=fs ; Type0=Type ),
    lex(A0,Word,B0,C0,D0,E0),
    copy_term(lex(A0,Word,B0,C0,D0,E0),lex(A,Word,B,C,D,E),Cons),
    hdrug_show:show(Type,Output,[clause(lex(A,Word,B,C,D,E),Cons)]).

show_gen_fs(Type0,Output) :-
    (   Type0 == default -> Type=fs ; Type0=Type ),
    alpino_adt:simple_fs(Result0),
    copy_term(Result0,Result,Cons),
    hdrug_show:show(Type,Output,[clause(Result,Cons)]).

show_edge(N,Type,Output) :-
    number(N),
    start_it_deepening(debug_edges,0,10,Cur),
    unpack_all(_,Fs,N,Cur,Tree,off,0),
    end_it_deepening(debug_edges),
    alpino_data:result_term(_,_,Fs,Tree,_,Result),
    hdrug_show:show(Type,Output,[value(Result)]).

show_edge(N,Type,Output) :-
    number(N),
    active_edge(_Head,Mother,_Bc,_Id,_,Ds,N),
    alpino_data:result_term(_,_,Mother,tree(Mother,_,Ds,_),_,Result),
    hdrug_show:show(Type,Output,[value(Result)]).

:- public combine_rule_edges/2.
combine_rule_edges(Rule,Id1) :-
    inactive_edge(OldMother,BitCode,Id1),
    %his(His,_Id,IDerivTree),
    alpino_genrules:headed_grammar_rule(OldMother,Rule,Mother,Rest),
    substitute(OldMother,Rest,Id1,UDaughters),
    construct_edge(Mother,BitCode,Rule,[],UDaughters,EDGE),
    dot_movement(EDGE,_NEDGE).

glex(N) :-
    set_flag(parse_or_generate,generate),
    hdrug:a_sentence(N,Sent),
    hdrug:display_extern_phon(Sent),
    set_flag(current_ref,N),
    alpino_treebank:treebank_directory(Dir),
    charsio:format_to_chars('~w/~w.xml',[Dir,N],Chars),
    atom_codes(FileXml,Chars),
    alpino_treebank:xml_file_to_dt(FileXml,DT),
    clean,
    alpino_adt:dt_to_adt(DT,Sem),    
    prepare_adt_and_lex(Sem,_Fs,_Bc),
    count_edges(lex(_,_,_,_,_,_),Edges),
    debug_message(1,"~w lexical frames for generation~n",[Edges]),
    gtags.

:- public glex/1, gtags/0.
gtags :-
    (   lex(Frame,Root,BitCode,_,_,Surfs),
        format(user_error,"~w ~w ~w ~w~n",[Root,Frame,BitCode,Surfs]),
        fail
    ;   true
    ).

%%%%%%%%%%%%
%% Packing %
%%%%%%%%%%%%

%% This predicate produces a 'frozen' representation of an edge
%% where blocked goals are separated and variables are instantiated
%% with numbered terms.
freeze_edge(inactive_edge(SynSem,Bitcode,_),FS) :-
    copy_term(SynSem,Copy,Const),
    numbervars(Copy/Const,0,_),
    terms:term_hash(Copy/Const/Bitcode,Hash),
    FS = fs(Hash,Copy,Bitcode,Const).
freeze_edge(active_edge(_,_,_,_,_),none).

%% Pack an inactive edge if there is already an inactive edge on the chart
%% with the same (frozen) attribute-value structure. Packing adds a
%% construction history to the existing edge.
pack_edge(inactive_edge(__SynSem,Bitcode,Ds),fs(Hash,Copy,Bitcode,Cons)) :-
    inactive_edge_frozen(Hash,Copy,Bitcode,Cons,N),
    debug_message(2,"history ~q can be forward-packed as edge ~d~n",
			     [Ds,N]),
    assert_history(N,Ds).


