:- module(alpino_genlex, []).

:- expects_dialect(sicstus).

:- use_module(library(lists)).
:- use_module(hdrug(hdrug_util)).
:- use_module(alpino('src/utils')).
:- use_module(alpino('src/latin1')).
:- use_module(bitcode).

%% all called from cg.pl
:- public filter_tags/0, pos2frames/5, check_conditions/4,
          dict_entry/3, surf_to_list/2, lex_lookup/1.


%%% lexical lookup etc.
%%% perhaps move all this stuff to genlex.pl

lex_lookup(AdtNoRefs) :-
    retractall(alpino_cg:lex(_,_,_,_,_,_)),
    time(1,create_lex(AdtNoRefs,[])),
    time(1,introduce_particles_lex),
    time(1,introduce_with_dt(AdtNoRefs,Roots)),
    time(1,filter_tags),
    debug_call(1,check_missing_roots(Roots)).

%% The create_lex and create_lex_daughters predicates find all leave nodes
%% that are lexical items, and assert them as facts.
create_lex(tree(r(opt(Rel),Node),Ds),Path) :-
    !,
    create_lex(Node,Rel,Ds,Path).
create_lex(tree(r(Rel,Node),Ds),Path) :-
    create_lex(Node,Rel,Ds,Path).

create_lex(p(Cat,_),Rel,Ds,Path) :-
    create_lex_daughters(Ds,[Rel/Cat|Path]).
create_lex(i(_,Node),Rel,Ds,Path) :-
    create_lex_indexed(Node,Rel,Ds,Path).
create_lex(i(_),_,[],_).
create_lex(adt_lex(Root,Bc,Frames,Dt,SimpleDt),REL,[],Path) :-
    if(create_frames(Frames,Root,Bc,Dt,SimpleDt,[REL|Path]),true,true).

%% special case for "ik heb gegeten en gedronken"
%% a head word that is co-indexed does not get unified
%% with its DT...
create_lex_indexed(adt_lex(Root,Bc,Frames,_Dt,_SimpleDt),hd,[],Path) :-
    !,
    debug_message(2,"ps: co-indexed head spotted (~w); not unifying DT~n",[Root]),
    alpino_data:lexical(Hwrd,Root,_,_,_,_,Bc),
    alpino_data:dt(Dt,Hwrd,_,_,_),
    if(create_frames(Frames,Root,Bc,Dt,Dt,[hd|Path]),true,true).
create_lex_indexed(Node,Rel,Ds,Path):-
    create_lex(Node,Rel,Ds,Path).


create_lex_daughters([],_).
create_lex_daughters([Hd|Tl],Path) :-
    create_lex(Hd,Path),
    create_lex_daughters(Tl,Path).

/*
create_frames(Root,Sense,Pos,Attrs,Bc,Dt,SimpleDt,REL) :-
    pos2frames(Root,Sense,Pos,Attrs,FramesSurfs0),
    create_frames(FramesSurfs0,Root,Bc,Dt,SimpleDt,REL).
*/

create_frames(FramesSurfs0,Root,Bc,Dt,SimpleDt,REL) :-
    filter_frames(FramesSurfs0,[Fr|FramesSurfs],Dt,SimpleDt,REL),
    assert_lex([Fr|FramesSurfs],Root,Bc,Dt,SimpleDt).

filter_frames([],[],_,_,_).
filter_frames([Frame-Surfs|T],N,Dt,SimpleDt,REL) :-
    (   check_conditions(Frame,Dt,SimpleDt,REL)
    ->  N = [Frame-Surfs|T2]
    ;   N = T2
    ),
    filter_frames(T,T2,Dt,SimpleDt,REL).

assert_lex([],_,_,_,_).
assert_lex([Frame-Surfs|Tail],Label,Bitcode,Dt,SimpleDt) :-
    assertz(alpino_cg:lex(Frame,Label,Bitcode,Dt,SimpleDt,Surfs)),
    assert_lex(Tail,Label,Bitcode,Dt,SimpleDt).

%%%%%%%%%%%%%%%%%%%%%%%%%
% Particle introduction %
%%%%%%%%%%%%%%%%%%%%%%%%%

introduce_particles_lex :-
    (   alpino_cg:lex(Frame,Root,Bitcode,Dt,SimpleDt,_Surfs),
        (  Frame = verb(_,_,Subcat)
        ;  Frame = v_noun(Subcat)
        ),
        Subcat =.. [F|_],
        F \== ninv,
        alpino_postags:postag_of_frame(Frame,_,Attrs),
        attr_particles(Attrs,Particles0),
        alpino_data:dt_svp(SimpleDt,SvpList),
        remove_existing_particles(Particles0,Particles,SvpList),
        assert_particles_lex(Particles,Bitcode,FinalBitcode),
	reassert_lex_bitcode(Frame,Root,Bitcode,Dt,SimpleDt,FinalBitcode),
        fail
    ;   true
    ).

remove_existing_particles([],[],_).
remove_existing_particles([Part|Parts],Rest,SvpList) :-
    svp_has_part(SvpList,Part),
    !,
    remove_existing_particles(Parts,Rest,SvpList).
remove_existing_particles([H|T0],[H|T],SvpList) :-
    remove_existing_particles(T0,T,SvpList).

svp_has_part(SvpList,Part) :-
    member(Svp,SvpList),
    alpino_data:dt(Svp,Hwrd,_,_,_),
    alpino_data:lexical(Hwrd,Part,_,_,_,_,_).

svp_has_part(SvpList,Part) :-
    member(SvpConj,SvpList),
    alpino_data:dt_cnj_crd(SvpConj,Cnj,_),
    member(Svp,Cnj),
    alpino_data:dt(Svp,Hwrd,_,_,_),
    alpino_data:lexical(Hwrd,Part,_,_,_,_,_).

reassert_lex_bitcode(Frame,Root,Bitcode,Dt,SimpleDt,NewBitcode) :-
    clause(alpino_cg:lex(Frame,Root,Bitcode,Dt,SimpleDt,Surfs),_,Ref),
    erase(Ref),
    assertz(alpino_cg:lex(Frame,Root,NewBitcode,Dt,SimpleDt,Surfs)).


/*
frames_particle_lengths([],[]).
frames_particle_lengths([Frame|FrameTail],[Length|LengthTail]) :-
    particles_of_frame(Frame,Particles),
    length(Particles,Length),
    frames_particle_lengths(FrameTail,LengthTail).

particles_of_frame(Frame,Particles) :-
    alpino_postags:postag_of_frame(Frame,_,Attrs),
    attr_particles(Attrs,Particles).
*/

assert_particles_lex([],Bc,Bc).
assert_particles_lex([Particle|RestParticles],Bc,FinalBc) :-
    next_bitcode(Bc,NextBc,BcRest),
    alpino_data:lexical(Hwrd,Particle,_,_,_,_,NextBc),
    alpino_data:dt(Dt,Hwrd,particle(Particle),_,_),
    findall(Surf,dict_entry(Particle,particle(Particle),Surf),Surfs),
    (    \+ alpino_cg:lex(particle(Particle),Particle,NextBc,Dt,Dt,Surfs)
    ->   assertz(alpino_cg:lex(particle(Particle),Particle,NextBc,Dt,Dt,Surfs))
    ;    true
    ),
    assert_particles_lex(RestParticles,BcRest,FinalBc).

:- public attr_particles/2.  % called from adt.pl
:- public frames_and_particles/6.  % called from adt.pl

frames_and_particles(Root,Sense,PosTag,Attrs,Parts,Frames) :-
    pos2frames(Root,Sense,PosTag,Attrs,Frames),
    findall(Part,frames_particle(Frames,Part),Parts0),
    sort(Parts0,Parts).

frames_particle(Frames,Part) :-
    member(Frame-_,Frames),
    alpino_postags:postag_of_frame(Frame,_,Attrs),
    member(sc=Term,Attrs),
    nonvar(Term),
    lex_particle(Term,Part).

attr_particles([],[]).
attr_particles([sc=Term|Attrs],[Particle|OtherParticles]) :-
    nonvar(Term),
    lex_particle(Term,Particle),
    !,
    attr_particles(Attrs,OtherParticles).
attr_particles([_|Attrs],OtherParticles) :-
    attr_particles(Attrs,OtherParticles).

lex_particle(Term,Particle) :-          % for verbs, v_noun
    Term =.. [Functor,Particle|_Rest],
    atom_concat(part_,_,Functor).

%%%%%%%%%%%%%%%%%%%%%%%%
% with_dt introduction %
%%%%%%%%%%%%%%%%%%%%%%%%

%% Some entries in the dictionary consist of multiple words. These so-called
%% 'with-dt' items come with a pre-packaged dependency structure.
%% Unfortunately, we cannot detect directly whether the use of with_dt entries
%% is required to realize an abstract dependency structure.
%%
%% The procedure for finding with_dt entries that should be used (if any)
%% as follows:
%%
%% For every interior node, and the list of roots it dominates over, we:
%%
%% - Filter the roots, keeping only the roots that occur in one of the
%%   with dt entries.
%% - Find all possible sublists of roots, containing at least two elements
%%   (since that is the minimum number of words that a with_dt spans).
%% - We sort each sublist, and look up the sublist in the dictionary.
%%   with_dt entries in the dictionary are accessed by the sorted list of
%%   roots.
%% - If bitcoding is used, the bits associated with the individual roots are
%%   OR'ed to retrieve the bitcode of the with_dt item.


introduce_with_dt(FsAdt,Roots) :-
    introduce_with_dt(FsAdt,Roots,[]).

introduce_with_dt(tree(r(_,Node),Ds),Roots,Roots0) :-
    introduce_with_dt(Node,Ds,Roots,Roots0).
introduce_with_dt(p(_,PDt),Ds,Roots,Roots0) :-
    introduce_with_dt_ds(Ds,Roots,Roots0),
    construct_with_dts(Roots,PDt).
introduce_with_dt(i(_,Node),Ds,Roots,Roots0) :-
    introduce_with_dt(Node,Ds,Roots,Roots0).
introduce_with_dt(i(_),[],Roots,Roots).
introduce_with_dt(adt_lex(Root,Bc,_,_,_),[],[Root-Bc|Roots0],Roots0).

introduce_with_dt_ds([],Roots,Roots).
introduce_with_dt_ds([H|T],Roots,Roots0) :-
    introduce_with_dt(H,Roots1,Roots0),
    introduce_with_dt_ds(T,Roots,Roots1).

construct_with_dts(Pairs0,Pdt) :-
    findall(R,potential_with_dt_root(Pairs0,R), Pairs1),
    keysort(Pairs1,Pairs2),
    (   find_matching_with_dt(Pairs2,Roots,Codes),
	construct_with_dts_aux(Roots,Codes,Pdt),
	fail
    ;   true
    ).	

find_matching_with_dt([Root-Code|Pairs0],[Root|Roots],[Code|Codes]) :-
    alpino_lex:with_dt_all(Root,List),
    find_matching_with_dt_aux(List,Pairs0,Roots,Codes).
find_matching_with_dt([_|Pairs],Roots,Codes) :-
    find_matching_with_dt(Pairs,Roots,Codes).

find_matching_with_dt_aux([],_,[],[]).
find_matching_with_dt_aux([Root|Roots],[Root-Code|Tail],[Root|Rs],[Code|Cs]) :-
    find_matching_with_dt_aux(Roots,Tail,Rs,Cs).
find_matching_with_dt_aux([Root|Roots],[_|Tail],Rs,Cs) :-
    find_matching_with_dt_aux([Root|Roots],Tail,Rs,Cs).

potential_with_dt_root(Roots,Root-Code) :-
    member(Root-Code,Roots),
    alpino_lex:with_dt_root(Root).

construct_with_dts_aux(CatRoots,Bcs,Pdt) :-
    concat_all(CatRoots,Stem,' '),
    findall(dict_entry(Stem,with_dt(Frame,Dt),Surface),
    	    dict_entry_with_dt(Stem,with_dt(Frame,Dt),Surface),

	    WithDts),
    merge_bc_list(Bcs,Bc),
    assert_with_dts(WithDts,Bc,Pdt).

/*
construct_with_dts(Roots0,Pdt) :-
    findall(R,potential_with_dt_root(Roots0,R), Roots),
    findall([W1,W2|T],sublist([W1,W2|T],Roots),SubLists),
    construct_with_dts_aux(SubLists,Pdt).

construct_with_dts_aux([],_).
construct_with_dts_aux([Roots|T],Pdt) :-
    findall(Root,member(Root-_,Roots),CatRoots0),
    sort_not_unique(CatRoots0,CatRoots),
    concat_all(CatRoots,Stem,' '),
    findall(dict_entry(Stem,with_dt(Frame,Dt),Surface),
    	    dict_entry_with_dt(Stem,with_dt(Frame,Dt),Surface),

	    WithDts),
    findall(Bitcode,member(_-Bitcode,Roots),Bcs),
    merge_bc_list(Bcs,Bc),
    assert_with_dts(WithDts,Bc,Pdt),
    construct_with_dts_aux(T,Pdt).

sort_not_unique(List,Sorted) :-
    add_vals(List,KeyList),
    keysort(KeyList,SortedKeyList),
    del_vals(SortedKeyList,Sorted).

add_vals([],[]).
add_vals([H|T0],[H-_|T]) :-
    add_vals(T0,T).

%% keys with args swappen. Have first arg indexing.
del_vals([],[]).
del_vals([H-_|T0],[H|T]) :-
    del_vals(T0,T).


*/

assert_with_dts([],_,_).
assert_with_dts([dict_entry(Label,Frame,Surface)|T],Bc,Pdt) :-
    copy_pdt(Frame,Pdt,PdtCopy),
    (   alpino_cg:lookup_tag(Frame,Surface,_,_,PdtCopy,Surface),
	\+ alpino_cg:lex(Frame,Label,Bc,PdtCopy,PdtCopy,[Surface])
    ->  assertz(alpino_cg:lex(Frame,Label,Bc,PdtCopy,PdtCopy,[Surface]))
    ;   true
    ),
    assert_with_dts(T,Bc,Pdt).

%% In the case of etc with_dts, the list of the cnj attribute contains
%% too many elements. Do we want to remove entries from the constraints
%% here, and activate the constraints again?
copy_pdt(with_dt(complex_etc,_),Pdt,PdtCopy) :-
    !,
    copy_term(Pdt,PdtCopy,_Cons).
copy_pdt(_,Pdt,PdtCopy) :-
    copy_term(Pdt,PdtCopy).


punct('`', aanhaal_links).
punct('\'', aanhaal_rechts).
punct(:,    dubb_punt).
punct('(',  haak_open).
punct(')',  haak_sluit).
punct(...,  hellip).
punct(=,    is_gelijk).
punct(',',  komma).
punct(-,    ligg_streep).
punct('.',  punt).
punct(;,    punt_komma).
punct('/',  schuin_streep).
punct('!',  uitroep).
punct('?',  vraag).
punct(x,    maal).
punct('×',  maal).
punct(+,    plus).
punct(&,    ampersand).
punct('|',  staand_streep).

%%%%%%%%%%%%%%%%%%%%%
% Inflection lookup %
%%%%%%%%%%%%%%%%%%%%%

dict_entry(Root,Frame,SurfaceAtom) :-
    root_surface(Root,SurfaceAtom,SurfaceList),
    debug_message(2,"lex lookup for surface form ~w~n",
                             [SurfaceAtom]),
    alpino_lex:lexicon(Frame,Root,SurfaceList,[],_).

dict_entry_with_dt(Root,Frame,SurfaceAtom) :-
    alpino_lex:inv_lex(Root,SurfaceAtom),
    surf_to_list(SurfaceAtom,SurfaceList),
    %% format(user_error,"lexical lookup with_dt: ~w~n",[SurfaceList]),
    alpino_lex:lexicon(Frame,Root,SurfaceList,[],_).

%% default: Root=Surf
root_surface(Root,Root,List) :-
    surf_to_list(Root,List).
root_surface(Root,Surf,List) :-
    alpino_lex:inv_lex(Root,Surf),
    surf_to_list(Surf,List).

%% for genitive inflection of names
root_surface(Root,Surf,[Surf]) :-
    (   Root = Surf0
    ;   alpino_lex:inv_lex(Root,Surf0)
    ),
    alpino_lex:in_names_dictionary(_Name,Root,Surf0,[],[],_),
    (   atom_concat(Surf0,s,Surf)
    ;   atom_concat(Surf0,'\'s',Surf)
    ;   alpino_lex:remove_s(Surf0),
        atom_concat(Surf0,'\'',Surf)   % dat was Mies' probleem
    ).

surf_to_list(Surf,List) :-
    atom_codes(Surf,Codes),
    alpino_util:split_string(Codes," ",CodesList),
    atom_codes_list(CodesList,List).

atom_codes_list([],[]).
atom_codes_list([H0|T0],[H|T]) :-
    atom_codes(H,H0),
    atom_codes_list(T0,T).

/*
atom_or_list([],H,H).
atom_or_list([H|T],F,[F,H|T]).
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% POS tag and attributes to frames %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pos2frames(Root,Sense,Pos,Attr,Frames) :-
    findall(Frame-Surfs,
            pos2frames_aux(Root,Sense,Pos,Attr,Frame,Surfs),
            Frames0),
    unknown_root_heuristics(Frames0,Frames1,Root,Sense,Pos,Attr),
    last_resort_heuristics(Frames1,Frames2,Root,Sense,Pos,Attr),
    final_heuristics(Frames2,Frames,Root,Sense,Pos,Attr).

pos2frames_aux(Root,Sense,Pos,Attr,Frame,Surfs) :-
    setof(Surf,dict_entry(Root,Frame,Surf),Surfs),
    alpino_treebank:frame2sense(Root,Frame,Sense),
    alpino_postags:postag_of_frame(Frame,Pos,CheckAttr),
    check_attributes(CheckAttr,Attr).

%% check_attributes(DictAtts,InputAtts)
%% we must find all InputAtts
%% and there must not be an inconsistency with DictAtts
check_attributes([],[]).
check_attributes([Att=Val|T],Attrs0) :-
    (   select(Att=Val1,Attrs0,Attrs)
    ->  Val=Val1
    ;   Attrs0 = Attrs
    ),
    check_attributes(T,Attrs).

exc_postag_of_frame(Frame,Pos,Attrs) :-
    (   alpino_postags:postag_of_frame(Frame,Pos,Attrs)
    ->  fail
    ;   Pos=noun,                          % exceptions for old treebanks
        alpino_postags:postag_of_frame(Frame,name,Attrs)
    ;   Pos=noun,
        alpino_postags:postag_of_frame(Frame,pron,Attrs)
    ;   Pos=noun,
        alpino_postags:postag_of_frame(Frame,adv,Attrs)
    ;   Pos=adj,
        alpino_postags:postag_of_frame(Frame,adv,Attrs)
    ;   Pos=adv,
        alpino_postags:postag_of_frame(Frame,adj,Attrs)
    ;   Pos=adv,
        alpino_postags:postag_of_frame(Frame,pp,Attrs)
    ).
    
%%% some frames should only be attempted if certain properties of the
%%% corresponding DT are satisfied and/or certain other tags are present

%% these are *global* checks:
filter_tags :- 
    alpino_filter_tag:initialize_filter_tags,
    (   alpino_cg:clause(lex(Tag,Root,Bc,_,_,_),_,Ref),
        (   alpino_filter_tag:filter_tag(Tag,Root,Bc,Bc)
        ->  true
        ;   erase(Ref)
        ),
        fail
    ;   true
    ).

%% these are *local* checks; there could also be co-occurrences checks.
check_conditions(Frame,Dt,SimpleDt,REL) :-
    findall(Check,condition(Frame,Check),Checks),
    (   \+ apply_checks(Checks,SimpleDt,REL)
    ->  debug_message(2,"frame ~w discarded~n",[Frame])
    ;   true
    ),
    \+ \+ lookup(Frame,Dt).

apply_checks([],_,_).
apply_checks([H|T],Dt,REL) :-
    apply_check(H,Dt,REL),
    apply_checks(T,Dt,REL).

lookup(Frame,Dt) :-
    alpino_lex_types:lex(Cat,Frame,_,Constraints),
    call_constraints(Constraints),
    alpino_data:dt(Cat,Dt).

call_constraints([]).
call_constraints([H|T]) :-
    call_constraint(H),!, % one is enough
    call_constraints(T).

call_constraint(als_word) :-
    alpino_cg:lex(complementizer(als),_,_,_,_,_).

call_constraint(er_word) :-
    er_tag(Tag),
    alpino_cg:lex(Tag,_,_,_,_,_).

er_tag(er_vp_adverb).     % er
er_tag(er_loc_adverb).    % daar hier ergens nergens overal
er_tag(er_wh_loc_adverb). % waar
er_tag(iets_adverb).      % ergens nergens (anders)
er_tag(wh_iets_adverb).   % waar (anders)

condition(postnp_adverb,rel(mod)).
condition(predm_adverb,(rel(dp);rel(predm))).
condition(preposition(_,_,me_adj),obj1_me).
condition(preposition(_,_,pp),not_cat(obj1,np)).
condition(verb(_,_,Sc),Cond) :-
    condition_sc(Sc,Cond).
condition(verb(_,Inf,_),Cond) :-
    condition_infl(Inf,Cond).
condition(v_noun(_),cat(np)).
condition(v_noun(Sc),Cond) :-
    condition_sc(Sc,Cond).
condition(noun(_,_,_,Sc),Cond) :-
    condition_noun_sc(Sc,Cond).
condition(tmp_noun(_,_,_,Sc),Cond) :-
    condition_noun_sc(Sc,Cond).
condition(mod_noun(_,_,_,Sc),Cond) :-
    condition_noun_sc(Sc,Cond).
condition(meas_mod_noun(_,_,_,Sc),Cond) :-
    condition_noun_sc(Sc,Cond).
condition(amount_meas_mod_noun(_,_,_,Sc),Cond) :-
    condition_noun_sc(Sc,Cond).

condition(vandaar_adverb,rel(dp)).
condition(preposition(Prep,_,pc_adv),Cond) :-
    pc_adv_condition(Prep,not_cat(obj1,np),Cond).
condition(preposition(_,_,pc_vp),cat(obj1,ti)).
condition(preposition(_,_,voor_pred),rel(predc)).

pc_adv_condition(NotVan,Cond0,(Cond0,(rel(pc);rel(whd);rel(rhd)))) :-
    NotVan \== van,
    !.
pc_adv_condition(_NotVan,Cond,Cond).

condition_infl(psp,cat(ppart)).
condition_infl(inf,cat(inf)).
condition_infl(inf(no_e),cat(inf)).
condition_infl(inf(e),rel(body)).
condition_infl(imp(_),cat(sv1)).
condition_infl(X,( cat(smain)
                 ; cat(ssub)
                 ; cat(whq)
                 ; cat(sv1)
                 )) :-
    finite_infl(X).

condition_noun_sc(measure,ne_list(mod)).
condition_noun_sc(app_measure,ne_list(app)).
condition_noun_sc(np_app_measure,ne_list(app)).
condition_noun_sc(start_app_measure,ne_list(app)).

condition_sc(inverted_aux(L),Cond) :-
    condition_sc(aux(L),Cond).
condition_sc(so_aux(L),Cond) :-
    condition_sc(aux(L),Cond).
condition_sc(aux(inf),cat(vc,inf)).
condition_sc(aux(te),cat(vc,ti)).
condition_sc(aux(te_inf),( cat(vc,ti)
                         ; cat(vc,inf)
                         )).
condition_sc(aux(psp),cat(vc,ppart)).
condition_sc(aux(_),dt(vc)).
condition_sc(modifier(Sub),Cond) :-
    condition_sc(Sub,Cond).
condition_sc(aux_psp_hebben,(cat(vc,ppart)
                            ;ipp
                            )).                            
condition_sc(aci,cat(vc,inf)).
condition_sc(aci_no_obj,cat(vc,inf)).
condition_sc(passive,cat(vc,ppart)).
condition_sc(aci_simple,(cat(vc,inf),
                         vc_simple)).
condition_sc(aci_simple(_),(dt(vc),
                            vc_simple)).
condition_sc(sbar_obj_opt_het,(cat(vc,cp)
                              ;cat(vc,whsub)
                              )).
condition_sc(sbar,sbar_vc).
condition_sc(np_sbar,sbar_vc).
condition_sc(tr_sbar,sbar_vc).
%condition_sc(copula_sbar,(if_cat(su,cp);if_cat(su,whsub))).  % de vraag is met wie
%condition_sc(so_copula_sbar,(if_cat(su,cp);if_cat(su,whsub))). % de vraag lijkt me met wie
condition_sc(copula_vp,(cat(su,ti)
                       ;cat(su,oti)
                       )).
condition_sc(so_copula_vp,(cat(su,ti)
                          ;cat(su,oti)
                          )).
condition_sc(so_pp_sbar,( sbar_vc,
                          cat(obj2,pp)
                        )).
condition_sc(subj_control(pass_te),cat(vc,ti)).
condition_sc(subj_control(te),cat(vc,ti)).
condition_sc(obj_control(pass_te),cat(vc,ti)).
condition_sc(obj_control(te),cat(vc,ti)).
condition_sc(te_passive,cat(vc,ti)).
condition_sc(sbar_subj_te_passive,cat(vc,ti)).
condition_sc(aan_het,cat(vc,ahi)).
condition_sc(np_aan_het,cat(vc,ahi)).
condition_sc(op,cat(vc,ahi)).
condition_sc(pc_pp(Prep),pc(Prep)).
condition_sc(refl_pc_pp(Prep),(pc(Prep),dt(se))).
condition_sc(np_pc_pp(Prep),pc(Prep)).
condition_sc(uit,cat(vc,ahi)).
condition_sc(van_sbar,cat(vc,svan)).
condition_sc(vp,(  cat(vc,ti)
                ;  cat(vc,oti)
                )).
condition_sc(np_vp_subj,(  cat(vc,ti)
                        ;  cat(vc,oti)
                        )).
condition_sc(np_vp_obj,(  cat(vc,ti)
                       ;  cat(vc,oti)
                       )).
condition_sc(so_vp_obj,(  cat(vc,ti)
                       ;  cat(vc,oti)
                       )).
condition_sc(copula_np,predc_obj).
condition_sc(so_copula_np,predc_obj).
condition_sc(ninv(L,_),Cond) :-
    condition_sc(L,Cond).
condition_sc(incorporated_subj_topic(L),Cond) :-
    condition_sc(L,Cond).
condition_sc(fixed(List,_),Cond) :-
    member(El,List),
    condition_fixed_el(El,Cond).

condition_fixed_el({List},Cond) :-
    member(El,List),
    condition_fixed_el(El,Cond).
condition_fixed_el(vp,dt(vc)).
condition_fixed_el(extra_obj_vp(_,_),dt(vc)).
condition_fixed_el(pc(Prep),pc(Prep)).
condition_fixed_el(er_pp(Prep),pc(Prep)).
condition_fixed_el(er_pp(Prep,_),pc(Prep)).
condition_fixed_el(pred,dt(predc)).
condition_fixed_el(pred(Root),pred_root(Root)).
condition_fixed_el(ap_pred,dt(predc)).
condition_fixed_el(np_pred,dt(predc)).
condition_fixed_el(als_pred,dt(predc)).
condition_fixed_el(subj(Root),su_root(Root)).  % rel?
condition_fixed_el(pp_refl(Voor),(pc(Voor),
                                  pp_refl)).
condition_fixed_el(vc(Root,_,_),svp_root(Root)).
condition_fixed_el(svp_pp(A,B),svp_pp(A,B)).

:- use_module('../Hdrug/Prolog/hdrug_feature').

apply_check(dt_part(Cond),_,_) :-
    alpino_adt:dt_part(DT),
    apply_check(Cond,DT,none).
apply_check(true,_,_).
apply_check(ne_list(Path),Dt,_Rel) :-
    Dt:Path <=> [_|_].
apply_check(dt(Path),Dt,_Rel) :-
    Dt:Path => dt.
apply_check(Path<=>Val,Dt,_Rel) :-
    Dt:Path <=> Val.
apply_check(Path==>Val,Dt,_Rel) :-
    Dt:Path ==> Val.
apply_check(Path=>Val,Dt,_Rel) :-
    Dt:Path => Val.
apply_check((A,B),Dt,Rel) :-
    apply_check(A,Dt,Rel),
    apply_check(B,Dt,Rel).
apply_check((A;_B),Dt,Rel) :-
    apply_check(A,Dt,Rel).
apply_check((_;B),Dt,Rel) :-
    apply_check(B,Dt,Rel).
apply_check(su_root(Val),Dt,_Rel) :-
    su(Dt,Su),
    root(Su,Val).
apply_check(acc_root(Val),Dt,_Rel) :-
    obj1(Dt,Su),
    root(Su,Val).
apply_check(pred_root(Val),Dt,_Rel) :-
    predc(Dt,Su),
    root(Su,Val).
apply_check(vc_root(Val),Dt,_Rel) :-
    vc(Dt,Su),
    root(Su,Val).
apply_check(cat(Rel,Cat),Dt,_Path) :-
    dep_cat(Rel,Cat,Dt).
apply_check(not_cat(Rel,Cat),Dt,_Path) :-
    not_dep_cat(Rel,Cat,Dt).
apply_check(if_cat(Rel,Cat),Dt,_Path) :-
    dep_if_cat(Rel,Cat,Dt).
apply_check(cat(Cat),_Dt,Path) :-
    cat(Cat,Path).
apply_check(rel(Rel),_,Path) :-
    rel(Rel,Path).
apply_check(ipp,Dt,_) :-
    ipp(Dt).
apply_check(vc_simple,Dt,_) :-
    vc_simple(Dt).
apply_check(pc(Prep),Dt,_) :-
    pc(Prep,Dt).
apply_check(predc_obj,Dt,_) :-
    predc(Dt,Predc),
    (   obj1(Predc,Obj1),
        Obj1 => dt
    ;  pobj1(Predc,Obj1),
        Obj1 => dt
    ;  se(Predc,Obj1),
        Obj1 => dt
    ).
apply_check(obj1_me,Dt,_) :-
    obj1(Dt,Obj1),
    me(Obj1,Me),
    Me => dt.
apply_check(pp_refl,Dt,_) :-
    dtpc(Dt,Pc),
    Pc:se => dt.
apply_check(nlex(Path),Dt,_) :-
    Dt:Path <=> Arg,
    Arg:hwrd /=> hwrd.

%% problems......
apply_check(svp_root(V),Dt,_):-
    Dt:svp <=> List,
    member(Part,List),
    root(Part,V).

apply_check(svp_pp(P,N),Dt,_) :-
    Dt:svp <=> List,
    member(PP,List),
    root(PP,Prep),
    prep(Prep,P),
    obj1(PP,NP),
    root(NP,N).   

%% vc is not always there
%% normally cp or whsub
%% but it can also be wh-cat,
%% "hij vroeg me waarom"
%% and coordination as usual
apply_check(sbar_vc,Dt,_) :-
    Dt:vc <=> RelDT,
    (   RelDT => []
    ;   RelDT => dt,
        (  RelDT:conj <=> List,
           member(EmDt,List),
           EmDt:cat ==> Cat
        ;  RelDT:cat ==> Cat
        ),
        \+ (nonvar(Cat), member(Cat,[inf,ti,oti,svan]))
    ).


root(Cmp,Val) :-
    alpino_data:dt(Cmp,Hwrd,_,_,_),
    alpino_data:label(Hwrd,Val,_,_).

su(Dt,Su) :-
    Dt:su <=> Su.
su(Dt,Su) :-
    Dt:su:cnj <=> SuList,
    member(Su,SuList).

predc(Dt,Predc) :-
    Dt:predc <=> Predc.
predc(Dt,Predc) :-
    Dt:predc:cnj <=> PredcList,
    member(Predc,PredcList).

dtpc(Dt,Pc) :-
    Dt:pc <=> Pc.
dtpc(Dt,Pc) :-
    Dt:pc:cnj <=> PcList,
    member(Pc,PcList).

me(Dt,Me) :-
    Dt:me <=> Me.
me(Dt,Me) :-
    Dt:me:cnj <=> MeList,
    member(Me,MeList).

se(Dt,Se) :-
    Dt:se <=> Se.
se(Dt,Se) :-
    Dt:se:cnj <=> SeList,
    member(Se,SeList).

obj1(Dt,Obj1) :-
    Dt:obj1 <=> Obj1.
obj1(Dt,Obj1) :-
    Dt:obj1:cnj <=> Obj1List,
    member(Obj1,Obj1List).

pobj1(Dt,Obj1) :-
    Dt:pobj1 <=> Obj1.
pobj1(Dt,Obj1) :-
    Dt:pobj1:cnj <=> Obj1List,
    member(Obj1,Obj1List).

rel(Rel,[Rel/_|_]).
rel(Rel,[Rel|_]).
rel(Rel,[hd|Path]) :-
    rel(Rel,Path).
rel(Rel,[cnj/_|Path]) :-
    rel(Rel,Path).

cat(Cat,[hd|Path]) :-
    !,
    cat_continuation(Cat,Path).
cat(_,_).

cat_continuation(Cat1,[_/Cat|_]) :-
    (  Cat=Cat1
    ;  Cat=conj  % tja
    ).

finite_infl(sg).
finite_infl(sg1).
finite_infl(sg3).
finite_infl(sg_heeft).
finite_infl(sg_hebt).
finite_infl(pl).
finite_infl(past(_)).
finite_infl(modal_u).
finite_infl(modal_not_u).
finite_infl(modal_inv).
finite_infl(subjunctive).

dep_cat(Rel,Cat,Dt) :-
    Dt:Rel <=> RelDT,
    RelDT => dt,
    (  RelDT:cat ==> conj
    ;  RelDT:cat ==> Cat
    ).

not_dep_cat(Rel,Cat,Dt) :-
    Dt:Rel <=> RelDT,
    RelDT => dt,
    RelDT:cat ==> Cat0,
    Cat0==Cat,
    !,
    fail.
not_dep_cat(_,_,_).

%% if there is a Rel, then it must have Cat
dep_if_cat(Rel,Cat,Dt) :-
    Dt:Rel <=> RelDT,
    (   RelDT => []
    ;   RelDT => dt,
        (   RelDT:cat ==> conj
        ;   RelDT:cat ==> Cat
        )
    ).

ipp(Dt) :-
    vc(Dt,VC),
    VC:cat ==> inf,
    vc(VC,SubVC),
    ( SubVC:cat ==> inf
    ; SubVC:cat ==> ti
    ).

vc(Dt,VC) :-
    Dt:vc <=> VC,
    VC => dt.
vc(Dt,VC) :-
    Dt:vc <=> CONJ,
    CONJ:conj <=> List,
    member(VC,List).

vc_simple(Dt) :-
    vc(Dt,VC),
    VC:vc => [].

pc(Prep,Dt) :-
    Dt:pc <=> PC,
    pc_prep(Prep,PC).

pc(Prep,Dt) :-
    Dt:pc <=> PC,
    PC:conj <=> List,
    member(El,List),
    pc_prep(Prep,El).

pc_prep(Prep,DT) :-
    root(DT,Prep0),
    prep(Prep,Prep0).

prep([H|T],Root) :-
    concat_all([H|T],Root,' ').
prep(uit_op,_).
prep(met,mee).
prep(tot,toe).
prep(van,af).
prep(Prep,Prep).
prep(Prep,ErPrep) :-
    atom(Prep),
    member(Er,[er,daar,hier,waar]),
    ( atom_concat(Er,Prep,ErPrep)
    ; atom_concat(Er,toe,ErPrep)
    ; atom_concat(Er,mee,ErPrep)
    ; atom_concat(Er,af,ErPrep)
    ).

unknown_root_heuristics([Fr0|Fr],[Fr0|Fr],_Root,_Sense,_Pos,_Attr).
unknown_root_heuristics([],Frames,Root,Sense,Pos,Attr) :-
    findall(Frame-Surf,
            unknown_root_heuristic(Pos,Root,Sense,Attr,Frame,Surf),Frames0),
    sort(Frames0,Frames).

unknown_root_heuristic(noun,Root,_,Attr,noun(Gen,both,Num),Surfs) :-
    \+ sub_atom(Root,_,1,_,'_'),
    (   member(gen=Gen,Attr)
    ->  true
    ;   Gen=both
    ),
    (   member(num=Num,Attr)
    ->  true
    ;   Num=both
    ),
    findall(Surf,realize_surf(Root,Surf),Surfs).


unknown_root_heuristic(name,Root,_,Attr,FRAME,Surfs) :-
    (   member(neclass=TYPE,Attr)
    ->  FRAME=proper_name(Num,TYPE)
    ;   FRAME=proper_name(Num)
    ),
    (   member(num=Num,Attr)
    ->  true
    ;   Num=both
    ),
    findall(Surf,realize_surf(Root,Surf),Surfs).

unknown_root_heuristic(prefix,Root,Root,[],within_word_conjunct,[RootDash]) :-
    atom_concat(Root,'-',RootDash).

unknown_root_heuristic(adj,Root,Root,Attrs,adjective(no_e(adv)),[Root]) :-
    alpino_postags:postag_of_frame(adjective(no_e(adv)),adj,CheckAttr),
    check_attributes(CheckAttr,Attrs).

unknown_root_heuristic(verb,Root,Root,Attrs,Tag,[Root]) :-
    verb_tag(Tag),
    alpino_postags:postag_of_frame(Tag,verb,CheckAttr),
    check_attributes(CheckAttr,Attrs).

unknown_root_heuristic(verb,Root,Root,Attrs,verb(HZ,subjunctive,Sc),[Surf]) :-
    lists:member(infl=subjunctive,Attrs),
    dict_entry(Root,verb(HZ,inf,Sc),Inf),
    atom_concat(Inf0,en,Inf),
    atom_concat(Inf0,e,Surf),
    alpino_postags:postag_of_frame(verb(HZ,subjunctive,Sc),verb,CheckAttr),
    check_attributes(CheckAttr,Attrs).

unknown_root_heuristic(Pos,Root,_,Attr,Frame,Surfs) :-
    atom_concat(Prefix,Rest,Root),
    atom_concat(Pref,'_',Prefix),
    pos2frames_aux(Rest,__Sense,Pos,Attr,Frame,Surfs0),
    add_prefixes(Surfs0,Pref,Surfs).

unknown_root_heuristic(Pos,Root,_,Attr,Frame,Surfs) :-
    exc_pos2frames_aux(Root,Pos,Attr,Frame,Surfs).

verb_tag(verb('hebben/zijn',inf,intransitive)).
verb_tag(verb('hebben/zijn',inf,transitive)).

exc_pos2frames_aux(Root,Pos,Attr,Frame,Surfs) :-
    setof(Surf,dict_entry(Root,Frame,Surf),Surfs),
    exc_postag_of_frame(Frame,Pos,CheckAttr),
    check_attributes(CheckAttr,Attr).

add_prefixes([],_,[]).
add_prefixes([H|T],Pref,Results) :-
    findall(Form,add_prefix(H,Pref,Form),Results,Results1),
    add_prefixes(T,Pref,Results1).

add_prefix(H,Pref,Surf) :-
    atom_concat(Pref,'_',Pref2),
    atom_concat(Pref2,H,Pref3),
    realize_surf(Pref3,Surf).

final_heuristics([Fr0|Fr],[Fr0|Fr],_Root,_Sense,_Pos,_Attr).
final_heuristics([],Frames,Root,Sense,Pos,Attr) :-
    findall(Frame-Surf,final_heuristic(Pos,Root,Sense,Attr,Frame,Surf),Frames).

final_heuristic(Pos,Root,_,Attrs,Frame,[MarkedRoot]) :-
    atom_concat('***',Root,MarkedRoot),
    open_class(Frame),
    alpino_postags:postag_of_frame(Frame,Pos,CheckAttr),
    check_attributes(CheckAttr,Attrs),
    debug_message(1,"guessing|final heuristic|~w|~w~n",[Root,Frame]).

open_class(adverb).
open_class(modal_adverb).
open_class(tmp_adverb).
open_class(noun(both,both,both)).
open_class(proper_name(both)).
open_class(adjective(no_e(adv))).
open_class(adjective(e)).
open_class(number(hoofd(pl_num))).
open_class(verb('hebben/zijn',Infl,intransitive)) :-
    infl(Infl).
open_class(verb('hebben/zijn',Infl,transitive)) :-
    infl(Infl).
open_class(verb('hebben/zijn',Infl,refl)) :-
    infl(Infl).

infl(sg3).
infl(past(sg)).
infl(past(pl)).
infl(pl).
infl(inf).
infl(psp).


%final_heuristic(adv,Root,_,[],adverb,[MarkedRoot]) :-
%    atom_concat('**',Root,MarkedRoot). % make problem explicit


last_resort_heuristics([Fr0|Fr],[Fr0|Fr],_Root,_Sense,_Pos,_Attr).
last_resort_heuristics([],Frames,Root,Sense,Pos,Attr) :-
    findall(Frame-Surf,last_resort_heuristic(Pos,Root,Sense,Attr,Frame,Surf),Frames).

last_resort_heuristic(Pos,Root,_,Attr,Frame,Surfs):-
    set_flag(current_input_sentence,[Root]),
    alpino_lexical_analysis:lexical_analysis_cleanup,
    alpino_lexical_analysis:add_word_forms([Root]),
    alpino_lexical_analysis:guess_names([Root]),
    alpino_lexical_analysis:guess_unknowns([Root],0),
    alpino_lexical_analysis:tag(_,_,_,_,Root,__Root,_,Frame),
    alpino_postags:postag_of_frame(Frame,Pos,CheckAttr),
    check_attributes(CheckAttr,Attr),
    findall(Surf,realize_surf(Root,Surf),Surfs).

last_resort_heuristic(noun,Root,_,Attr,noun(Gen,both,Num),Surfs) :-
    (   member(gen=Gen,Attr)
    ->  true
    ;   Gen=both
    ),
    (   member(num=Num,Attr)
    ->  true
    ;   Num=both
    ),
    findall(Surf,realize_surf(Root,Surf),Surfs).

%% get rid of meta '_'
realize_surf(Root,Surf) :-
    atom_codes(Root,RootCodes),
    realize_surf_codes(RootCodes,SurfCodes),
    atom_codes(Surf,SurfCodes).

realize_surf_codes([],[]).
realize_surf_codes([H|T],Result) :-
    realize_surf_codes(T,H,Result).

realize_surf_codes([],H,[H]).
realize_surf_codes([NH|T],H,Result) :-
    realize_surf_codes(T,H,NH,Result).

realize_surf_codes([],A,B,[A,B]).
realize_surf_codes([H|T],A,B,Result) :-
    (   B =:= 95                % "_"
    ->  (   H=68,
            T = [73,77|T2]      % "DIM"
        ->  dim(A,Suffix),
            append(Suffix,NResult,Result),
            realize_surf_codes(T2,106,101,NResult)
        ;   Result = [A|NResult],
            (   islower(A),
                islower(H),
                realize_surf_codes(T,H,NResult)
            ;   allows_s(A), 
                islower(H),
                realize_surf_codes(T,115,H,NResult) % "s"
            ;   realize_surf_codes(T,45,H,NResult) % "-"
            )
        )
    ;   Result = [A|NResult],
        realize_surf_codes(T,B,H,NResult)
    ).

dim(X,Suffix) :-
    (   islower(X)
    ->  dim1(X,Suffix)
    ;   Suffix=[X]
    ).

allows_s(100). % d
allows_s(101). % e  spruitjes
allows_s(102). % f
allows_s(103). % f
allows_s(107). % k
allows_s(108). % l
allows_s(109). % m
allows_s(110). % n
allows_s(112). % p
allows_s(114). % r
allows_s(116). % t

dim1(X,Suffix) :-
    (   (  X =:= 97   % a
        ;  X =:= 101  % e
        ;  X =:= 105  % i
        ;  X =:= 106  % j
        ;  X =:= 108  % l
        ;  X =:= 110  % n
        ;  X =:= 111  % o
        ;  X =:= 114  % r
        ;  X =:= 117  % u
        ;  X =:= 119  % w
        ;  X =:= 121  % y
        )
    ->  Suffix = [X,116]
    ;   X =:= 109     % m
    ->  Suffix = [X,112]
    ;   Suffix = [X]
    ).
    
                             

%% todo: add couple of noun adjective and verb frames per default?

check_missing_roots([]).
check_missing_roots([H-C|T]) :-
    (   alpino_cg:lex(_,_,C1,_,_,_),
        C /\ C1 =\= 0
    ->  true
    ;   format(user_error,"warning: missing lex for root ~w~n",[H])
    ),
    check_missing_roots(T).

