<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exslt="http://exslt.org/common">

  <xsl:output method="text" encoding="UTF-8"/>

  <xsl:param name="filename"/>

  <xsl:template match="/">
    <xsl:call-template name="print-mods"/>
  </xsl:template>

  
  <xsl:template name="print-mods">
    <xsl:for-each select="//node[@rel='hd' and (@pos='noun' or @pos='name')]">
    <xsl:variable name="nounpos" select="number(@begin)" />
    <xsl:if test="count(../node[@root and @rel='mod' and number(@begin) &lt; $nounpos]) > 0">
      <xsl:for-each select="../node[@root and @rel='mod' and number(@begin) &lt; $nounpos]">
        <xsl:value-of select="@word" />
        <xsl:text> </xsl:text>
      </xsl:for-each>
      <xsl:value-of select="@word" />
      <xsl:text>
      </xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>


