#!/usr/bin/python

#
# Counts to Prolog output
#
# The Prolog facts will form the gold standard. 
#

import sys

for line in sys.stdin:
  line = line.replace("'", "\\'")
  lineParts = line.strip().split()
  count = int(lineParts[0])
  noun = lineParts[-1]
  mods = lineParts[1:-1]

  print "head_mod('%s',['%s'],%d)." % (noun, "','".join(mods), count)
