#!/usr/bin/python

#
# Counts of head nouns and modifier
#

import sys

headMods = {}

for line in sys.stdin:
	lineParts = line.strip().split()
	count = int(lineParts[0])
	noun = lineParts[-1]
	mod  = lineParts[-2]
	f = headMods.get((noun, mod), 0)
	headMods[(noun, mod)] = f + count

for ((noun, mod), count) in headMods.iteritems():
	print "%s %s %d" % (noun, mod, count)
