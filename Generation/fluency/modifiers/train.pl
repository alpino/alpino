:- use_module([head_mod, closest_modifiers]).

%% feature(Noun,RevMods,Feature)

train :-
    findall(head_mod(Noun,Mods),head_mod(Noun,Mods,_),Insts),
    train(Insts).

train([]).
train([head_mod(Noun,Mods)|Tail]) :-
    features(Noun,Mods,Fs),
    format("~q~n",[Fs]),
    train(Tail).

features(Noun,Mods,Features) :-
    lists:reverse(Mods,RevMods),
    findall(F,feature(Noun,RevMods,F),Features).

feature(Noun,[Mod|_],close_mod-N) :-
    (   closest_modifier(Noun,Mod,F)
    ->  N = F
    ;   N = 0
    ).

feature(_,Mods,mod_len(Idx)-Len) :-
    lists:nth(Idx,Mods,Mod),
    atom_length(Mod,Len).

feature(_,Mods,hyphen(Idx)-Bool) :-
    lists:nth(Idx,Mods,Mod),
    atom_chars(Mod,Chars),
    (   lists:memberchk(45,Chars) %% Other hyphens?
    ->  Bool = 1
    ;   Bool = 0
    ).