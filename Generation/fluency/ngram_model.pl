:- module(ngram_model, [fluency_model_initialize,
			sentence_fluency/2,
			phrase_fluency/2]).

:- use_module(library(lists)).
:- use_module(['../../fadd/pro_fadd']).

fluency_model_initialize.

initialize_unigram_dict(No) :-
    hdrug_util:hdrug_flag(initialized_unigram_probs,UniInit),
    initialize_unigram_dict(UniInit,No).
initialize_unigram_dict(undefined,No) :-
    hdrug_util:hdrug_flag(ngram_model_words,WordsFile),
    hdrug_util:hdrug_flag(ngram_model_unigrams,File),
    pro_fadd:init_tuple([File,WordsFile],No),
    hdrug_util:debug_message(1,"initialized unigram_probs ~w (~w)~n",[File,No]),
    hdrug_util:set_flag(initialized_unigram_probs,initialized(No)).
initialize_unigram_dict(initialized(No),No).

initialize_bigram_dict(No) :-
    hdrug_util:hdrug_flag(initialized_bigram_probs,Init),
    initialize_bigram_dict(Init,No).
initialize_bigram_dict(undefined,No) :-
    hdrug_util:hdrug_flag(ngram_model_words,WordsFile),
    hdrug_util:hdrug_flag(ngram_model_bigrams,File),
    pro_fadd:init_tuple([File,WordsFile,WordsFile],No),
    hdrug_util:debug_message(1,"initialized bigram_probs ~w (~w)~n",[File,No]),
    hdrug_util:set_flag(initialized_bigram_probs,initialized(No)).
initialize_bigram_dict(initialized(No),No).

initialize_trigram_dict(No) :-
    hdrug_util:hdrug_flag(initialized_trigram_probs,Init),
    initialize_trigram_dict(Init,No).
initialize_trigram_dict(undefined,No) :-
    hdrug_util:hdrug_flag(ngram_model_words,WordsFile),
    hdrug_util:hdrug_flag(ngram_model_trigrams,File),
    pro_fadd:init_tuple([File,WordsFile,WordsFile,WordsFile],No),
    hdrug_util:debug_message(1,"initialized trigram_probs ~w (~w)~n",[File,No]),
    hdrug_util:set_flag(initialized_trigram_probs,initialized(No)).
initialize_trigram_dict(initialized(No),No).

sentence_fluency(Sentence0,P) :-
    lists:append(Sentence0,['<END>'],Sentence),
    hdrug_util:debug_message(2,"~n~w~n",[Sentence]),
    sentence_fluency_aux(Sentence,['<START>','<START>'],0,P),
    hdrug_util:debug_message(2,"~n~w~n",[P]).

%% Calculate probability without using sentence boundaries.
phrase_fluency([W1,W2,W3|Rest],P) :-
    !,
    hdrug_util:debug_message(2,"~n~w~n",[W1,W2,W3|Rest]),
    sentence_fluency_aux([W3|Rest],[W1,W2],0,P),
    hdrug_util:debug_message(2,"~n~w~n",[P]).

sentence_fluency_aux([],_,P,P).
sentence_fluency_aux([Head|Tail],PrevNgram,P0,P) :-
    length(PrevNgram,Length),
    (	Length==3
    ->	[_|PrevTail] = PrevNgram,
        lists:append(PrevTail,[Head],Ngram)
    ;	lists:append(PrevNgram,[Head],Ngram)
    ),
    trigram_prob(Ngram,NgramP),
    hdrug_util:debug_message(2," ~w",[NgramP]),
    P1 is NgramP + P0,
    sentence_fluency_aux(Tail,Ngram,P1,P).

trigram_prob([T1,T2,T3],P) :-
    ngram_prob([T3],UnigramP),
    ngram_prob([T2,T3],BigramP),
    ngram_prob([T1,T2,T3],TrigramP),
    log_add(UnigramP,BigramP,P0),
    log_add(P0,TrigramP,P).

ngram_prob([W],P) :-
    initialize_unigram_dict(Dict),
    pro_fadd:word_tuple_grams([W],Dict,[P|_]),
    !.
ngram_prob([_],P) :-
    initialize_unigram_dict(Dict),
    pro_fadd:word_tuple_grams(['<unknown_word>'],Dict,[P|_]),
    !.
    
ngram_prob([W1,W2],P) :-
    initialize_bigram_dict(Dict),
    pro_fadd:word_tuple_grams([W1,W2],Dict,[P|_]),
    !.

ngram_prob([W1,W2,W3],P) :-
    initialize_trigram_dict(Dict),
    pro_fadd:word_tuple_grams([W1,W2,W3],Dict,[P|_]),
    !.

ngram_prob(_,1000).

min(X,Y,X) :-
    X < Y,
    !.
min(_,Y,Y).

max(X,Y,X) :-
    X > Y,
    !.
max(_,Y,Y).

log_add(X,Y,Sum) :-
    (   A is Y + 70, X > A ->  Sum = Y
    ;   B is X + 70, Y > B ->  Sum = X
    ;   min(X,Y,Min),
	max(X,Y,Max),
	Sum is Min - log(exp(X - Max) + exp(Y - Max))
    ).
