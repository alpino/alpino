#!/usr/bin/python
#
# Pick random realizations, and calculate the average ROUGE-SU score.
#

import random
import re
import sys

def processSents(fh):
    prevSent = 0
    sentScores = []
    randScores = []

    for line in fh:
        line = line.strip()

        sentMatch = re.match(r'G#([0-9]+)#', line)
        if sentMatch == None:
            continue

        sent = sentMatch.group(1)
        if sent != prevSent:
            if len(sentScores) != 0:
                randScore = sentScores[random.randrange(len(sentScores))]
                randScores.append(randScore)
            prevSent = sent
            sentScores = []

        score = float(line.split('#')[3])
        sentScores.append(score)

    return randScores

if __name__ == '__main__':
    randScores = processSents(sys.stdin)
    sum = reduce(lambda x, y: x + y, randScores)
    print sum / len(randScores)
