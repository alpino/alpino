# Bad habits? ;)
def endCurry(func, *curried):
    return lambda *args: func(*(args + curried))

def readFeatureWeights(fh):
    featureWeights = dict()

    for line in fh:
        (feature, weight) = line.strip().rsplit('|', 1)

        featureWeights[feature] = -float(weight)

    return featureWeights

def featureValues(featurePairs):
    vals = dict()

    for pair in featurePairs:
        (val, feature) = pair.split('@', 1)

        vals[feature] = float(val)

    return vals

def scoreSent(featureWeights, featureValues):
    score = 0.0

    for pair in featureValues.items():
        weight = featureWeights.get(pair[0], 0.0)
        score += weight * pair[1]

    return score

def scoreSents(sents, featureWeights):
    scoredSents = []
    for sent in sents:
        sentParts = sent.split('#')
        featurePairs = sentParts[4].split(r'|')
        vals = featureValues(featurePairs)
        score = scoreSent(featureWeights, vals)
        scoredSents.append((score, vals['ngram_lm'], vals['ngram_tag'], 0.0, float(sentParts[3]),sentParts[2]))

    return scoredSents

def sentCmp(x, y, scoreField):
    if (x[scoreField] > y[scoreField]):
        return 1
    if (x[scoreField] < y[scoreField]):
        return -1
    else:
        # We can't pick one based on the score. This seems not to be
        # completely fair, consider the case where s2 is the correct
        # sentence, and the results of ranking are:
        #
        # 0.5 s1
        # 0.5 s2
        # [...]
        #
        # On the other hand, if we judge this as a correct match, the
        # evaluation can be cheated easily by giving all the realizations
        # the same score (see the commented return statement).
        return 0

        #return cmp(y[2], x[2])

def evalScoredSents(scoredSents, scoreField = 0, inverse = False):
    cmpFun0 = sentCmp
    cmpFun0 = endCurry(cmpFun0, scoreField)
    if inverse:
        # Hmmm, this is strange, if we build cmpFun from cmpFun (rather
        # than the auxiliary cmpFun0), we get an infinite recursion. But
        # apparently the currying above is fine...
        cmpFun = lambda x, y: -cmpFun0(x, y)
    else:
        cmpFun = cmpFun0
    scoredSents = sorted(scoredSents, cmpFun)

    return scoredSents[0][4]

def sortScoredSents(scoredSents, scoreField = 0, inverse = False):
    cmpFun0 = sentCmp
    cmpFun0 = endCurry(cmpFun0, scoreField)
    if inverse:
        # Hmmm, this is strange, if we build cmpFun from cmpFun (rather
        # than the auxiliary cmpFun0), we get an infinite recursion. But
        # apparently the currying above is fine...
        cmpFun = lambda x, y: -cmpFun0(x, y)
    else:
        cmpFun = cmpFun0
    scoredSents = sorted(scoredSents, cmpFun)

    return scoredSents
