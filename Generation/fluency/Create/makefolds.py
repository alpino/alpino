#!/usr/bin/python

import optparse
import re
import sys
import gzip

def split(fh, parts, files):
    count = 0
    prevSen = '#NONE#'

    for line in fh:
        match = re.search(r'^[GP]#([^#]+)', line)
        if match == None:
            continue
        sen = match.group(1)

        if prevSen != sen:
            count += 1
            if count > parts:
                count = 1
            prevSen = sen

        print >> files[count - 1], line,

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option("-z", "--gzip", action = "store_true", dest = "gzip",
                      default = False, help = "compress input/output")
    (options, args) = parser.parse_args()

    if len(args) != 4:
        print "Usage: %s [-z] parts prefix suffix filename"
        sys.exit(1)

    openFun = gzip.open if options.gzip else open

    parts = int(args[0])

    fh = openFun(args[3])

    files = []
    for i in range(1, parts + 1):
        newf = openFun("%s%d%s" % (args[1], i, args[2]), r'w')
        files.append(newf)

    split(fh, parts, files)
