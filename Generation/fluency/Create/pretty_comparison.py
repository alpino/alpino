#!/usr/bin/python
#
# Pretty print comparisons
#

import gzip
import sys

def readComparisons(fh):
    rels = dict()

    for line in fh:
        lineParts = line.strip().split()
        rels[lineParts[0]] = lineParts[1:]

    return rels

def hasUnique(seq):
    return len(list(set(seq))) != 1

def processSents(fh, comparisons):
    prevKey = 'NONE'
    rels = dict()

    for line in fh:
        lineParts = line.strip().split('|')
        key = lineParts[0][2:]

        #print key, prevKey, lineParts

        if key != prevKey:
            if prevKey in comparisons.keys() and hasUnique(comparisons[prevKey]):
                comp = comparisons[prevKey]
                print "Best: %s|%s|%s" % (prevKey, comp[0],
                                          rels[comp[0]][1])
                for i in range(1, len(comp)):
                    print "Model%d: %s|%s|%s" % (i, prevKey, comp[i],
                                                 rels[comp[i]][1])
                print
            rels = dict()
            prevKey = key

        if key in comparisons.keys():
            rels[lineParts[1]] = (lineParts[2], lineParts[3])


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "Usage: %s compare realizations" % sys.argv[0]
        sys.exit(1)


    cmpHandle = open(sys.argv[1])
    comps = readComparisons(cmpHandle)

    relsHandle = gzip.open(sys.argv[2])
    processSents(relsHandle, comps)
