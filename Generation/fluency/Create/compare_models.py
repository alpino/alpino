#!/usr/bin/python

import optparse
import random
import re
import sys

import maxent

# List element shuffling. We have random.shuffle, but it is in-place
# and does not seem to have such good properties. This should do for
# our purposes.
def shuffle(x):
	n = len(x)
	shuffled = list() 
	indicesSet = set()

	while len(shuffled) != n:
		pick = int(random.random() * n)
		if not pick in indicesSet:
			shuffled.append(x[pick])
			indicesSet.add(pick)

	return shuffled

def printCompare(models, prevSent, sents):
      printedCorrect = False
      print "%s" % prevSent,
      for model in models:
            scoredSents = maxent.scoreSents(sents, model)
            if not printedCorrect:
                  print maxent.sortScoredSents(scoredSents, 4, True)[0][5],
                  printedCorrect = True
            print maxent.sortScoredSents(scoredSents)[0][5],
      print


def processSents(fh, models, minRealizations):
      prevSent = 'NONE'
      sents = []

      for line in fh:
            line = line.strip()
        
            sentMatch = re.match(r'G#([^#]+)#', line)
            if sentMatch == None:
                  continue

            sent = sentMatch.group(1)
            if sent != prevSent:
                  if len(sents) >= minRealizations and prevSent != 'NONE':
                        sents = shuffle(sents)
                        printCompare(models, prevSent, sents)

                  prevSent = sent
                  sents = []
            
            sents.append(line)

      # Leftover
      printCompare(models, prevSent, sents)

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option("-r", "--realizations", dest = "realizations",
		      default = "1", help = "Minimum number of realizations")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print "Usage: %s feature_weights0 .. feature_weightsN" % sys.argv[0]
        sys.exit(1)

    # Make results reproducable.
    random.seed(13)

    minRealizations = int(options.realizations)

    models = list()
    for filename in args:
        fh = open(filename, 'r')
        models.append(maxent.readFeatureWeights(fh))

    processSents(sys.stdin, models, minRealizations)

