#!/usr/bin/python
#
# Read 'sfr' input and produce a sequence of frames, one sentence per
# line.
#

import sys

if __name__ == '__main__':
    curId = ''
    frames = list()

    for line in sys.stdin:
        lineParts = line.strip().split(r'|')

        if len(lineParts) != 7:
            continue # Ignore

        lineId = lineParts[2]
        if curId != lineId:
            if curId != '':
                print ' '.join(frames)
                frames = list()

            curId = lineId

        frames.append(lineParts[1])

    # Flush
    print ' '.join(frames)
