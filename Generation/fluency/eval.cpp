#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>

#include "../../fadd/fadd.h"
#include "lm.h"

using namespace std;

list_of_words *arr2low(char const * const *words, size_t n)
{
  list_of_words *head = 0;
  list_of_words *prev = 0;

  for (size_t i = 0; i < n; ++i) {
    if (head == 0) {
      head = new list_of_words;
      prev = head;
    }
    else {
      list_of_words *cur = new list_of_words;
      prev->next = cur;
      prev = cur;
    }
    prev->word = words[i];
  }

  prev->next = 0;

  return head;
}

double sentence_fluency(vector<string> const &sentence)
{
  vector<string> words(2, START_MARKER);
  copy(sentence.begin(), sentence.end(), back_inserter(words));
  words.push_back(END_MARKER);

  return fluency(words);
}

double processCorpus(istream &corpusStream)
{
  double logProbSum = 0.0;
  size_t tokens = 0;

  string line;
  while (getline(corpusStream, line))
  {
    istringstream lineStream(line);
    vector<string> sentence;
    copy(istream_iterator<string>(lineStream), istream_iterator<string>(),
	 back_inserter(sentence));

    logProbSum += sentence_fluency(sentence);
    tokens = sentence.size() + 3; // Start/end tokens
  }

  // Shannon-McMillan-Breiman theorem -> for a stationary ergodic
  // process: H(p,m) = lim_{n->inf} - 1/n log m(w1,w2...wn)
  return logProbSum / tokens;
}

int main(int argc, char *argv[])
{
  if (argc < 6) {
    cerr << "Usage: " << argv[0] << " words unigrams bigrams trigrams corpus"
	 << endl;
    return 1;
  }

  initLib();

  char const * const unigramFiles[] = {argv[2], argv[1]};
  char const * const bigramFiles[] = {argv[3], argv[1], argv[1]}; 
  char const * const trigramFiles[] = {argv[4], argv[1], argv[1], argv[1]};

  list_of_words *unigramDictList = arr2low(unigramFiles,2);
  list_of_words *bigramDictList = arr2low(bigramFiles,3);
  list_of_words *trigramDictList = arr2low(trigramFiles,4);

  initLM(unigramDictList, bigramDictList, trigramDictList);

  ifstream corpusStream(argv[5]);
  if (!corpusStream) {
    cerr << "Error opening the corpus file!" << endl;
    return 1;
  }

  cout << "Cross-entropy: " << processCorpus(corpusStream) << endl;

  deleteListOfWords(unigramDictList);
  deleteListOfWords(bigramDictList);
  deleteListOfWords(trigramDictList);

  deinitLM();
}
