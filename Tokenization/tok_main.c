#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <errno.h>
#include <wchar.h>
#define MAXLINE 131072        /* this constrains line length */

int t_accepts(wchar_t *in,wchar_t *out,int max);

int main(int argc, char *argv[]) {
  wchar_t in[MAXLINE];
  wchar_t out[MAXLINE];
  int return_value;
  wchar_t *ptr;

  setlocale(LC_ALL,"");

  errno = 0;
  ptr = fgetws(in,MAXLINE,stdin);
  while(ptr != NULL) {

    wchar_t *p = wcschr(in, L'\n');
    if (p) { *p = L'\0'; }  /* chomp */

    return_value = t_accepts(in,out,MAXLINE);
    switch(return_value) {
    case 0:
      fprintf(stderr,"ERROR: Could not tokenize %S\n",in);
      break;
    case 1:
      if (wcslen(out)>0)
	printf("%S\n",out);
      break;
    case 2:
      fprintf(stderr,"ERROR: Could not tokenize %.60S\nbecause output line too long\n",in);
      break;
    }
    errno = 0;
    ptr = fgetws(in,MAXLINE,stdin);
  }
  if (errno) {
    fprintf(stderr,"%s error: %s\n",argv[0],strerror(errno));
  }

                                         /* end-of-file */
  return 0;
}

