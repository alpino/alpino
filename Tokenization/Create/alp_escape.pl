:- multifile macro/2.
:- multifile rx/2.

:- ensure_loaded(eacl99).

%%% escape \ [ ] characters with backslash
%%% for Alpino

macro(alp_escape, replace({ '\\':['\\','\\'],
			    '[':['\\','['],
			    ']':['\\',']']
			   })).
