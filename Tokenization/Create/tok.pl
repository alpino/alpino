:- multifile macro/2.
:- multifile rx/2.

%%% tokenizer
%%% input: every paragraph is on a single line
%%% output: every sentence is on a single line; word-tokens are
%%%         separated by a single space


:- ensure_loaded(abbrs).

macro(breaks,

      replace(sep:control(p),['(',sep,{word,num}+],[])

                    o

%%% don't break if followed by tlq tldq punctuation followed by .!? or ,
     
      replace(sep:break,
	      [sep,eos,[sep,{sq,dq,rq,trq,trdq,')'}]^],
	      {`{sq,dq,rq,trq,trdq,komma,eos,';',':',
                 sluithaak,lower,'&','-','+',tlq,tldq}, 
	       [lower,`lower],
               [{tlq,tldq},sep,`{eos,komma}],
               [{tlq,tldq},`sep],
               [{'-'},sep,upper]}
	     )
                    o

       replace(control(p):sep)


     %  ( dat is een feit ) Ik ...  ==>
     %  ( dat is een feit )
     %  Ik

% machine gets too big
%       [sep*, '(',(? - ')')*,')',sep:break,upper,?*]
     
     ).


macro(tok,
      file('tokenize_before_breaks.m')
         o
      breaks
         o
      file('tokenize_after_breaks.m')
     ).

macro(tok_no_breaks,
      file('tokenize_before_breaks.m')
         o
      file('tokenize_after_breaks.m')
      ).

