:- multifile macro/2.
:- multifile rx/2.

:- ensure_loaded(eacl99).

macro(eos,    {'!',escape(?),'.'}).
macro(komma,  ',').
macro(sepchar,{',','(',')',':',';','*',quotes,'&',eos,'°','/','♥','♡','☹','☺' }).
macro(lower,  {a..z,á,ä,à,é,ë,è,í,ì,ï,ü,ù,ú,ó,ò,ö}).
macro(upper,  {'A'..'Z','Á','À','Ä','É','È','Ë','Í','Ì','Ï','Ó','Ò','Ö',
	      'Ú','Ù','Ü'}).
macro(word,   {upper,lower}).
macro(num,    {'0','1','2','3','4','5','6','7','8','9'}).
macro(quotes, {dq,sq,lq,rq,tlq,tldq,trq,trdq}).
macro(dq,     '"'). % "	
macro(sq,     '\'').
macro(lq,     {'‘','`','«'}).
macro(rq,     {'»','’'}).
macro(tlq,    control(q)).
macro(tldq,   control(r)).
macro(trq,    control(t)).
macro(trdq,   control(u)).
macro(sep,    ' ').
macro(openhaak,'(').
macro(sluithaak,')').
%macro(break,  control(j)).

macro(break, control(j)).

macro(space,{' ',control(i),' ','﻿'}).
/*  The third space character is:


        character:   (160, #o240, #xa0)
preferred charset: iso-8859-1 (Latin-1 (ISO/IEC 8859-1))
       code point: 0xA0
           syntax: . 	which means: punctuation
         category: .:Base, b:Arabic, j:Japanese, l:Latin
      buffer code: #xC2 #xA0
        file code: #xC2 #xA0 (encoded by coding system utf-8-unix)
          display: by this font (glyph code)
    xft:-unknown-DejaVu Sans Mono-normal-normal-normal-*-17-*-*-*-m-0-iso10646-1 (#x62)
   hardcoded face: nobreak-space

Character code properties: customize what to show
  name: NO-BREAK SPACE
  old-name: NON-BREAKING SPACE
  general-category: Zs (Separator, Space)
  decomposition: (noBreak 32) (noBreak ' ')

There are text properties here:
  face                 (font-lock-string-face)
  fontified            t
  rear-nonsticky       t
*/

/* the fourth space character is:
        character: ﻿ (65279, #o177377, #xfeff)
preferred charset: unicode (Unicode (ISO10646))
       code point: 0xFEFF
           syntax: w 	which means: word
      buffer code: #xEF #xBB #xBF
        file code: #xEF #xBB #xBF (encoded by coding system utf-8-with-signature-unix)
          display: by this font (glyph code)
    xft:-unknown-DejaVu Sans Mono-normal-normal-normal-*-17-*-*-*-m-0-iso10646-1 (#xBEC)

Character code properties: customize what to show
  name: ZERO WIDTH NO-BREAK SPACE
  old-name: BYTE ORDER MARK
  general-category: Cf (Other, Format)
*/

macro(control(Letter), Atom ) :-
    atom_codes(Letter,[C|_]),
    D is C-96,
    atom_codes(Atom,[D]).
