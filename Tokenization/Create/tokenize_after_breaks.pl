:- multifile macro/2.
:- multifile rx/2.

:- ensure_loaded(abbrs).

macro(tokenize_after_breaks,
      {[sep:break, ?*, sep:break ],[sep x [break,break]]}
         o
      initial_dash
         o
      replace(sep:[],[num,u],'.')
         o
      replace(sep:[],[{break,sep},afkorting_after],['.',{break,sep}])
         o
      replace(sep:[],[{break,sep},ambiguous_afkorting_after],['.',sep,{word,num}])
         o
      {`{tlq,tldq,trq,trdq},tldq:dq,tlq:sq,trq:sq,trdq:'"'}*
         o
      hellip
         o
     replace([sep+]:sep)
         o
     [break:[], ?*, break:[]]
     ).

%% -Waarom kom je ? ==> - Waarom kom je ?
macro(initial_dash,
      replace([]:sep,[break,'-'], upper)).

%% . . . ==> ...
macro(hellip,replace(sep:[],eos,eos)).

macro(ambiguous_afkorting_after,
      { file('words_ambiguous_afkorting_after.m'),
      [word+,s,z],   % Pieter Claesz. de Ruiter
      [num]
      }).

macro(afkorting_after,file('words_afkorting_after.m')).

