
:- multifile macro/2.
:- multifile rx/2.

%%% tokenizer
%%% input: every paragraph is on a single line
%%% output: every sentence is on a single line; word-tokens are
%%%         separated by a single space

macro( words_afkortingen,
       	cap_words([
   acad,acc,adm,'Am',adr,adv,afb,afd,afgel,afk,afl,aggl,alc,ambas,ann,ant,
                 antr,amerik,'Arab',arr,artill,av,'Ave',
   beg,beh,'Bel',beleidsmedew,bep,ber,bew,bijg,bijv,bijz,
                 biol,blvd,bldz,blz,bn,br,'Brab','Bros','Bulg',bvb,
   ca,'Can',cat,caval,centr,cf,cfr,ch,chem,'Chin',chor,chr,'Cic',cie,cl,'Col',
                 commerc,comp,'Conn',cons,corp,cq,cs,ct,
   dbl,dd,'Del','Den',dep,dept,'Deut',di,dir,diss,distr,div,dl,dld,dln,doct,
                 doelp,doopsgez,dtsl,'Du',dui,duitsl,dwz,
   echtgen,econ,ed,edd,eds,eendr,eetl,eff,eig,eigenn,el,
                   elektr,electr,'em.pred',
                   'Eng','Ex',enkv,ent,enw,ev,evang,
		 evt,exec, excl,exx,
   fa,fam,feat,ffrs,fig,fin,fiz,fl,'FL',fr,'Fra',ft,
   'Gal',geb,gebr,ged,geïll,geill,'g.mv',
                   gelijkn,geh,gem,'Gen',gepens,gepubl,geref,gereform,
                   gespr,gest,gew,gld,gr,
   'Hand','Hebr',herh,herv,hfd,hon,'Hong',hoofdtelw,hoogl,hst,hyp,
   ill,incl,inc,ind,indiv,inf,infant,ing,ingel,inl,inst,inspect,
                   instell,int,intend,inw,ipv,'Isr','It','Ital',ivm,
   'Jer','Jes','Joeg',jrg,
   kand,kath,'Kerkgem',kl,kol,'Kon',kr,kw,
   'Lat','Lc',lett,'Lev',lib,lt,ltd,'Luc','luit.kol',luth,
   ma,mass,max,
          'Matt','Md',med,medew,meerv,mg,mgr,'Mich',mil,milj,'Miss',ml,mln,
	   mnl,mog,mrg,mrt,ms,muz,mv,
   'n.Chr','niet-chr','niet-Chr','Nat.Lab',
                   nation,nav,nbr,ned,nederl,'Neh',niem,nl,nmd,
		 nr,nrs,'Num',nvdr,
   oa,ol,ondertit,onderz,ong,ongeh,oorspr,'Opb',opn,opp,'oud-dir',oudfr,oudholl,
		 ouderl,ov,overl,
   'Pa',partn,pct,penningm,pf,plc,plm,plv,pnt,posth,pp,
		 pred,pres,prot,prov,ps,pseud,psych,pt,ptas,ptn,
   'radio-uitz',reb,'rector-magn',red,redbl,reg,res,resp,ret,
		   rijksuniv,'r.-k','Rom','Russ',
   samenst,samenw,'Sanskr',sc,scen,sp,'Spr',st,'Ste',str,subsp,symf,syn,
   'Tac',techn,tek,tel,'tel.nr',temp,theel,tl,toegel,toep,toez,
                   'Tsj','Tu','TV-uitz','tv-uitz',
   'U',uitg,uitk,'vakantie-uitk',uitdr,uitz,univ,
   var,vb,'v.Chr',verl,verm,vert,vervr,verz,vgl,'vice-vrz',vlp,
		 vm,vml,vnl,vols,volw,voorh,voorm,vr,vriendsch,
                 vrijg,vrijw,vrz,vs,vz,'vice-vz','fr.vz',
   wd,weled,werkz,wetensch,wijkgem,wnd,wo,
   za,zg,zgn,zn,zr,zw,'z.mv'
   ]) ).

macro(words_titel,cap_words([bc,dhr,dipl,dr,ds,drs,ir,jhr,mgr,mrs,mw,mej,mevr,mr,hr,prof])).

macro(words_afkorting_with_number,cap_words([art,no,op,zo])).

macro(words_afkorting_after,cap_words(
  [ apr,aug,
    bv,bw,
    cm,co,
    dec,
   'e.a','e.d','e.v','e.v.a',
    enz,etc,
    feb,febr,
    'Jap',jl,'j.l',jr,jul,jun,
    kg,km,
    m,maa,mm,
    nov,
    'o.i.d',okt,
    pag,
    sec,sep,sept,sr,
    tek
    ])).

macro(words_poule,cap_words(
  [formule,groep,poule,serie])).

macro(words_ambiguous_afkorting_after,cap_words(
  [
   alg,alt,as,
   'Belg',
   bros,
   cat,christ,cult,
   dag,diss,do,dom,
   eng,ex,
   geest,
   ha,herz,
   infant,
   jan,'Jap',
   kap,kon,kwal,
   lat,
   min,
   nat,nation,
   off,
   part,pol,
   'Port',
   'Rus',
   sticht,
   tent,'Tsjech',trad,
   vak,
   veren,verg,vol,volt,
   ww
  ])).

rx(cap_words(List),Fa) :-
    fsa_regex:list_atom_chars(List,CharList0),
    add_capitalization(CharList0,CharList,[]),
    fsa_dict:fsa_dict_to_fsa(CharList,Fa).

add_capitalization([],L,L).
add_capitalization([H0|T],L0,L) :-
    findall(H,add_cap(H0,H),L0,L1),
    add_capitalization(T,L1,L).

add_cap(X,X).
add_cap([H0|T],[NH|T]) :-
    atom_codes(H0,[H]),
    H >= 97,
    H =< 122,
    is(NH0,-(H,32)),
    atom_codes(NH,[NH0]).

