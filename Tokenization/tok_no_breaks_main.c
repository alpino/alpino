#include <wchar.h>
#include <stdio.h>
#include <locale.h>
#define MAXLINE 131072        /* this constrains line length */

int t_accepts(wchar_t *in, wchar_t *out, int max);

int main() {
  wchar_t in[MAXLINE];
  wchar_t out[MAXLINE];
  int return_value;

  setlocale(LC_ALL,"");

  while(fgetws(in,MAXLINE,stdin) != NULL) {

    wchar_t *p = wcschr(in, '\n');
    if (p) { *p = '\0'; }  /* chomp */
					  
    return_value = t_accepts(in,out,MAXLINE);
    switch(return_value) {
    case 0:
      fprintf(stderr,"no\n");
      break;
    case 1:
      printf("%S\n",out);
      break;
    case 2:
      fprintf(stderr,"error: output line too long\n");
      break;
    }
  }                                         /* end-of-file */
  return 0;
}

