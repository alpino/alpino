:- module(hdrug_call_tree,
	  [ call_tree_bu/0,
	    call_tree_bu/1,
	    call_tree_bu_tk/0,
	    call_tree_bu_tk/1,
	    call_tree_bu_dot/0,
	    call_tree_bu_dot/1,
	    call_tree_bu_clig/0,
	    call_tree_bu_clig/1,
	    call_tree_bu_flat/0,
	    call_tree_bu_flat/1,
	    call_tree_bu_latex/0,
	    call_tree_bu_latex/1
	  ]).

:- expects_dialect(sicstus).

%% otherwise, swi writes hd/mod as 'hd/ (mod)' etc
:- op(0,yfx,mod).

:- multifile user:help_info/4.

:- use_module(library(lists)).
:- use_module(hdrug_util).

:- public
    help_info/4.
:- discontiguous
    help_info/4.

help_info(class,hook,"Hook Predicates",
"This section lists the hook predicates for the hdrug_call_tree
library."). 

help_info(class,pred,"Predicates",
"This section lists the predicates exported by the hdrug_call_tree
library."). 

user:help_info(module,hdrug_call_tree,"Displaying Lexical Hierarchies",
"This library is intended to be used to display lexical hierarchies in
tree format. The relevant predicates all take a unary predicate
Pred. The predicates then pretty print in a tree format the hierarchy
related to the predicate Fuctor/1 as follows. Pred dominates all
predicates that call Pred in their body. 

If the optional Functor argument is absent, then the
call_default/1 hook predicate is used to obtain Functor.  

        transitive(X) :- verb(X).  
        
        verb(X) :- lex(X).
        
        noun(X) :- lex(X).
        
gives the tree: lex(verb(transitive),noun)

Other calls in the body are attached to the label, as a poor man's way
to illustrate multiple inheritance: 

        transitive(X) :- verb(X).
        
        verb(X) :- lex(X).
        
        noun(X) :- lex(X),other(X).
        
gives: lex(verb(transitive),noun[other])

Leaves of the tree can be defined by the user (e.g. to stop the tree
at interesting point, and to give interesting info in the label, use
the hook predicate call_leaf(Call,Label). And yes, don't forget
the obvious: it is assumed that the predicates are not recursive."). 

help_info(pred,call_tree_bu,"
hdrug_call_tree:call_tree_bu[_tk/_clig/_latex][(Functor)]",
"pretty prints in a tree format the hierarchy related to the predicate
Fuctor/1. If the optional Functor argument is absent, then the
call_default/1 hook predicate is used to obtain Functor. The _tk
_clig and _latex suffices indicate that a different output medium
should be chosen (instead of the console). "). 

help_info(hook,call_default,"call_default(Functor)","Indicates
that Functor is the default predicate for the various call_tree
predicates."). 

help_info(hook,call_clause,"call_clause(Head,Body)","").
help_info(hook,call_leaf,"call_leaf(Leaf)","").
help_info(hook,call_build_lab,"call_build_lab(F,Fs,L)","").
help_info(hook,call_ignore_clause,"call_ignore_clause(F/A)","").



call_tree_bu :-
    hdrug:call_default(F),
    call_tree_bu(F).

call_tree_bu_tk :-
    hdrug:call_default(F),
    call_tree_bu_tk(F).

call_tree_bu_dot :-
    hdrug:call_default(F),
    call_tree_bu_dot(F).

call_tree_bu_latex :-
    hdrug:call_default(F),
    call_tree_bu_latex(F).

call_tree_bu_clig :-
    hdrug:call_default(F),
    call_tree_bu_clig(F).

call_tree_bu_flat :-
    hdrug:call_default(F),
    call_tree_bu_flat(F).

call_tree_bu(G) :-
    (	atom(G)
    ->	F=G,
	Head =.. [F,_],
	A=1
    ;	G=F/A,
	functor(Head,F,A)
    ),
    hdrug:call_clause(Head,Body),
    l_tree(Head,Body,Tree),
    hdrug_txt:pretty_graphic(t,Tree).

call_tree_bu_tk(G) :-
    (	atom(G)
    ->	F=G,
	Head =.. [F,_],
	A=1
    ;	G=F/A,
	functor(Head,F,A)
    ),
    hdrug:call_clause(Head,Body),
    l_tree(Head,Body,Tree),
    hdrug_gui:tk_tree(t,Tree).

call_tree_bu_dot(G) :-
    (	atom(G)
    ->	F=G,
	Head =.. [F,_],
	A=1
    ;	G=F/A,
	functor(Head,F,A)
    ),
    hdrug:call_clause(Head,Body),
    l_tree(Head,Body,Tree),
    hdrug_dot:dot_tree(t,Tree).

call_tree_bu_latex(G) :-
    (	atom(G)
    ->	F=G,
	Head =.. [F,_],
	A=1
    ;	G=F/A,
	functor(Head,F,A)
    ),
    hdrug:call_clause(Head,Body),
    l_tree(Head,Body,Tree),
    hdrug_latex:latex_tree(t,Tree).

call_tree_bu_clig(G) :-
    (	atom(G)
    ->	F=G,
	Head =.. [F,_],
	A=1
    ;	G=F/A,
	functor(Head,F,A)
    ),
    hdrug:call_clause(Head,Body),
    l_tree(Head,Body,Tree),
    hdrug_clig:clig_tree(t,Tree).

l_tree(Head,Cons,tree(Label,x,Ds)) :-
    hdrug:call_leaf(Head,L,Ds),
    !,
    build_lab(L,Cons,Label).

l_tree(Head,Cons,tree(Label,x,[])) :-
    hdrug:call_leaf(Head,L),
    !,
    build_lab(L,Cons,Label).

l_tree(Head,Body,tree(Label,x,Ds)) :-
    functor(Head,CallF,Ar),
    build_lab(CallF/Ar,Body,Label),
    findall(D0,a_d(Head,D0),Ds0),
    sort(Ds0,Ds1),
    l_tree_ds(Ds1,Ds2),
    sort(Ds2,Ds).

build_lab(F,Body,L):-
    functors(Body,Fs),
    (	Fs = []
    ->	L = F
    ;	hdrug:call_build_lab(F,Fs,L)
    ).

functors([],[]).
functors([Term|T0],T) :-
    functor(Term,F,A),
    a_functor(F/A,T,T1),
    functors(T0,T1).

a_functor(F/A,Res,Tail) :-
    (	hdrug:call_ignore_clause(F/A)
    ->	Res=Tail
    ;	Res=[F/A|Tail]
    ).

a_d(Goal,Head+Body) :-
    hdrug:call_clause(Head,Body0),
    select(Goal,Body0,Body),
    hdrug_util:debug_message(2,"~w ~w~n",[Goal,Head+Body]).

l_tree_ds([],[]).
l_tree_ds([H0+B|T0],[H|T]):-
    l_tree(H0,B,H),
    l_tree_ds(T0,T).

call_tree_bu_flat(G) :-
    (	atom(G)
    ->	F=G,
	Head =.. [F,_],
	A=1
    ;	G=F/A,
	functor(Head,F,A)
    ),
    hdrug:call_clause(Head,Body),
    l_tree(Head,Body,Tree),
    flat(Tree,'').

flat(tree(Label,_,Ds),Tab) :-
    format("~a~w",[Tab,Label]),
    flat_ds(Ds,Tab).

flat_ds([],_) :-
    nl.
flat_ds([H|T],Tab0) :-
    atom_concat(Tab0,'  ',Tab),
    nl,
    flat_ds1([H|T],Tab).

flat_ds1([],_).
flat_ds1([H|T],Tab) :-
    flat(H,Tab),
    flat_ds1(T,Tab).
    
