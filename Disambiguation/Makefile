cdb_part=0
MIN_FEATURE_FREQ=2
DATA_NUMBER_ANALYSES=3000
MODEL_NUMBER_ANALYSES=100
USER_MAX=3000000
PARSE_CANDIDATES_BEAM=$(DATA_NUMBER_ANALYSES)
DATA_MAX_SENTENCE_LENGTH=1000
MODEL_MAX_SENTENCE_LENGTH=400
TESTDEBUG=
ESTIMATE=/net/aistaff/dekok/aps/bin/tinyest
VARIANCES=--l1 0.0001
BASEPARTS=p0.sgz p1.sgz p2.sgz p3.sgz p4.sgz p5.sgz p6.sgz p7.sgz p8.sgz p9.sgz
GZPARTS=$(BASEPARTS) $(EXTRAPARTS)
tenfoldlog=tenfold.log

default: 

new0:
	$(MAKE) --no-print-directory -s his_score cdb_part=$(i) 2>p$(i).log | gzip > p$(i).gz
	$(MAKE) -s p$(i).counts

%.counts: %.sgz ignore_features
	zcat $*.sgz\
        | $(ALPINO_HOME)/Disambiguation/count_features\
        | grep -vF -f ignore_features > $*.counts

models:
	$(MAKE) tenfold_weights | tee $(tenfoldlog)
	$(MAKE) tenfold.analyse >> $(tenfoldlog)
	echo "START RUN" >> log
	cat $(tenfoldlog) >> log
	echo "END RUN" >> log

tenfold.analyse:
	-@awk '/first-p\/m/ {XFP=XFP+$$2;XFN=XFN+$$3}\
              /best-p\/m/  {XBP=XBP+$$2;XBN=XBN+$$3}\
              /test-p\/m/  {XTP=XTP+$$2;XTN=XTN+$$3}\
              /first-av/ {XFS=XFS+$$2;XFC=XFC+$$3}\
              /best-av/  {XBS=XBS+$$2;XBC=XBC+$$3}\
              /test-av/  {XTS=XTS+$$2;XTC=XTC+$$3;XXX++}\
              END       {FIRST=100*(1-XFP/XFN);\
                         BEST=100*(1-XBP/XBN);\
                         TEST=100*(1-XTP/XTN);\
                         printf "total result after %s tests\n",XXX;\
                         printf "first %.2f\n", FIRST;\
                         printf "best  %.2f\n", BEST;\
                         printf "test  %.2f\n", TEST;\
                         printf "rate  %.2f\n", \
                           100*(TEST-FIRST)/(BEST-FIRST);\
                         printf "p/s-first %.2f\n",XFS/XFC;\
                         printf "p/s-best  %.2f\n",XBS/XBC;\
                         printf "p/s-test  %.2f\n",XTS/XTC;\
                         printf "p/s-rate  %.2f\n",\
                           100*(XTS/XTC-XFS/XFC)/(XBS/XBC-XFS/XFC);\
                        } ' $(tenfoldlog)
	$(MAKE) loglog

loglog:
	wc -l *.pl
	#awk -F\| '{ print $$2 }' *.features | sort -u | awk -F\( '{ print $$1 }' \
        #                 | uniq -c | sort -nr > feature-counts
	-echo out of memory `cat p*.log | grep -c 'out of memory'`
	-echo timed out `cat p*.log | grep -c 'timed out'`
	-echo sentences `cat p*.log | grep -c 'Done parser'`
	-cat p*.log | grep '^Found [0-9][0-9]* solution[(]s[)]$$' |\
              awk '{ N=N+$$2 } END {print N,"parses in total"} '
	( mv parses parses0 ; $(MAKE) -s parses )

parses:
	-cat p*.log | grep '^T#' > parses

tf:
	$(MAKE) $(TEST).zest
	$(MAKE) $(TEST).weights
	$(MAKE) $(TEST)_weights.pl
	$(MAKE) test

tf0:
	$(MAKE) $(TEST).zest
	$(MAKE) $(TEST).weights
	$(MAKE) $(TEST)_weights.pl

$(TEST).zest : p[$(PARTS)].sgz $(TEST).features
	zcat p[$(PARTS)].sgz |\
             $(ALPINO_HOME)/Disambiguation/prepare_estimate -l $(MODEL_MAX_SENTENCE_LENGTH)\
                                 -f $(TEST).features | $(ALPINO_HOME)/Disambiguation/score0-1.py | gzip > $@

$(TEST).tcounts: p[$(PARTS)].counts 
	cat p[$(PARTS)].counts | $(ALPINO_HOME)/Disambiguation/count_features -a \
                > $(TEST).tcounts


FIRSTKEY=0
LASTKEY=1000000

VARIANT=noframes
#VARIANT=frames

his_score:
	@Alpino -notk cmdint=off\
          debug=1 user_max=$(USER_MAX)\
          display_quality=off\
          display_main_parts=off\
          pos_tagger=$(POS_TAGGER)\
          pos_tagger_n=$(POS_TAGGER_N)\
          number_analyses=$(DATA_NUMBER_ANALYSES)\
          max_sentence_length=$(DATA_MAX_SENTENCE_LENGTH)\
          parse_candidates_beam=$(PARSE_CANDIDATES_BEAM)\
          -flag suite ../../Suites/cdb_part\
          cdb_part=$(cdb_part)\
          display_main_parts=off\
          -flag treebank ../../Treebank/cdb\
          unpack_bestfirst=off disambiguation=off\
          end_hook=train_penalties\
          list_all_maxent_features=on\
          treebank_dep_features_variant=$(VARIANT)\
          $(options)\
          $(extra) batch_command='parser_comparisons_int($(FIRSTKEY),$(LASTKEY))'

%.iweights: %.zest 
	$(ESTIMATE) $(VARIANCES)   $<  >  $@

%.weights: %.iweights %.features
	paste -d\| $*.features $< | awk -F\| '{ if ($$3>0||$$3<0) printf("%s|%s\n",$$2,$$3) }' >$@

%_weights.pl: %.weights $(ALPINO_HOME)/Disambiguation/index.pl
	$(ALPINO_HOME)/Disambiguation/estimate2pl < $*.weights | sicstus -l $(ALPINO_HOME)/Disambiguation/index > $@

test: 
	zcat $(TEST).gz | $(ALPINO_HOME)/Disambiguation/kleiweg/eval -w $(TEST).weights $(TESTDEBUG)

prec:
	@awk '/^first-precision/ {FP=FP+$$2; FN=FN+1};\
             /^first-recall/    {FR=FR+$$2};\
             /^first-fscore/    {FF=FF+$$2};\
             /^best-precision/ {BP=BP+$$2; BN=BN+1};\
             /^best-recall/    {BR=BR+$$2};\
             /^best-fscore/    {BF=BF+$$2};\
             /^test-precision/ {TP=TP+$$2; TN=TN+1};\
             /^test-recall/    {TR=TR+$$2};\
             /^test-fscore/    {TF=TF+$$2};\
             END { printf "first-precision-total %s\n",FP/FN;\
                   printf "first-recall-total    %s\n",FR/FN;\
                   printf "first-fscore-total    %s\n",FF/FN;\
                   printf "best-precision-total  %s\n",BP/BN;\
                   printf "best-recall-total     %s\n",BR/BN;\
                   printf "best-fscore-total     %s\n",BF/BN;\
                   printf "test-precision-total  %s\n",TP/TN;\
                   printf "test-recall-total     %s\n",TR/TN;\
                   printf "test-fscore-total     %s\n",TF/TN; }' $(log)


include $(ALPINO_HOME)/Makefile.errormining

IS=0 1 2 3 4 5 6 7 8 9

data0:
	for i in $(IS);\
        do $(MAKE) -s new0 i=$$i;\
        done

%.sgz : %.gz
	zcat $*.gz | $(ALPINO_HOME)/Disambiguation/extract_subset -n $(MODEL_NUMBER_ANALYSES) | gzip > $*.sgz

%.features: %.tcounts
	awk -F\| 'BEGIN { N=0 }\
                  { if ( $$4 > $(MIN_FEATURE_FREQ) )\
                              {  print N "|" $$1; N++} }'\
           $*.tcounts >$*.features
