marginal(dep35(Arg,ArgPos,Rel,HeadPos,Head),
         tdep35,
	 ldep35(Arg,ArgPos),
	 rdep35(Rel,HeadPos,Head)).

marginal(hdpp(Head,HeadPos,Rel,Prep,Noun,NounPos),
	 tdep35,
	 dep35(Noun,NounPos,hd/obj1,prep,Prep),
	 rdep35(Rel,HeadPos,Head)
	).

marginal(appos_person(TYPE,Word),appos,apposl(TYPE),apposr(Word)).

score_corpus_feature(Feature,Val) :-
%    (   Feature=appos_person(_,_)
%    ->  trace
%    ;   true
%    ),
    corpus_frequency_lookup(Feature,Pair),
    marginal(Feature,Total,Left,Right),
    corpus_frequency_lookup(Left,L),
    corpus_frequency_lookup(Total,T),
    corpus_frequency_lookup(Right,R),
    association_score(L,R,Pair,T,Val),
    Val > 0.

:- initialize_flag(pmi_threshold,1000).

%%% Gerlof's normalized pointwise mutual information
%%% however, we ignore candidates with a value that is
%%% lower than Threshold, and we subtract Threshold from
%%% all other values.
%%%   Motivation:
%%%           - smaller models
%%%           - low values are often due to noise
%%%             eg: wijn as subject of drink due to misparses
%%% Finally, everything is multiplied by 10000 so that
%%% we can work with integers.
association_score(L,R,Pair,T,Val) :-
    PMI is log(T*Pair/(L * R)),
    NORM is -log(Pair/T),
    Val is integer(10000*PMI/NORM).

corpus_frequency_lookup(TripleTerm,Score) :-
    initialize_corpus_frequency(DictNo),
    feature_term_to_codes(TripleTerm,Triple),
    pro_fadd:associate_word_integer(Triple,DictNo,Score),
    Score > 0.

initialize_corpus_frequency(No) :-
    hdrug_flag(initialized_corpus_frequency,Init),
    initialize_corpus_frequency(Init,No).

initialize_corpus_frequency(undefined,No) :-
    !,
    File = 'corpus_frequency_features.fsa',
    pro_fadd:init_morph(File,0,0,0,0,No),
    debug_message(1,"initialized corpus_frequency ~w (~w)~n",[File,No]),
    set_flag(initialized_corpus_frequency,initialized(No)).
initialize_corpus_frequency(initialized(No),No).

feature_term_to_codes(Feature,Codes) :-
    format_to_chars('~q',[Feature],Codes).

go:-
    repeat,
    catch(read(X),
	  syntax_error(_,_,_,_,_),
	  fail
	 ),
    (   X == end_of_file
    ->  true
    ;   score_corpus_feature(X,Val),
        format("~q\t~q\t~w~n",[X,X,Val]),
        fail
    ), !.

