:- module(alpino_genrules, [ headed_grammar_rule/4, grammar_rule_unpack/4 ]).

:- use_module(hdrug(hdrug_util)).

:- multifile user:term_expansion/2.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grammar rule modification %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% We will generate head first rules by picking the next grammar rule,
%% and moving the head daughter as the first argument of the headed_grammar_rule
%% term. This will allow us to invocate rules quickly.	
generate_headfirst_rule(Rule) :-
    alpino_lc_in:grammar_rule(Id,Mother,Daughters),
    \+ ignore_rule(Id),
    generate_headfirst_rules_aux(Id,Mother,Daughters,Rule).

generate_headfirst_rule(gap(ID,Cat)) :-
    alpino_lc_in:grammar_rule(ID,Cat,[]).

%generate_headfirst_rule(headed_grammar_rule(Punct,optpunct(ne),Mother,[Punct])) :-
%    alpino_lc_in:grammar_rule(optpunct(ne),Mother,[Punct]).

%% imp and imp_imp form a special case, since the semantic head uses get_val, but we
%% need to do a put_val first. So, we will make the leftmost daughter the head.
generate_headfirst_rules_aux(vast_staat,Mother,[Head|Daughters],Rule) :-
    !,
    Rule = headed_grammar_rule(Head,vast_staat,Mother,[Head|Daughters]).

generate_headfirst_rules_aux(imp,Mother,[Head|Daughters],Rule) :-
    !,
    Rule = headed_grammar_rule(Head,imp,Mother,[Head|Daughters]).

generate_headfirst_rules_aux(imp_imp,Mother,[Head|Daughters],Rule) :-
    !,
    Rule = headed_grammar_rule(Head,imp_imp,Mother,[Head|Daughters]).

%% First attempt to move the head in the 'proper' manner.
generate_headfirst_rules_aux(Id,Mother,Daughters,Rule) :-
    move_head(Mother,Head,Daughters),
    !,
    Rule = headed_grammar_rule(Head,Id,Mother,Daughters).

%% Last resort.
generate_headfirst_rules_aux(Id,Mother,[Daughter|Rest],Rule) :-
    Rule = headed_grammar_rule(Daughter,Id,Mother,[Daughter|Rest]).

%% Move the leftmost daughter that has the same semantics as the mother.
move_head(Mother,Head,[Daughter|_]) :-
    sem_match(Mother,Daughter),
    !,
    Head = Daughter.
move_head(Mother,Head,[_|Rest]) :-
    move_head(Mother,Head,Rest).

%% case 1:
%% ignore optional punctuation
ignore_rule(optpunct(ne)).  % Temporarily exclude optional punctuation
ignore_rule(vp_c_mod_c_v).
ignore_rule(vp_predm_v_comma).
ignore_rule(vp_predm_adj_v_comma).
ignore_rule(vp_predm_pp_v_comma).
ignore_rule(vp_v_komma_mod).
ignore_rule(vpx_v_komma_mod).
ignore_rule(vp_v_komma_predm).
ignore_rule(v2_vp_dubb_vproj).

%% case 2:
%% ignore rules where punctuation really ought to have an effect on
%% the representation!
ignore_rule(modifier_p(1)).
ignore_rule(modifier_p(2)).
ignore_rule(v_v_bracketed_v).
ignore_rule(v_bracketed_v_v).
ignore_rule(vp_bracketed_predm_v).
ignore_rule(vp_bracketed_predm_adj_v).
ignore_rule(vp_bracketed_predm_pp_v).
ignore_rule(vp_dashed_predm_v).
ignore_rule(vp_dashed_predm_adj_v).
ignore_rule(vp_dashed_predm_pp_v).
ignore_rule(vp_v_bracketed_extra_rel).
ignore_rule(vp_v_bracketed_m_extra_vp).
ignore_rule(vp_v_bracketed_m_extra).
ignore_rule(n_dashed_adj_n).
ignore_rule(n_bracketed_adj_n).
ignore_rule(n_dashed_mod_n).
ignore_rule(n_bracketed_mod_n).
ignore_rule(q_n).
ignore_rule(q_pn).
ignore_rule(q_pred).
ignore_rule(q_mod).
ignore_rule(q_vb).
ignore_rule(q_v).
ignore_rule(q_vp).
ignore_rule(q_vproj).
ignore_rule(q_sbar).
ignore_rule(q_a).
ignore_rule(np_det_n_q).
ignore_rule(sbar(quoted_a)).
ignore_rule(a_bracketed_mod_a).
ignore_rule(a_ligg_mod_a).
ignore_rule(a_bracketed_int_adv_a).
ignore_rule(a_a_bracketed_mod).
ignore_rule(a_a_lig_mod).
ignore_rule(bracketed_te).
ignore_rule(sbar(quoted_a)).
ignore_rule(bracketed_top_start).
ignore_rule(top_bracketed_start).
ignore_rule(quoted_top_start).
ignore_rule(top_quoted_start).
ignore_rule(n_bracketed_num_n).
ignore_rule(num_bracketed_adv_num).
ignore_rule(det_bracketed_adv_det).
ignore_rule(bracketed_end_coord(_,conj)).
ignore_rule(bracketed_end_coord(_,conj,mod)).
ignore_rule(bracketed_end_coord(_,conj,c_mod)).

%% case 3:
%% rules not thought useful for generation:
%% not necc. related to punctuation
%% typically rules that are there only for robustness' sake
ignore_rule(n_adj_n_marked).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modified grammar rules for unpacking %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

unpack_rule(grammar_rule_unpack(Id,Mother,Daughters,Projections)) :-
    alpino_lc_in:grammar_rule(Id,Mother,Daughters0),
    \+ ignore_rule(Id),
    remove_call(Daughters0,Daughters),
    daughter_proj(Daughters,Mother,Projections).

remove_call(Daughters0,Daughters) :-
    lists:select(call(_),Daughters0,Daughters),
    !.
remove_call(Daughters,Daughters).

daughter_proj([],_,[]).
daughter_proj([H|T],Mother,[Proj|NewT]) :-
    (   sem_match(Mother,H)
    ->  Proj = -
    ;   Proj = +
    ),
    daughter_proj(T,Mother,NewT).

%%%%%%%%%%%%%
% Semantics %
%%%%%%%%%%%%%

%% Check whether two feature structures have the same semantics 
sem_match(Mother,Daughter) :-
    alpino_data:dt(Mother,MDT),
    alpino_data:dt(Daughter,DDT),
    MDT == DDT.

call_with_constraints(Body1,Goal1,Rule) :-
    call(Body1),
    copy_term(Body1/Goal1,_/Goal,ConsList),
    cons_body(ConsList,Goal,Rule).

cons_body([],Head,Head).
cons_body([Goal|T],Head,(Head:-Goals)):-
    cons_body1(T,Goal,Goals).

cons_body1([],Goal,Goal).
cons_body1([Goal|T],PrevGoal,(PrevGoal,Goals)) :-
    cons_body1(T,Goal,Goals).

user:term_expansion(expand_genrules,GoalList) :-
    findall(Rule,call_with_constraints(generate_headfirst_rule(Goal),
				       Goal,Rule),GoalList).

user:term_expansion(expand_unpack_rules,GoalList) :-
    findall(Rule,call_with_constraints(unpack_rule(Goal),
				       Goal,Rule),GoalList).


expand_genrules.
expand_unpack_rules.