include Makefile.include

DIRS= zlib TreebankTools fadd unix Derivbank SuffixArrays PosTagger\
      Names Tokenization Generation Generation/fluency Suites Lexicon\
      Grammar Treebank src

.PHONY: all install clean realclean xref checks

export ALPINO_HOME

all:
	( cd Hdrug ; $(MAKE) hdrug state )
	( cd src ; $(MAKE) guides$(MODULEEXT) )
	for dir in $(DIRS); do ( if [ -d $$dir ]; \
                                 then cd $$dir ; $(MAKE);\
                                 fi ); done

install:
	-for dir in $(DIRS); do ( if [ -d $$dir ]; \
                                  then cd $$dir ; $(MAKE) install;\
                                  fi ); done
	install -D Annotate $(BINDIR)

clean:
	-for dir in Hdrug $(DIRS); do ( if [ -d $$dir ]; \
                                  then cd $$dir ; $(MAKE) clean;\
                                  fi ); done

realclean:
	-for dir in Hdrug $(DIRS); do ( if [ -d $$dir ]; \
                                  then cd $$dir ; $(MAKE) realclean;\
                                  fi ); done

xref:
	@spxref -i xref.pl -w - Hdrug/Prolog/hdrug_main Hdrug/Prolog/hooks\
                         src/start.pl src/hooks.pl\
                        Generation/fluency/pro_lm\
                        Grammar/penalties.pl Generation/fluency/maxent.pl \
        | grep -v 'can not follow'
	@echo "(25 warnings expected; 1 unused ugraphs)"


include Makefile.export

