#include <sys/wait.h>
#include <unistd.h>

#include <sicstus/sicstus.h>

SP_term_ref pl_fork() {
  SP_term_ref r = SP_new_term_ref();

  pid_t pid = fork();
  if (pid == 0)
    SP_put_string(r, "child");
  else if (pid > 0)
    SP_put_integer(r, pid);
  else {
    SP_term_ref m = SP_new_term_ref();
    SP_put_string(m, "fork/1: error in system call");
    SP_raise_exception(m);
  }

  return r;
}

void pl_reap_zombies() {
  while (1) {
   int r = waitpid(0, NULL, WNOHANG);
   if (r == 0 || r == -1)
     return;
  }
}

SP_term_ref pl_wait(long *pid) {
  SP_term_ref r = SP_new_term_ref();

  int stat;
  *pid = wait(&stat);
  if (*pid == -1) {
    SP_term_ref m = SP_new_term_ref();
    SP_put_string(m, "wait/2: error in system call (no child?)");
    SP_raise_exception(m);
    return r;
  }

  SP_term_ref functor = SP_new_term_ref();
  SP_term_ref rStat = SP_new_term_ref();

  if (WIFEXITED(stat)) {
    SP_put_integer(rStat, WEXITSTATUS(stat));
    SP_cons_functor(r, SP_atom_from_string("exited"), 1, rStat);
  }
  else if (WIFSIGNALED(stat)) {
    SP_put_integer(rStat, WTERMSIG(stat));
    SP_cons_functor(r, SP_atom_from_string("signaled"), 1, rStat);
  }
  else if (WIFSTOPPED(stat)) {
    SP_put_integer(rStat, WSTOPSIG(stat));
    SP_cons_functor(r, SP_atom_from_string("stopped"), 1, rStat);
  }
  else {
    SP_put_string(r, "unknown");
  }

  return r;
}
