:- module(alpino_survive_tagger, [ survive_tagger/2 ]).

:- expects_dialect(sicstus).

%% meant to keep tags for pos-tags or words that should not be removed
%% e.g. because recently changed/added in grammar.
survive_tagger([],[]).
survive_tagger([T|Ts],Rs) :-
    survive_tagger(T,Rs,Rs0),
    survive_tagger(Ts,Rs0).

survive_tagger(Ref,Rs0,Rs) :-
    instance(Ref,(Tag:-true)),
    (	survives(Tag)
    ->	Rs0=Rs
    ;	Rs0=[Ref|Rs]
    ).

survives(tag(_,_,_,_,W,_,_,_T)) :-
    surviving_root(W).
survives(tag(_,_,_,_,_,W,_,_T)) :-
    surviving_word(W).
survives(tag(_,_,_,_,_W,_,_,T)) :-
    surviving_tag(T).
survives(tag(_,_,_,_,W,_,_,T)) :-
    surviving_root_tag(W,T).
survives(tag(_,_,_,_,_,W,_,T)) :-
    surviving_word_tag(W,T).

%% Wat later/vroeger/eerder
survives(tag(_P0,P1,_Q0,Q1,wat,'Wat',_,adverb)) :-
    alpino_lexical_analysis:tag(P1,_P,Q1,_Q,_,_,_,adjective(er(tmpadv))).

%% ik wil terug naar huis gaan
survives(tag(_P0,P1,_Q0,Q1,terug,_,_,loc_adverb)) :-
    alpino_lexical_analysis:tag(P1,_P,Q1,_Q,naar,_,_,preposition(naar,[toe])).

%% Kort geleden ...
survives(tag(_P0,P1,_Q0,Q1,kort,_,_,adjective(no_e(tmpadv)))) :-
    alpino_lexical_analysis:tag(P1,_P,Q1,_Q,geleden,_,_,adjective(ge_both(tmpadv))).

%% Kort samengevat ...
survives(tag(_P0,P1,_Q0,Q1,kort,_,_,adjective(no_e(tmpadv)))) :-
    alpino_lexical_analysis:tag(P1,_P,Q1,_Q,samengevat,_,_,adjective(ge_no_e(adv))).

%% Kort gezegd ...
survives(tag(_P0,P1,_Q0,Q1,kort,_,_,adjective(no_e(tmpadv)))) :-
    alpino_lexical_analysis:tag(P1,_P,Q1,_Q,gezegd,_,_,adjective(ge_no_e(adv))).

%% Net zomin
survives(tag(_,P1,_,Q1,net,_,_,adjective(no_e(adv)))) :-
    alpino_lexical_analysis:tag(P1,_P,Q1,_Q,zomin,_,_,_).

survives(tag(P0,_,Q0,_,zomin,_,_,als_adjective(both(adv)))) :-
    alpino_lexical_analysis:tag(_,P0,_,Q0,net,_,_,_).

survives(tag(P0,_,Q0,_,stap,stappen,_,noun(de,count,pl))) :-
    alpino_lexical_analysis:tag(_,P0,_,Q0,neem,_,_,verb(hebben,inf,transitive)).

survives(tag(P0,_,Q0,_,verdeeld,verdeeld,_,adjective(ge_no_e(adv)))) :-
    alpino_lexical_analysis:tag(_,P0,_,Q0,reageer,_,_,_).

surviving_word(_):-
    fail.


%surviving_root(_):-
%    fail.
%% various cases which the tagger always gets wrong :-(
%surviving_root(circa).
surviving_root('in totaal').
surviving_root('naar schatting').

surviving_root('van de wijs').
surviving_root('van streek').
surviving_root('in het oog').
surviving_root('uit elkaar').

%%
surviving_root('om het even').
surviving_root('\'s jaars').

surviving_root(behouden).

%surviving_word_tag(_,_):-
%    fail.
surviving_word_tag('Zo',adverb).

%% zich zorgen maken
surviving_word_tag(zorgen,noun(de,count,pl)).
surviving_word_tag('Slechts',adverb).
surviving_word_tag('Ongeveer',sentence_adverb).
surviving_word_tag(wennen,  v_noun(_)).         % het zal wel wennen zijn .
surviving_word_tag(dringen, v_noun(_)).         % het zal dringen worden voor..

%% tmp
surviving_word_tag(extra,adjective(postn_both(adv))).

%% + gaan
surviving_word_tag(verloren, adjective(ge_both(_))).
surviving_word_tag(vergezeld,adjective(ge_no_e(_))).
surviving_word_tag(vergezeld,adjective(ge_no_e(_),_)).

%% ... en Feyenoord won verdiend
surviving_word_tag(verdiend,adjective(ge_no_e(_))).

surviving_word_tag(bewezen, adjective(ge_both(_))).    % achtte ... bewezen
surviving_word_tag(bewezen, adjective(ge_both(_),subject_sbar_no_het)).

surviving_word_tag(bezet,   adjective(ge_no_e(_))).

surviving_word_tag(voorkomen, verb(hebben,psp,_)).

surviving_word_tag(bekeken, adjective(ge_both(adv))).

surviving_word_tag(verslagen,adjective(ge_both(adv))).


%surviving_root_tag(_,_):-
%    fail.
%% korte/lange termijn is also a mwu
%% vorige week         
surviving_root_tag(kort, adjective(e)).
surviving_root_tag(lang, adjective(e)).
surviving_root_tag(termijn,tmp_noun(de,count,sg)).

%% tmp
surviving_root_tag(gesproken,adjective(ge_both(adv))).
surviving_root_tag(gesproken,adjective(ge_both(adv),_)).

%surviving_tag(_):-
%    fail.

%% If there isn't a suitable verb (adjective) they'll be removed anyway.
surviving_tag(fixed_part(_)).

