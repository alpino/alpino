:- module(alpino_tr_tag, [ tr_tag/2 ]).

:- expects_dialect(sicstus).

:- discontiguous
    t/2.

tr_tag(with_dt(A,_),B) :-
    !,
    tr_tag(A,B).
tr_tag(C-A,C-B) :-
    !,
    tr_tag(A,B).
tr_tag(Tag0,Tag) :-
    t(Tag0,Tag),
    !.
tr_tag(Tag,Tag).


% ignore subcat and hebben/zijn
t(verb(_,B0,_),            verb(B)) :-
    (   B0 = inf(no_e)
    ->  B  = inf
    ;   B0 = imp(_)  % tmp
    ->  B  = imp     % tmp
    ;   B0 = B
    ).

t(name_determiner(X),  determiner(X)).
t(name_determiner(X,_),determiner(X)).

t(adjective(Infl,_),      adjective(Infl)).

t(np_adjective,             np_adjective).
t(np_adjective(_),          np_adjective).
t(het_np_adjective(_),      np_adjective).
t(het_np_adjective,         np_adjective).

t(nominalized_adjective(_), nominalized_adjective).
t(post_adjective(A,_),      post_adjective(A)).

t(noun(DeHet0,_,Num),             noun(DeHet,Num,[])) :-
    tr_dehet(Num,DeHet0,DeHet).

t(noun(DeHet0,_,Num,Sc0),         noun(DeHet,Num,Sc)) :-
    tr_dehet(Num,DeHet0,DeHet),
    tr_noun_subcat(Sc0,Sc).

t(meas_mod_noun(DeHet0,_,Num),    meas_mod_noun(DeHet,Num,[])) :-
    tr_dehet(Num,DeHet0,DeHet).

t(meas_mod_noun(DeHet0,_,Num,Sc0),meas_mod_noun(DeHet,Num,Sc)) :-
    tr_dehet(Num,DeHet0,DeHet),
    tr_noun_subcat(Sc0,Sc).

t(amount_meas_mod_noun(DeHet0,_,Num),    amount_meas_mod_noun(DeHet,Num,[])) :-
    tr_dehet(Num,DeHet0,DeHet).

t(amount_meas_mod_noun(DeHet0,_,Num,Sc0),amount_meas_mod_noun(DeHet,Num,Sc)) :-
    tr_dehet(Num,DeHet0,DeHet),
    tr_noun_subcat(Sc0,Sc).

t(mod_noun(DeHet0,_,Num),         mod_noun(DeHet,Num,[])) :-
    tr_dehet(Num,DeHet0,DeHet).

t(mod_noun(DeHet0,_,Num,Sc0),     mod_noun(DeHet,Num,Sc)) :-
    tr_dehet(Num,DeHet0,DeHet),
    tr_noun_subcat(Sc0,Sc).

t(tmp_noun(DeHet0,_,Num),         tmp_noun(DeHet,Num,[])) :-
    tr_dehet(Num,DeHet0,DeHet).

t(tmp_noun(DeHet0,_,Num,Sc0),     tmp_noun(DeHet,Num,Sc)) :-
    tr_dehet(Num,DeHet0,DeHet),
    tr_noun_subcat(Sc0,Sc).

tr_noun_subcat(Sc0,Sc) :-
    (  ignore_noun_subcat(Sc0)
    -> Sc = []
    ;  Sc0 = Sc
    ).

tr_dehet(sg,D,D).
tr_dehet(both,D,D).
tr_dehet(meas,D,D).
tr_dehet(pl,_D,both).

ignore_noun_subcat(subject_sbar).
ignore_noun_subcat(subject_vp).
ignore_noun_subcat(pred_pp(_)).
ignore_noun_subcat(pred_pp(_,_)).
ignore_noun_subcat(pred_pp_pl(_)).
ignore_noun_subcat(pred_pp_pl(_,_)).

% ignore subcat
t(v_noun(_),              v_noun).

% ignore subtype of conjunctions
t(left_conj(_),           left_conj).
t(right_conj(_),          right_conj).
t(conj(_),                conj).

% ignore netype for names
t(proper_name(_,_),       proper_name).
t(proper_name(_),         proper_name).

% treat cleft 'dit/dat' as determiner/pronoun 'dit/dat'
t(cleft_het_noun,determiner(het,nwh,nmod,pro,nparg)).

% treat special 'het' as determiner/pronoun 'het'
t(het_noun,determiner(het,nwh,nmod,pro,nparg,wkpro)).

% treat special 'wat' as wh-pronoun 'wat'
t(wh_cleft_het_noun,pronoun(ywh,thi,sg,het,both,indef,nparg)).

% treat reflexives as ordinary pronouns
t(reflexive(u_thi,both),pronoun(nwh,thi,both, de,dat_acc,def,wkpro)). % zich
t(reflexive(fir,sg),    pronoun(nwh,fir,sg,   de,dat_acc,def,wkpro)). % me/mij
t(reflexive(je,both),   pronoun(nwh,je, sg,   de,both,   def,wkpro)). % je
t(reflexive(je,pl),     pronoun(nwh,je, pl,   de,both,   def)). % jullie
t(reflexive(fir,pl),    pronoun(nwh,fir,pl,   de,dat_acc,def)). % ons
t(reflexive(u,sg),      pronoun(nwh,u,  sg,   de,both,   def)). % u
t(reflexive(thi,pl),    pronoun(nwh,thi,pl,   de,dat_acc,def)). % ulder

%% keep form only for potential hdf-postpositions
%% probably these ought to be their own tags,
%% because quite different distribution

t(particle(Form), Tag ) :-
    (   lists:member(Form,
                 [af,aan,[aan,toe],door,[en,al],heen,in,
                  [in,de,plaats],langs,mee,na,om,op,toe,
                  uit,vandaan])
    ->  Tag = particle(Form)
    ;   Tag = particle
    ).

t(preposition(A,_),Tag) :-
    (  lists:member(A,[van,in,op,met,voor,aan,bij,uit,over,naar,door,na,
                       tegen,tot,volgens,tussen,om,onder,tijdens,achter,
                       met,per,binnen,aldus,vanwege,sinds,zonder,via,
                       vanaf])
    ->  Tag = preposition(A)
    ;   Tag = preposition
    ).
t(preposition(A,_,Sc),Tag) :-
    (  lists:member(A,[van,in,op,met,voor,aan,bij,uit,over,naar,door,na,
                       tegen,tot,volgens,tussen,om,onder,tijdens,achter,
                       met,per,binnen,aldus,vanwege,sinds,zonder,via,
                       vanaf])
    ->  Tag = preposition(A,Sc)
    ;   Tag = preposition(Sc)
    ).

