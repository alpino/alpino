:- module(alpino_postagger, [ pos_filter/1,
			      pos_filter/2
			    ]).

:- expects_dialect(sicstus).

:- use_module(tr_tag).

:- use_module(survive_tagger).
:- use_module(hdrug(hdrug_util)).
:- use_module('../src/utils').

:- if(current_predicate(with_mutex/2)).

:- else.

:- meta_predicate with_mutex(?,:).
with_mutex(_,Call) :-
    once(Call).

:- endif.

foreign(alpino_pos_init,
	c,
	alpino_pos_init(+string)).

foreign(alpino_pos_filter,
	c,
        alpino_pos_filter(+term,+term,+integer,+integer,+integer,+integer,
			  +integer,[-term])).

foreign_resource(alpino_pos,[alpino_pos_filter,alpino_pos_init]).

:- load_foreign_resource(alpino_pos).

:- initialize_flag(pos_tagger,off).
:- initialize_flag(pos_tagger_n,300).
:- initialize_thread_flag(temp_pos_tagger_n,300).
:- initialize_flag(pos_tagger_p,-1).
:- initialize_flag(pos_tagger_state_size,3).
:- initialize_flag(alpino_pos_initialized,no).

pos_filter_initialize :-
    with_mutex(fadd,pos_filter_initialize_).

:- dynamic pos_filter_initialized/0.

pos_filter_initialize_ :-
    (   pos_filter_initialized
    ->  true
    ;	hdrug_flag(pos_tagger_dir,Dir0),
        absolute_file_name(Dir0,Dir1),
	atom_concat(Dir1,'/',Dir),
	debug_message(1,"Initializing tagger (~w)...~n",[Dir]),
	alpino_pos_init(Dir),
	assertz(pos_filter_initialized)
    ).

pos_filter(Sent) :-
    pos_filter(Sent,EraseTagRefs),
    erase_tags(EraseTagRefs).

pos_filter(Sent,EraseTagRefs):-
    statistics(runtime,[T0,_]),
    hdrug_flag(pos_tagger,Tagger),
    (	Tagger == on
    ->	pos_filter_initialize,   % ensure it has been initialized
	all_tags(Tags,RefsLists),
	length(Tags,TagsLength),
	thread_flag(temp_pos_tagger_n,P_t),
	hdrug_flag(pos_tagger_p,P_p),
	hdrug_flag(pos_tagger_state_size,Size),
	hdrug_flag(debug,Debug0),
	Debug is Debug0-1,
	
	debug_message(1,"Starting fw/bw POS-tagger (~w tags)~n",[TagsLength]),

	with_mutex(fadd,alpino_pos_filter(Sent,Tags,P_t,P_p,TagsLength,Debug,Size,Solution)),

	debug_message(1,"DONE     fw/bw POS-tagger~n",[]),

        alpino_pos_filter_continue(Solution,TagsLength,T0,
                                   RefsLists,EraseTagRefs)
%    for Yan
%    ;   Tagger == offline
%    ->  all_tags(Tags,RefsLists),
%	length(Tags,TagsLength),
%        pos_filter_offline(Tags,Solution),
%        alpino_pos_filter_continue(Solution,TagsLength,T0,
%                                   RefsLists,EraseTagRefs)
    ;	EraseTagRefs=[]
    ).

alpino_pos_filter_continue(Solution,TagsLength,T0,RefsLists,EraseTagRefs) :-
    keep_tags(Solution,RefsLists,KeepTagRefs0,0,RemainingTagLength),
    sort(KeepTagRefs0,KeepTagRefs),
    refs(RefsLists,TagRefs0),
    sort(TagRefs0,TagRefs),
    ordsets:ord_symdiff(TagRefs,KeepTagRefs,EraseTagRefs0),
    survive_tagger(EraseTagRefs0,EraseTagRefs),
    statistics(runtime,[T1,_]),
    T is T1-T0,
    length(TagRefs,TagsRefsLength),
    length(EraseTagRefs,EraseTagRefsLength),
    RemainingTagRefsLength is TagsRefsLength-EraseTagRefsLength,
    debug_message(2,
         "PosTagger (classes): ~w --> ~w tags~n",
         [TagsLength,RemainingTagLength]),
    debug_message(2,
         "PosTagger (categories): ~w --> ~w tags (~w msec)~n",
         [TagsRefsLength,RemainingTagRefsLength,T]).

all_tags(Tags,Origs) :-
    findall(Tag,a_tag(Tag),Tags0),
    keysort(Tags0,Tags1),
    keymerge(Tags1,Tags2),
    key_vals(Tags2,Tags,Origs).

key_vals([],[],[]).
key_vals([Tag-Ref|T0],[Tag|T1],[Ref|T]) :-
    key_vals(T0,T1,T).

keymerge([],[]).
keymerge([Key-Val|Tail0],[Key-[Val|Vals]|Tail]) :-
    keymerge(Tail0,Key,Vals,Tail).

keymerge([],_,[],[]).
keymerge([Key-Val|Tail0],Key0,Vals,Tail) :-
    (	Key == Key0
    ->	Vals=[Val|Vals1],
	keymerge(Tail0,Key,Vals1,Tail)
    ;	Vals=[],
	Tail=[Key-[Val|Vals1]|Tail1],
	keymerge(Tail0,Key,Vals1,Tail1)
    ).

a_tag(t(Q0,Q,TagChars)-Ref):-
    clause(alpino_lexical_analysis:tag(_R0,_R,R0,R,_,_,His,Tag0),true,Ref),
    tr_tag(Tag0,Tag1),
    expand_skips(His,Tag1,R0,R,S0,S,Tag2),
    in_pieces(S0,S,Q0,Q,Tag2,Tag3),
    prettyvars(Tag3),
    charsio:format_to_chars('~q',[Tag3],TagChars).

expand_skips(quoted_name(_B,_E),Tag0,R0,R,S0,S,Tag) :-
    !,
    (    Tag0=Tag, R0=S0, R=S
    ;    Tag=begin_quoted_name, S0 is R0-1, S is R0
    ;    Tag=end_quoted_name,   S0 is R,    S is R+1
    ).

expand_skips(skip(_,Left,Right,_),Tag0,R0,R,S0,S,Tag) :-
    !,
    (   expand_skip_left(Left,Tag0,R0,R,S0,S,Tag)
    ;   expand_skip_right(Right,Tag0,R0,R,S0,S,Tag)
    ).
expand_skips(_,Tag,R0,R,R0,R,Tag).

% expand_skip_left([],Tag,R0,R,R0,R,Tag).
expand_skip_left([_|_],_Tag0,R1,_,R0,R1,skip) :-
    R0 is R1 - 1.
expand_skip_left([_|T],Tag0,R1,R,S0,S,Tag) :-
    R0 is R1 - 1,
    expand_skip_left(T,Tag0,R0,R,S0,S,Tag).

% expand_skip_right([],Tag,R0,R,R0,R,Tag).
expand_skip_right([_|_],_Tag,_,R1,R1,R,skip) :-
    R is R1+1.
expand_skip_right([_|T],Tag0,_,R1,S0,S,Tag) :-
    R is R1+1,
    expand_skip_right(T,Tag0,R1,R,S0,S,Tag).

in_pieces(P0,P,Q0,Q,Tag1,Tag) :-
    Length is P-P0,
    (	Length =:= 1
    ->	Tag1=Tag,
	P0=Q0, P=Q
    ;	in_pieces0(P0,P,Q0,Q,1,Tag1,Tag,Length)
    ).

in_pieces0(P0,P,P0,P1,N,Tag,N/Q-Tag,Q) :-
    P0 < P,
    P1 is P0 + 1.

in_pieces0(P0,P,R0,R,N,Tag0,Tag,Q) :-
    P1 is P0 + 1,
    P1 < P,
    N1 is N + 1,
    in_pieces0(P1,P,R0,R,N1,Tag0,Tag,Q).

refs([],[]).
refs([Refs|T0],All) :-
    lists:append(Refs,Rest,All),
    refs(T0,Rest).

keep_tags([],_,[],S,S).
keep_tags([I,_Score|Is],RefsLists,Keep,S0,S) :-
    lists:nth0(I,RefsLists,Refs),
%    score_refs(Refs,Score),
    lists:append(Refs,T,Keep),
    S1 is S0 + 1,
    keep_tags(Is,RefsLists,T,S1,S).

erase_tags([]).
erase_tags([Ref|Refs]) :-
    erase(Ref),
    erase_tags(Refs).

%score_refs([],_).
%score_refs([H|T],Score) :-
%    assertz(score_ref:score_ref(H,Score)),
%    score_refs(T,Score).

%% for Yan
%pos_filter_offline(RefsLists,Solution) :-
%    hdrug_flag(current_ref,Ref),
%    findall(N,survive_offline(RefsLists,Ref,N),Solution0),
%    add_scores(Solution0,Solution).
%
%add_scores([],[]).
%add_scores([H|T],[H,0|T2]):-
%    add_scores(T,T2).
%
%survive_offline(Lists,Ref,N) :-
%    lists:nth0(N,Lists,t(P0,P,Tag)),
%    alpino_suite:input_tag(Ref,_,P0,P,Tag).

