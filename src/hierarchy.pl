:- expects_dialect(sicstus).

:- use_module(hdrug(hdrug_util)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% tree-like pretty printing of lexicon set-up %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% to view the lexicon as if it were a real inheritance
%% hierarchy. Interface to hdrug_call_tree library.

%% for display of lexical hierarchy
%call_leaf(a_lex(Tag,_),Tag).
call_leaf(grammar_rule(Id,_,_),Id).
call_leaf(a_lex(_,_,_,_),'').

%% top of hierarchy
%call_default(sign/2).
call_default(structure/0).

call_clause(A,Body) :-
    hook(alpino_grammar:user_clause(A,Body)).
call_clause(A,Body) :-
    hook(alpino_lex_types:user_clause(A,Body)).

%    (   A = grammar_rule(Id,Head,Ds)
%    ->  alpino_lc_in:grammar_rule(Id,Head,Ds)
%    ;   true
%    ).

%% this always ignores additional predicates used in a definition of a type
call_build_lab(F,_,F).

%% the following are now redundant given the definition of call_build_lab:
call_ignore_clause('=>'/2).
call_ignore_clause('=?>'/2).
call_ignore_clause('==>'/2).
call_ignore_clause('<=>'/2).
call_ignore_clause('=?>'/2).
call_ignore_clause('==?>'/2).
call_ignore_clause('<?=?>'/2).
call_ignore_clause(if_defined/2).
call_ignore_clause(if_defined/3).
call_ignore_clause(unify_except/3).
call_ignore_clause(overwrite/4).

:- public missing_rules/0.

missing_rules :-
    call_clause(structure,[]),
    hdrug_call_tree:l_tree(structure,[],Tree),
    all_rules(Tree,Rules,[]),
    call_leaf(Rule,Id),
    (   call_clause(Rule,_),
        \+ member(Id,Rules),
        format(user_error,"~w not in hierarchy~n",[Id]),
        fail
    ;   true
    ).

all_rules(tree(Name,_,Ds)) -->
    all_rules_ds(Ds,Name).

all_rules_ds([],Name) --> [Name].
all_rules_ds([H|T],_) -->
    all_rules(H),
    all_rules_list(T).

all_rules_list([]) --> [].
all_rules_list([H|T]) -->
    all_rules(H),
    all_rules_list(T).
