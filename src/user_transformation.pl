:- module(alpino_user_transformation, [ user_transformation/8 ]).

%% --------------------------------------------------------------------------------------------- %%

user_transformation(r(Rel,i(X,Cat)),A,B,
                    r(Rel2,i(X,Cat2)),C,D,E,F) :-
    user_transformation(r(Rel,Cat),A,B,
                        r(Rel2,Cat2),C,D,E,F).

user_transformation(r(REL,p(CAT)),B,Ds,
		    r(REL,p(mwu)),B,Ds,_,_) :-
    atom(CAT),
    atom_concat('mwu([',_,CAT).

%% NB: structure of first argument of l(...) of lexical nodes:
%% read_from_treebank/4: for machine generated treebanks
%% read_from_treebank/3: for manually corrected treebanks

%user_transformation(r(REL,l(read_from_treebank(Az,Bz,Pos,TAG1),Cat,Root/[P0,P])),B,[],
%		    r(REL,l(read_from_treebank(Az,Bz,Pos,TAG2),Cat,Root/[P0,P])),B,[],_,_) :-
%    correct_postag(TAG1,TAG2).

user_transformation(r(REL,l(read_from_treebank(Az,Bz1,TAG1),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,Bz2,TAG2),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,J),
    correct_postag_lemma(J,Bz1,Bz2,TAG1,TAG2).

correct_postag_lemma(_,vraag,vragen,Tag,Tag) :-
    atom_concat('WW(',_,Tag).

%% --------------------------------------------------------------------------------------------- %%




/*
user_transformation(r(REL,p(mwu)),B,Ds0,
		    r(REL,p(mwu)),B,Ds,String,_) :-
    lists:member(tree(r(mwp,l(read_from_treebank(_,_,TAG),_,_)),_,_),Ds0),
    TAG \== 'SPEC(deeleigen)',
    !,
    words(Ds0,String,Words),
    mwu_names(Words),
    change_postag(Ds0,Ds,String),
    format(user_error,"~w~n",[Words]).

words([],_String,[]).
words([tree(r(mwp,l(_,_,_/[P0,P])),_,_)|Trees],String,[Word|Words]) :-
    alpino_treebank:get_root(P0,P,String,Word),
    words(Trees,String,Words).

change_postag([],[],_String).
change_postag([H0|T0],[H|T],String) :-
    change_post(H0,H,String),
    change_postag(T0,T,String).

change_post(tree(r(mwp,l(read_from_treebank(Az,_,_),Cat,_/[P0,P])),Ix,[]),
	    tree(r(mwp,l(read_from_treebank(Az,Word,'SPEC(deeleigen)'),Cat,Word/[P0,P])),Ix,[]),String) :-
    alpino_treebank:get_root(P0,P,String,Word).

*/

/*
user_transformation(r(REL,p(mwu)),B,[tree(r(mwp,l(Pos1,Cat1,In)),_,[]),
				     tree(r(mwp,l(Pos2,Cat2,Totaal)),_,[])
				    ],
		    r(REL,p(pp)),B,[tree(r(hd,l(Pos1,Cat1,In)),_,[]),
				    tree(r(obj1,l(Pos2,Cat2,Totaal)),_,[])
				    ],_,_) :-
    In = in/_,
    Totaal = totaal/_.
*/

/*
user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,l(read_from_treebank(Az,Bz0,POS0),Cat,Root0/[P0,P])),
    MOD  = r(VRIJREL,l(read_from_treebank(Az,Bz,POS),Cat,Root/[P0,P])),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    alpino_treebank:get_root(P0,P,String,Word),
    vrijpos(REL,VRIJCAT,VRIJREL,Word,Bz0,Bz,Root0,Root,POS0,POS),
    format(user_error,"~w~n",[Word]).
user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,i(Ix,l(read_from_treebank(Az,Bz0,POS0),Cat,Root0/[P0,P]))),
    MOD  = r(VRIJREL,i(Ix,l(read_from_treebank(Az,Bz,POS),Cat,Root/[P0,P]))),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    alpino_treebank:get_root(P0,P,String,Word),
    vrijpos(REL,VRIJCAT,VRIJREL,Word,Bz0,Bz,Root0,Root,POS0,POS),
    format(user_error,"~w~n",[Word]).

vrijpos(_,_,_,eentje,eentje,één,Root,Root,Tag,Tag) :-
    Tag \= 'SPEC(deeleigen)'.

*/
/*

user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,l(read_from_treebank(Az,Bz0,POS0),Cat,Root0/[P0,P])),
    MOD  = r(VRIJREL,l(read_from_treebank(Az,Bz,POS),Cat,Root/[P0,P])),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    alpino_treebank:get_root(P0,P,String,Word),
    vrijpos(REL,VRIJCAT,VRIJREL,Word,Bz0,Bz,Root0,Root,POS0,POS),
    format(user_error,"~w~n",[Word]).
user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,i(Ix,l(read_from_treebank(Az,Bz0,POS0),Cat,Root0/[P0,P]))),
    MOD  = r(VRIJREL,i(Ix,l(read_from_treebank(Az,Bz,POS),Cat,Root/[P0,P]))),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    alpino_treebank:get_root(P0,P,String,Word),
    vrijpos(REL,VRIJCAT,VRIJREL,Word,Bz0,Bz,Root0,Root,POS0,POS),
    format(user_error,"~w~n",[Word]).






user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(hd,l(read_from_treebank(Az,Bz,POS0),Cat,Root/[P0,P])),
    MOD  = r(hd,l(read_from_treebank(Az,Bz,POS),Cat,Root/[P0,P])),
    DET  = r(det,l(_,_,DetRoot/_)),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    lists:select(tree(DET,_,_),Ds,_),
    alpino_treebank:get_root(P0,P,String,Word),
    vrijpos(DetRoot,Word,POS0,POS),
    format(user_error,"~w~n",[Word]).

vrijpos(de,_,'N(eigen,ev,basis,onz,stan)','N(eigen,ev,basis,zijd,stan)').
vrijpos(het,_,'N(eigen,ev,basis,zijd,stan)','N(eigen,ev,basis,onz,stan)').


user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,l(read_from_treebank(Az,Bz0,POS0),Cat,Root0/[P0,P])),
    MOD  = r(VRIJREL,l(read_from_treebank(Az,Bz,POS),Cat,Root/[P0,P])),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    alpino_treebank:get_root(P0,P,String,Word),
    vrijpos(REL,VRIJCAT,VRIJREL,Word,Bz0,Bz,Root0,Root,POS0,POS),
    format(user_error,"~w --> ~w~n",[Bz0,Word]).
user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,i(Ix,l(read_from_treebank(Az,Bz0,POS0),Cat,Root0/[P0,P]))),
    MOD  = r(VRIJREL,i(Ix,l(read_from_treebank(Az,Bz,POS),Cat,Root/[P0,P]))),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    alpino_treebank:get_root(P0,P,String,Word),
    vrijpos(REL,VRIJCAT,VRIJREL,Word,Bz0,Bz,Root0,Root,POS0,POS),
    format(user_error,"~w --> ~w~n",[Bz0,Word]).

vrijpos(_,_,REL,'Romeinen',_,'Romein',_,'Roemein','SPEC(deeleigen)','N(soort,mv,basis)') :-
    REL \== mwp.
vrijpos(_,_,REL,'Russen',_,'Rus',_,'Rus','SPEC(deeleigen)','N(soort,mv,basis)') :-
    REL \== mwp.
vrijpos(_,_,REL,'Cryptogram',_,cryptogram,_,cryptogram,'SPEC(deeleigen)','N(soort,ev,basis,onz,stan)') :-
    REL \== mwp.

vrijpos(_,_,REL,'Pvda',Bz,Bz,Root,Root,'N(eigen,ev,basis,onz,stan)','N(eigen,ev,basis,zijd,stan)') :-
    REL \== mwp.

vrijpos(_,_,REL,Word,Bz,Bz,Root,Root,'SPEC(deeleigen)','N(eigen,ev,basis,zijd,stan)') :-
    REL \== mwp,
    zijd_naam(Word).
vrijpos(_,_,REL,Word,Bz,Bz,Root,Root,'SPEC(deeleigen)','N(eigen,ev,basis,onz,stan)') :-
    REL \== mwp,
    onz_naam(Word).

vrijpos(_,_,REL,Word,Bz,Bz,Root,Root,'SPEC(deeleigen)','N(eigen,ev,basis,onz,stan)') :-
    REL \== mwp,
    alpino_lex:lexicon(proper_name(_,'LOC'),_,[Word],[],names_dictionary).
vrijpos(_,_,REL,Word,Bz,Bz,Root,Root,'SPEC(deeleigen)','N(eigen,ev,basis,onz,stan)') :-
    REL \== mwp,
    alpino_lex:lexicon(proper_name(_,'ORG'),_,[Word],[],names_dictionary).
vrijpos(_,_,REL,Word,Bz,Bz,Root,Root,'SPEC(deeleigen)','N(eigen,ev,basis,zijd,stan)') :-
    REL \== mwp,
    alpino_lex:lexicon(proper_name(_,'PER'),_,[Word],[],names_dictionary).
vrijpos(_,_,REL,_Word,Bz,Bz,Root,Root,'SPEC(deeleigen)','N(eigen,ev,basis,genus,stan)') :-
    REL \== mwp.


zijd_naam('Mercurius').
zijd_naam('Venus').
zijd_naam('Mars').
zijd_naam('Saturnus').
zijd_naam('Titan').
zijd_naam('Pluto').
zijd_naam('Jupiter').

zijd_naam('Hamida').
zijd_naam('Tour').

onz_naam('Vaticaan').




vrijpos(_,np,det,deze,deze,'ADJ(prenom,basis,met-e,bijz)','VNW(aanw,det,gen,prenom,met-e,rest3)',_,_).

vrijpos(_,np,hd,het,het,'LID(bep,stan,evon)','VNW(pers,pron,stan,red,3,ev,onz)',_,_).
vrijpos(_,du,dp,het,het,'LID(bep,stan,evon)','VNW(pers,pron,stan,red,3,ev,onz)',_,_).
vrijpos(_,_,su,het,het,'LID(bep,stan,evon)','VNW(pers,pron,stan,red,3,ev,onz)',_,_).
vrijpos(_,np,hd,een,één,'LID(onbep,stan,agr)','TW(hoofd,nom,zonder-n,basis)',_,_).
vrijpos(_,np,hd,één,één,'LID(onbep,stan,agr)','TW(hoofd,nom,zonder-n,basis)',_,_).
vrijpos(_,_,obj1,een,één,'LID(onbep,stan,agr)','TW(hoofd,nom,zonder-n,basis)',_,_).
vrijpos(_,_,su,een,één,'LID(onbep,stan,agr)','TW(hoofd,nom,zonder-n,basis)',_,_).

vrijpos(_,np,det,'De',de,R,R,'SPEC(deeleigen)','LID(bep,stan,rest)',_,_).
vrijpos(_,np,det,X,X,R,R,'SPEC(symb)','TW(hoofd,prenom,stan)',_,_) :-
    atom(X),
    atom_codes(X,[L|_]),
    alpino_latin1:isdigit(L).

vrijpos(_,np,det,AtomS,Atom,AtomS,Atom,'SPEC(deeleigen)','N(eigen,ev,basis,gen)',_,_) :-
    atom_concat(Atom,'\'s',AtomS),!.
vrijpos(_,np,det,AtomS,Atom,AtomS,Atom,'SPEC(deeleigen)','N(eigen,ev,basis,gen)',_,_) :-
    atom_concat(Atom,s,AtomS).
*/  

/*
user_transformation(r(REL,l(read_from_treebank(Az,Bz1,TAG1),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,Bz2,TAG2),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,J),
    correct_postag_lemma(J,Bz1,Bz2,TAG1,TAG2).


user_transformation(r(REL,l(read_from_treebank(Az,Bz1,TAG),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,Bz2,TAG),Cat,Root/[P0,P])),B,[],_,_) :-
    correct_lemma(TAG,Bz1,Bz2).


correct_postag_lemma(vroeg,vroeg,vragen,'WW(pv,verl,ev)','WW(pv,verl,ev)').

correct_postag_lemma(uitgebreide,uitbreiden,uitbreiden,'ADJ(prenom,basis,met-e,stan)','WW(vd,prenom,met-e)').
correct_postag_lemma(uitgebreid,uitbreiden,uitbreiden,'ADJ(prenom,basis,zonder)','WW(vd,prenom,zonder)').
correct_postag_lemma(uitgebreid,uitbreiden,uitbreiden,'ADJ(vrij,basis,zonder)','WW(vd,vrij,zonder)').

correct_postag_lemma(verbonden,verbond,verbinden,'WW(vd,vrij,zonder)','WW(vd,vrij,zonder)').

correct_postag_lemma(gevangene,_,vangen,_,'WW(vd,nom,met-e,zonder-n)').
correct_postag_lemma(gevangenen,_,vangen,_,'WW(vd,nom,met-e,mv-n)').

correct_postag_lemma(geraakt,_,geraken,'WW(pv,tgw,met-t)','WW(pv,tgw,met-t)').
correct_postag_lemma(geraakte,_,geraken,'WW(pv,verl,ev)','WW(pv,verl,ev)').
correct_postag_lemma(geraakten,_,geraken,'WW(pv,verl,mv)','WW(pv,verl,mv)').
correct_postag_lemma(geraken,_,geraken,'WW(pv,tgw,mv)','WW(pv,tgw,mv)').
correct_postag_lemma(geraken,_,geraken,'WW(inf,vrij,zonder)','WW(inf,vrij,zonder)').

correct_postag_lemma(gelukt,gelukken,lukken,'WW(vd,vrij,zonder)','WW(vd,vrij,zonder)').


correct_postag_lemma(gedacht,gedenken,denken,'WW(vd,vrij,zonder)','WW(vd,vrij,zonder)').
correct_postag_lemma('Gedacht',gedenken,denken,'WW(vd,vrij,zonder)','WW(vd,vrij,zonder)').


correct_lemma('WW(pv,tgw,ev)',Bz1,Bz2) :-
    correct_lemma_pv1(Bz1,Bz2).

correct_lemma('WW(vd,vrij,zonder)',gevallen,vallen).

correct_lemma('WW(pv,tgw,mv)',vlucht,vluchten).
correct_lemma('WW(pv,tgw,mv)',stam,stammen).

correct_lemma('WW(vd,vrij,zonder)',gevoelen,voelen).

correct_lemma('WW(vd,vrij,zonder)',gezien,zien).

correct_lemma('WW(pv,tgw,met-t)',staat,staan).

correct_lemma('WW(inf,vrij,zonder)',uitvoer,uitvoeren).
correct_lemma('WW(inf,vrij,zonder)',reis,reizen).
correct_lemma('WW(inf,vrij,zonder)',moord,moorden).
correct_lemma('WW(inf,vrij,zonder)',scherm,schermen).
correct_lemma('WW(inf,vrij,zonder)',slaap,slapen).
correct_lemma('WW(inf,vrij,zonder)',feest,feesten).
correct_lemma('WW(inf,vrij,zonder)',fiets,fietsen).
correct_lemma('WW(inf,vrij,zonder)',spook,spoken).
correct_lemma('WW(inf,vrij,zonder)',brand,branden).
correct_lemma('WW(inf,vrij,zonder)',vis,vissen).
correct_lemma('WW(inf,vrij,zonder)',vlucht,vluchten).
correct_lemma('WW(inf,vrij,zonder)',ontwerp,ontwerpen).
correct_lemma('WW(inf,nom,zonder,zonder-n)',reis,reizen).
correct_lemma('WW(inf,nom,zonder,zonder-n)',smelt,smelten).
correct_lemma('WW(inf,nom,zonder,zonder-n)',bestuur,besturen).
correct_lemma('WW(inf,nom,zonder,zonder-n)',inkoop,inkopen).
correct_lemma('WW(inf,nom,zonder,zonder-n)',vis,vissen).
correct_lemma('WW(pv,tgw,ev)',schat,schattenn).
correct_lemma('WW(pv,verl,ev)',was,zijn).
correct_lemma('WW(pv,verl,ev)',ontwikkeld,ontwikkelen).
correct_lemma('WW(pv,verl,mv)',wilde,willen).
correct_lemma('WW(pv,conj,ev)',waar,zijn).
correct_lemma('WW(pv,conj,ev)',koste,kosten).
correct_lemma('WW(pv,conj,ev)',geschiede,geschieden).
correct_lemma('WW(vd,vrij,zonder)',verslag,verslaan).

correct_lemma_pv1(wil,willen).
correct_lemma_pv1(kijk,kijken).
correct_lemma_pv1(lijk,lijken).
correct_lemma_pv1(geef,geven).
correct_lemma_pv1(los,lossen).
correct_lemma_pv1(ga,gaan).
correct_lemma_pv1(lucht,luchten).
correct_lemma_pv1(vlucht,vluchten).
correct_lemma_pv1(stel,stellen).
correct_lemma_pv1(kaart,kaarten).
correct_lemma_pv1(roest,roesten).
correct_lemma_pv1(slaap,slapen).
correct_lemma_pv1(vertrek,vertrekken).

user_transformation(r(REL,l(read_from_treebank(Az,ijzer,'ADJ(prenom,basis,zonder'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,ijzeren,'ADJ(prenom,basis,zonder)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,J),
    lists:member(J,[ijzeren,'IJzeren','Ijzeren','IJZEREN']).

user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,l(read_from_treebank(Az,Bz,POS0),Cat,Root/Pos)),
    MOD  = r(VRIJREL,l(read_from_treebank(Az,Bz,POS),Cat,Root/Pos)),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    vrijpos(REL,VRIJCAT,VRIJREL,POS0,POS,String,Pos).

vrijpos(_,pp,hd,_,'VZ(fin)',String,[P0,P]) :-
    alpino_treebank:get_root(P0,P,String,N),
    prep(N).

prep(bovenaan).
prep(bovenop).
prep(bovenin).
prep(vooraan).
prep(tegenaan).
prep(binnenuit).
prep(dichtbij).
prep(langszij).
prep(onderaan).
prep(achteraan).
prep(achterin).
prep(vlakbij).
prep(bovenop).
prep(voorbij).
prep(middendoor).
prep(vanachter).
*/

correct_postag('WW(vd,postnom,zonder)',
	       'WW(vd,vrij,zonder)').
correct_postag('WW(od,postnom,zonder)',
	       'WW(od,vrij,zonder)').
correct_postag('WW(inf,postnom,zonder)',
	       'WW(inf,vrij,zonder)').
correct_postag('WW(pv,tgw,met-e)',
	       'WW(pv,tgw,met-t)').
correct_postag('WW(vd,prenom,met-e,zonder-n)',
	       'WW(vd,prenom,met-e)').
correct_postag('WW(p,tgw,mv)',
	       'WW(pv,tgw,mv)').
correct_postag('WW(od,nom,nom,met-e,mv-n)',
	       'WW(od,nom,met-e,mv-n)').
correct_postag('WW(inf,nom,zonder,zoner-n)',
	       'WW(inf,nom,zonder,zonder-n)').
correct_postag('WW(pv,tgw,zonder)',
	       'WW(pv,tgw,ev)').
correct_postag('WW(od,nom,met-e,getal-n)',
	       'WW(od,nom,met-e,zonder-n)').
correct_postag('WW(vd,prenom,basis)',
	       'WW(vd,prenom,zonder)').

correct_postag('ADJ(prenom,basis,met-e)',
	       'ADJ(prenom,basis,met-e,stan)').
correct_postag('ADJ(nom,basis,met-e,zonder-n)',
	       'ADJ(nom,basis,met-e,zonder-n,stan)').
correct_postag('ADJ(nom,basis,met-e,stan)',
	       'ADJ(nom,basis,met-e,zonder-n,stan)').
correct_postag('ADJ(prenom,basis,stan)',
	       'ADJ(prenom,basis,zonder)').
correct_postag('ADJ(sup,zonder)',
	       'ADJ(vrij,sup,zonder)').
correct_postag('ADJ(prenom,basis,met-e,zonder-n,stan)',
	       'ADJ(prenom,basis,met-e,stan)').
correct_postag('ADJ(vrij,basis,met-e)',
	       'ADJ(vrij,basis,zonder)').
correct_postag('ADJ(nom,basis,zonder,zonder-e)',
	       'ADJ(nom,basis,zonder,zonder-n)').
correct_postag('ADJ(nom,basis,met-e,mv-m)',
	       'ADJ(nom,basis,met-e,mv-n)').
correct_postag('ADJ(nom,basis,met-e,getal-n)',
	       'ADJ(nom,basis,met-e,zonder-n,stan)').

correct_postag('N(soort,mv,stan)',
	       'N(soort,mv,basis)').
correct_postag('N(soort,ev,basis,onz)',
	       'N(soort,ev,basis,onz,stan)').
correct_postag('N(soort,ev,basis,stan)',
	       'N(soort,ev,basis,onz,stan)').
correct_postag('N(soort,ev,basis,onz,basis)',
	       'N(soort,ev,basis,onz,stan)').
correct_postag('N(soort,basis,mv)',
	       'N(soort,mv,basis)').

correct_postag('VNW(onbep,grad,stan,nom,zonder,sup)',
	       'VNW(onbep,grad,stan,nom,zonder,zonder-n,sup)').
correct_postag('VNW(bez,det,stan,vol,1,mv,nom,met-e,getal-n)',
	       'VNW(bez,det,stan,vol,1,mv,nom,met-e,zonder-n)').
correct_postag('VNW(bez,det,stan,vol,3m,ev,nom,met-e,getal-n)',
	       'VNW(bez,det,stan,vol,3m,ev,nom,met-e,zonder-n)').
correct_postag('VNW(aanw,det,stan,nom,met-e,getal-n)',
	       'VNW(aanw,det,stan,nom,met-e,zonder-n)').
correct_postag('VNW(vb,det,stan,prenom,buiging,agr)',
	       'VNW(vb,det,stan,prenom,met-e,rest)').
correct_postag('VNW(onbep,det,stan,nom,met-e,zonder-n,basis)',
	       'VNW(onbep,det,stan,nom,met-e,zonder-n)').
correct_postag('VNW(betr,pron,stan,vol,3o,ev)',
	       'VNW(betr,det,stan,nom,zonder,zonder-n)').

correct_postag('TW(rang,prenom,met-e)',
	       'TW(rang,prenom,stan)').
correct_postag('TW(hoofd,prenom)',
	       'TW(hoofd,prenom,stan)').
correct_postag('TW(hoofd,prenom,stna)',
	       'TW(hoofd,prenom,stan)').
correct_postag('TW(hoofd,prenom,basis)',
	       'TW(hoofd,prenom,stan)').
correct_postag('TW(hoofd,postnom)',
	       'TW(hoofd,vrij)').

correct_postag('LID(bep,stan,agr)',
	       'LID(bep,stan,rest)').


/*
%user_transformation(r(REL,p(pp)),B,Ds0,
%		    r(REL,p(pp)),B,[tree(MOD,XX,YY)|Ds],_,_) :-
%    MOD0 = r(hd,l(read_from_treebank(Az,Bz,'VZ(fin)'),Cat,Root/[P0,P])),
%    MOD  = r(hd,l(read_from_treebank(Az,Bz,'VZ(init)'),Cat,Root/[P0,P])),
%    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
%    lists:member(tree(r(obj1,l(_,_,RRoot/[Q0,_Q])),_,_),Ds),
%    Q0 > P0,
%    format(user_error,"~w ~w~n",[Root,RRoot]).

user_transformation(r(REL,l(read_from_treebank(Az,'\'t','VNW(pers,pron,stan,red,3,ev,onz)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,het,'VNW(pers,pron,stan,red,3,ev,onz)'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,'-','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,'-','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,'<','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,'<','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,'>','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,'>','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,'—','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,'—','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,'_','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,'_','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,'=','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,'=','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,'/','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,'/','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,',,','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,',,','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(REL,l(read_from_treebank(Az,'`','SPEC(symb)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,'`','LET()'),Cat,Root/[P0,P])),B,[],_,_).

user_transformation(r(hd,l(read_from_treebank(Az,Bz,'ADJ(postnom,comp,zonder)'),Cat,Root/[P0,P])),B,[],
		    r(hd,l(read_from_treebank(Az,Bz,'ADJ(vrij,comp,zonder)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    lists:member(Zijn,[later,'Later','LATER']).

user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,l(read_from_treebank(Az,Bz,POS0),Cat,Root/Pos)),
    MOD  = r(VRIJREL,l(read_from_treebank(Az,Bz,POS),Cat,Root/Pos)),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    vrijpos(REL,VRIJCAT,VRIJREL,POS0,POS,String,Pos).

user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,i(ZZ,l(read_from_treebank(Az,Bz,POS0),Cat,Root/Pos))),
    MOD  = r(VRIJREL,i(ZZ,l(read_from_treebank(Az,Bz,POS),Cat,Root/Pos))),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds),
    vrijpos(REL,VRIJCAT,VRIJREL,POS0,POS,String,Pos).



user_transformation(r(REL,p(CAT)),B,Ds0,
		    r(REL,p(CAT)),B,[tree(r(su,l(read_from_treebank(Ax,Bx,Posy),Catx,Rootx/[P0,P])),_,[])|Ds],String,_) :-
    lists:member(tree(r(hd,l(read_from_treebank(_,_,Posz),_,_)),_,[]),Ds0),
    lists:select(tree(r(su,l(read_from_treebank(Ax,Bx,Posx),Catx,Rootx/[P0,P])),_,[]),Ds0,Ds),
    alpino_treebank:get_root(P0,P,String,ZIJ),
    lists:member(ZIJ,[zij,'Zij','ZIJ',
		      ze, 'Ze', 'ZE']),
    xxx(Posz,Posx,Posy).
user_transformation(r(REL,p(CAT)),B,Ds0,
		    r(REL,p(CAT)),B,[tree(r(su,i(ZZ,l(read_from_treebank(Ax,Bx,Posy),Catx,Rootx/[P0,P]))),_,[])|Ds],String,_) :-
    lists:member(tree(r(hd,l(read_from_treebank(_,_,Posz),_,_)),_,[]),Ds0),
    lists:select(tree(r(su,i(ZZ,l(read_from_treebank(Ax,Bx,Posx),Catx,Rootx/[P0,P]))),_,[]),Ds0,Ds),
    alpino_treebank:get_root(P0,P,String,ZIJ),
    lists:member(ZIJ,[zij,'Zij','ZIJ',
		      ze, 'Ze', 'ZE']),
    xxx(Posz,Posx,Posy).

%% zij
xxx('WW(pv,verl,ev)',  'VNW(pers,pron,nomin,vol,3p,mv)','VNW(pers,pron,nomin,vol,3v,ev,fem)') :- !.
xxx('WW(pv,tgw,ev)',   'VNW(pers,pron,nomin,vol,3p,mv)','VNW(pers,pron,nomin,vol,3v,ev,fem)') :- !.
xxx('WW(pv,tgw,met-t)','VNW(pers,pron,nomin,vol,3p,mv)','VNW(pers,pron,nomin,vol,3v,ev,fem)') :- !.

xxx('WW(pv,verl,mv)',  'VNW(pers,pron,nomin,vol,3v,ev,fem)','VNW(pers,pron,nomin,vol,3p,mv)') :- !.
xxx('WW(pv,tgw,mv)',  'VNW(pers,pron,nomin,vol,3v,ev,fem)','VNW(pers,pron,nomin,vol,3p,mv)') :- !.

%% ze
xxx('WW(pv,verl,mv)',  'VNW(pers,pron,stan,red,3,ev,fem)','VNW(pers,pron,stan,red,3,mv)') :- !.
xxx('WW(pv,tgw,mv)',   'VNW(pers,pron,stan,red,3,ev,fem)','VNW(pers,pron,stan,red,3,mv)') :- !.
xxx('WW(pv,verl,ev)',  'VNW(pers,pron,stan,red,3,mv)','VNW(pers,pron,stan,red,3,ev,fem)') :- !.
xxx('WW(pv,tgw,met-t)','VNW(pers,pron,stan,red,3,mv)','VNW(pers,pron,stan,red,3,ev,fem)') :- !.
xxx('WW(pv,tgw,ev)',   'VNW(pers,pron,stan,red,3,mv)','VNW(pers,pron,stan,red,3,ev,fem)') :- !.

xxx(A,B,B) :-
    format(user_error,"~w ~w~n",[A,B]),
    fail.

*/

%vrijpos(_,mwu,mwp,_,'ADJ(nom,basis,met-e,zonder-n,bijz)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,volle).

%vrijpos(su,np,hd,'N(soort,mv,basis)','WW(inf,vrij,zonder)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,R),
%    lists:member(R,[reizen,'Reizen','REIZEN']).

%vrijpos(_,np,hd,'VNW(aanw,adv-pron,stan,red,3,getal)','VNW(onbep,adv-pron,gen,red,3,getal)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    lists:member(VERD,[er,'Er','ER']).


%vrijpos(_,np,hd,'ADJ(prenom,basis,met-e,stan)','ADJ(nom,basis,met-e,zonder-n,stan)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%
%vrijpos(_,np,hd,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)','VNW(onbep,grad,stan,vrij,zonder,comp)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%
%vrijpos(_,np,hd,'VNW(onbep,det,stan,prenom,met-e,evz)','VNW(onbep,det,stan,nom,met-e,zonder-n)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%
%vrijpos(_,np,hd,'VNW(onbep,det,stan,prenom,met-e,rest)','VNW(onbep,det,stan,nom,met-e,zonder-n)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%
%vrijpos(_,np,hd,'VNW(aanw,det,stan,prenom,met-e,rest)','VNW(aanw,det,stan,nom,met-e,zonder-n)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%
%vrijpos(_,np,hd,'VNW(onbep,grad,stan,prenom,met-e,agr,basis)','VNW(onbep,grad,stan,nom,met-e,zonder-n,basis)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).



%vrijpos(_,np,hd,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%vrijpos(_,np,hd,'VNW(aanw,det,stan,prenom,met-e,rest)','VNW(aanw,det,stan,nom,met-e,zonder-n)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).

%vrijpos(_,np,hd,_,'TW(hoofd,nom,mv-n,basis)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    lists:member(VERD,[miljarden,'Miljarden','MILJARDEN']).
%
%vrijpos(_,np,hd,'ADJ(prenom,basis,zonder)','ADJ(nom,basis,zonder,zonder-n)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).

%vrijpos(_,np,hd,'WW(vd,prenom,met-e)','WW(vd,nom,met-e,zonder-n)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%vrijpos(_,np,hd,'ADJ(prenom,basis,met-e,stan)','ADJ(nom,basis,met-e,zonder-n,stan)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]),
%    lists:member(VERD,[andere,'Andere','ANDERE']).
%vrijpos(_,np,hd,'ADJ(prenom,comp,met-e,stan)','ADJ(nom,comp,met-e,zonder-n,stan)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%vrijpos(_,np,hd,'ADJ(prenom,sup,met-e,stan)','ADJ(nom,sup,met-e,zonder-n,stan)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).

%vrijpos(_,np,hd,'TW(hoofd,prenom,stan)','TW(hoofd,nom,zonder-n,basis)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%
%vrijpos(_,np,hd,'VNW(onbep,det,stan,prenom,zonder,evon)','VNW(onbep,det,stan,vrij,zonder)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).

%vrijpos(_,np,hd,'VNW(aanw,det,stan,prenom,zonder,rest)','VNW(aanw,pron,stan,vol,3,getal)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).

% vrijpos(_,np,hd,'VNW(onbep,grad,stan,prenom,met-e,mv,basis)','VNW(onbep,grad,stan,nom,met-e,zonder-n,basis)',_).

%vrijpos(_,np,hd,'TW(rang,prenom,stan)','TW(rang,nom,zonder-n)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).
%vrijpos(_,np,hd,'VNW(onbep,det,stan,prenom,met-e,rest)','VNW(onbep,det,stan,nom,met-e,zonder-n)',String,[P0,P]) :-
%    alpino_treebank:get_root(P0,P,String,VERD),
%    format(user_error,"~w~n",[VERD]).



/*

user_transformation(r(REL,l(read_from_treebank(Az,Bz,'VNW(onbep,det,stan,prenom,met-e,agr)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,Bz,'VNW(onbep,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Je),
    lists:member(Je,[wat,'Wat','WAT']).




vrijpos(_,_,cmp,'VZ(fin)','VZ(init)',_).
vrijpos(_,_,cmp,'VG(neven)','VG(onder)',_).


vrijpos(_,_,su,'VNW(onbep,grad,stan,prenom,met-e,mv,basis)','VNW(onbep,grad,stan,nom,met-e,zonder-n,basis)',_).
vrijpos(_,_,obj1,'VNW(onbep,grad,stan,prenom,met-e,mv,basis)','VNW(onbep,grad,stan,nom,met-e,zonder-n,basis)',_).
vrijpos(_,_,obj2,'VNW(onbep,grad,stan,prenom,met-e,mv,basis)','VNW(onbep,grad,stan,nom,met-e,zonder-n,basis)',_).
vrijpos(_,_,predm,'VNW(onbep,grad,stan,prenom,met-e,mv,basis)','VNW(onbep,grad,stan,nom,met-e,zonder-n,basis)',_).
vrijpos(_,_,dp,'VNW(onbep,grad,stan,prenom,met-e,mv,basis)','VNW(onbep,grad,stan,nom,met-e,zonder-n,basis)',_).

vrijpos(_,np,mod,'ADJ(vrij,basis,zonder)','ADJ(prenom,basis,zonder)',_).
vrijpos(_,np,mod,'ADJ(vrij,comp,zonder)','ADJ(prenom,comp,zonder)',_).
vrijpos(_,np,mod,'ADJ(vrij,sup,zonder)','ADJ(prenom,sup,zonder)',_).
vrijpos(_,np,mod,'WW(vd,vrij,zonder)','WW(vd,prenom,zonder)',_).
vrijpos(_,np,mod,'WW(od,vrij,zonder)','WW(od,prenom,zonder)',_).

vrijpos(_,np,det,'VNW(aanw,det,stan,nom,met-e,zonder-n)','VNW(aanw,det,stan,prenom,met-e,rest)',_).

vrijpos(_,np,det,'VNW(pr,pron,obl,vol,1,mv)','VNW(bez,det,stan,vol,1,mv,prenom,zonder,evon)',_).
vrijpos(_,np,det,'VNW(aanw,pron,stan,vol,3,getal)','VNW(aanw,det,stan,prenom,zonder,rest)',_).


vrijpos(_,np,det,'VNW(onbep,pron,stan,vol,3o,ev)','VNW(onbep,det,stan,prenom,met-e,agr)',_).
vrijpos(_,np,det,'VNW(vb,pron,stan,vol,3o,ev)','VNW(onbep,det,stan,prenom,met-e,agr)',_).

user_transformation(r(REL,l(read_from_treebank(Az,_,'ADJ(prenom,basis,met-e,stan)'),Cat,_/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,aanstaande,'ADJ(prenom,basis,zonder)'),Cat,aanstaande/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Aanstaande),
    lists:member(Aanstaande,[aanstaande,'Aanstaande','AANSTAANDE']).


vrijpos(np,hd,'TW(hoofd,prenom,stan)','TW(hoofd,nom,zonder-n,basis)',_).
vrijpos(np,hd,'TW(hoofd,vrij)','TW(hoofd,nom,zonder-n,basis)',_).
vrijpos(np,hd,'ADJ(prenom,basis,met-e,stan)','ADJ(nom,basis,met-e,zonder-n,stan)',_).
vrijpos(np,hd,'ADJ(prenom,comp,met-e,stan)','ADJ(nom,comp,met-e,zonder-n,stan)',_).
vrijpos(np,hd,'ADJ(prenom,sup,met-e,stan)','ADJ(nom,sup,met-e,zonder-n,stan)',_).
vrijpos(np,hd,'ADJ(prenom,basis,zonder)','ADJ(nom,basis,zonder,zonder-n)',_).
vrijpos(np,hd,'WW(inf,vrij,zonder)','WW(inf,nom,zonder,zonder-n)',_).

user_transformation(r(det,l(read_from_treebank(Az,Bz,'TW(hoofd,vrij)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'TW(hoofd,prenom,stan)'),Cat,Root/[P0,P])),B,[],_String,_).

user_transformation(r(det,l(read_from_treebank(Az,Bz,'VNW(onbep,grad,stan,vrij,zonder,comp)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)'),Cat,Root/[P0,P])),B,[],_String,_).

user_transformation(r(det,l(read_from_treebank(Az,Bz,'VNW(aanw,det,stan,nom,met-e,zonder-n)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'VNW(aanw,det,stan,prenom,met-e,rest)'),Cat,Root/[P0,P])),B,[],_String,_).

user_transformation(r(det,l(read_from_treebank(Az,Bz,'VNW(onbep,grad,stan,vrij,zonder,basis)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)'),Cat,Root/[P0,P])),B,[],_String,_).


user_transformation(r(M,l(read_from_treebank(Az,Bz,'TW(hoofd,prenom,stan)'),Cat,Root/[P0,P])),B,[],
		    r(M,l(read_from_treebank(Az,Bz,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,teveel).

user_transformation(r(mod,l(read_from_treebank(Az,Bz,'TW(rang,nom,zonder-n)'),Cat,Root/[P0,P])),B,[],
		    r(mod,l(read_from_treebank(Az,Bz,'TW(rang,prenom,stan)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,eerste).





%vrijpos(np,mod,'WW(vd,vrij,zonder)','WW(vd,prenom,zonder)',_).
user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,l(read_from_treebank(Az,Bz,POS0),Cat,Root/Pos)),
    MOD  = r(VRIJREL,l(read_from_treebank(Az,Bz,POS),Cat,Root/Pos)),
    vrijpos(VRIJCAT,VRIJREL,POS0,POS,String),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds).

user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,i(ZZ,l(read_from_treebank(Az,Bz,POS0),Cat,Root/Pos))),
    MOD  = r(VRIJREL,i(ZZ,l(read_from_treebank(Az,Bz,POS),Cat,Root/Pos))),
    vrijpos(VRIJCAT,VRIJREL,POS0,POS,String),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds).


user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,l(read_from_treebank(Az,Bz,POS0),Cat,Root/Pos)),
    MOD  = r(VRIJREL,l(read_from_treebank(Az,Bz,POS),Cat,Root/Pos)),
    vrijpos(VRIJCAT,VRIJREL,POS0,POS,String),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds).

user_transformation(r(REL,p(VRIJCAT)),B,Ds0,
		    r(REL,p(VRIJCAT)),B,[tree(MOD,XX,YY)|Ds],String,_) :-
    MOD0 = r(VRIJREL,i(ZZ,l(read_from_treebank(Az,Bz,POS0),Cat,Root/Pos))),
    MOD  = r(VRIJREL,i(ZZ,l(read_from_treebank(Az,Bz,POS),Cat,Root/Pos))),
    vrijpos(VRIJCAT,VRIJREL,POS0,POS,String),
    lists:select(tree(MOD0,XX,YY),Ds0,Ds).


vrijpos(_,obj1,'VNW(pers,pron,nomin,red,2v,ev)','VNW(pr,pron,obl,red,2v,getal)',_).
vrijpos(_,obj2,'VNW(pers,pron,nomin,red,2v,ev)','VNW(pr,pron,obl,red,2v,getal)',_).
vrijpos(_,se,'VNW(pers,pron,nomin,red,2v,ev)','VNW(pr,pron,obl,red,2v,getal)',_).
vrijpos(_,mwp,'VNW(pers,pron,nomin,red,2v,ev)','VNW(pr,pron,obl,red,2v,getal)',_).

vrijpos(_,det,'VNW(pr,pron,obl,red,2v,getal)','VNW(bez,det,stan,red,2v,ev,prenom,zonder,agr)',_).


user_transformation(r(REL,l(read_from_treebank(Az,',,','TW(hoofd,prenom,stan)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,',,','LET()'),Cat,Root/[P0,P])),B,[],_,_).

vrijpos(smain,mod,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(ssub,mod,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(sv1,mod,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(ppart,mod,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(ap,mod,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(inf,mod,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(ppres,mod,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).

vrijpos(smain,predc,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(ssub,predc,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(sv1,predc,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(ppart,predc,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(ap,predc,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(inf,predc,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).
vrijpos(ppres,predc,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).

vrijpos(du,dp,'ADJ(prenom,basis,zonder)','ADJ(vrij,basis,zonder)',_).

vrijpos(smain,mod,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(ssub,mod,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(sv1,mod,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(ppart,mod,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(ap,mod,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(inf,mod,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(ppres,mod,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).

vrijpos(smain,predc,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(ssub,predc,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(sv1,predc,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(ppart,predc,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(ap,predc,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(inf,predc,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).
vrijpos(ppres,predc,'ADJ(prenom,comp,zonder)','ADJ(vrij,comp,zonder)',_).

vrijpos(smain,mod,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(ssub,mod,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(sv1,mod,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(ppart,mod,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(ap,mod,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(inf,mod,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(ppres,mod,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).

vrijpos(smain,predc,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(ssub,predc,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(sv1,predc,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(ppart,predc,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(ap,predc,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(inf,predc,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).
vrijpos(ppres,predc,'WW(od,prenom,zonder)','WW(od,vrij,zonder)',_).

vrijpos(smain,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)','VNW(onbep,grad,stan,vrij,zonder,comp)',_).
vrijpos(ssub,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)','VNW(onbep,grad,stan,vrij,zonder,comp)',_).
vrijpos(sv1,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)','VNW(onbep,grad,stan,vrij,zonder,comp)',_).
vrijpos(ppart,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)','VNW(onbep,grad,stan,vrij,zonder,comp)',_).
vrijpos(ap,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)','VNW(onbep,grad,stan,vrij,zonder,comp)',_).
vrijpos(inf,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)','VNW(onbep,grad,stan,vrij,zonder,comp)',_).
vrijpos(ppres,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,comp)','VNW(onbep,grad,stan,vrij,zonder,comp)',_).

vrijpos(smain,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',_).
vrijpos(ssub,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',_).
vrijpos(sv1,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',_).
vrijpos(ppart,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',_).
vrijpos(ap,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',_).
vrijpos(inf,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',_).
vrijpos(ppres,mod,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',_).

vrijpos(_,su,'VNW(aanw,det,stan,prenom,zonder,evon)','VNW(aanw,pron,stan,vol,3o,ev)',_).
vrijpos(_,obj1,'VNW(aanw,det,stan,prenom,zonder,evon)','VNW(aanw,pron,stan,vol,3o,ev)',_).
vrijpos(_,obj2,'VNW(aanw,det,stan,prenom,zonder,evon)','VNW(aanw,pron,stan,vol,3o,ev)',_).

vrijpos(_,obj1,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)','VNW(pers,pron,obl,vol,3,getal,fem)',_).

vrijpos(_,su,'VNW(aanw,det,stan,prenom,zonder,rest)','VNW(aanw,pron,stan,vol,3,getal)',_).
vrijpos(_,obj1,'VNW(aanw,det,stan,prenom,zonder,rest)','VNW(aanw,pron,stan,vol,3,getal)',_).
vrijpos(_,obj2,'VNW(aanw,det,stan,prenom,zonder,rest)','VNW(aanw,pron,stan,vol,3,getal)',_).

vrijpos(_,su,'VNW(aanw,det,stan,prenom,met-e,rest)','VNW(aanw,det,stan,nom,met-e,zonder-n)',_).
vrijpos(_,obj1,'VNW(aanw,det,stan,prenom,met-e,rest)','VNW(aanw,det,stan,nom,met-e,zonder-n)',_).
vrijpos(_,obj2,'VNW(aanw,det,stan,prenom,met-e,rest)','VNW(aanw,det,stan,nom,met-e,zonder-n)',_).

vrijpos(_,predm,'VNW(onbep,det,stan,prenom,zonder,evon)','VNW(onbep,det,stan,vrij,zonder)',_).

vrijpos(_,obj1,'TW(hoofd,prenom,stan)','TW(hoofd,vrij)',_).
vrijpos(du,sat,'TW(hoofd,prenom,stan)','TW(hoofd,vrij)',_).
vrijpos(_,su,'TW(hoofd,prenom,stan)','TW(hoofd,vrij)',_).
vrijpos(_,predc,'TW(hoofd,prenom,stan)','TW(hoofd,vrij)',_).

vrijpos(_,obj1,'VNW(bez,det,stan,vol,3,mv,prenom,zonder,agr)','VNW(pers,pron,obl,vol,3p,mv)',_).
vrijpos(_,obj2,'VNW(bez,det,stan,vol,3,mv,prenom,zonder,agr)','VNW(pers,pron,obl,vol,3p,mv)',_).

vrijpos(_,obj1,'VNW(vb,det,stan,prenom,met-e,rest)','VNW(vb,det,stan,nom,met-e,zonder-n)',_).

vrijpos(cp,body,'TW(rang,prenom,stan)','TW(rang,nom,zonder-n)',_).

vrijpos(_,obj1,'VNW(onbep,grad,stan,prenom,zonder,agr,basis)','VNW(onbep,grad,stan,vrij,zonder,basis)',_).

*/

/*
user_transformation(r(det,l(read_from_treebank(Az,Bz,'TW(hoofd,vrij)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'TW(hoofd,prenom,stan)'),Cat,Root/[P0,P])),B,[],String,_).
user_transformation(r(det,l(read_from_treebank(Az,een,'TW(hoofd,prenom,stan)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,een,'LID(onbep,stan,agr)'),Cat,Root/[P0,P])),B,[],String,_).
user_transformation(r(det,l(read_from_treebank(Az,één,'TW(hoofd,prenom,stan)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,een,'LID(onbep,stan,agr)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Z),
    \+ member(Z,[één,'Eén']).


user_transformation(r(su,l(read_from_treebank(Az,het,'LID(bep,stan,evon)'),Cat,Root/[P0,P])),B,[],
		    r(su,l(read_from_treebank(Az,het,'VNW(pers,pron,stan,red,3,ev,onz)'),Cat,Root/[P0,P])),B,[],String,_).
user_transformation(r(predc,l(read_from_treebank(Az,het,'LID(bep,stan,evon)'),Cat,Root/[P0,P])),B,[],
		    r(predc,l(read_from_treebank(Az,het,'VNW(pers,pron,stan,red,3,ev,onz)'),Cat,Root/[P0,P])),B,[],String,_).
user_transformation(r(sup,l(read_from_treebank(Az,het,'LID(bep,stan,evon)'),Cat,Root/[P0,P])),B,[],
		    r(sup,l(read_from_treebank(Az,het,'VNW(pers,pron,stan,red,3,ev,onz)'),Cat,Root/[P0,P])),B,[],String,_).
user_transformation(r(obj1,l(read_from_treebank(Az,het,'LID(bep,stan,evon)'),Cat,Root/[P0,P])),B,[],
		    r(obj1,l(read_from_treebank(Az,het,'VNW(pers,pron,stan,red,3,ev,onz)'),Cat,Root/[P0,P])),B,[],String,_).
user_transformation(r(pobj1,l(read_from_treebank(Az,het,'LID(bep,stan,evon)'),Cat,Root/[P0,P])),B,[],
		    r(pobj1,l(read_from_treebank(Az,het,'VNW(pers,pron,stan,red,3,ev,onz)'),Cat,Root/[P0,P])),B,[],String,_).





user_transformation(r(det,l(read_from_treebank(Az,Bz,'VNW(aanw,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'VNW(aanw,det,stan,prenom,zonder,evon)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT',
		 dit,'Dit','DIT']).

user_transformation(r(det,l(read_from_treebank(Az,Bz,'VG(onder)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'VNW(aanw,det,stan,prenom,zonder,evon)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT']).

user_transformation(r(REL,l(read_from_treebank(Az,voldoend,PT),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,voldoende,PT),Cat,Root/[P0,P])),B,[],String,_).

user_transformation(r(REL,l(read_from_treebank(Az,onvoldoend,PT),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,onvoldoende,PT),Cat,Root/[P0,P])),B,[],String,_).

user_transformation(r(REL,l(read_from_treebank(Az,voldoen,'WW(od,prenom,met-e)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,voldoende,'ADJ(prenom,basis,zonder)'),Cat,Root/[P0,P])),B,[],String,_).

user_transformation(r(REL,l(read_from_treebank(Az,voldoen,'WW(od,vrij,zonder)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,voldoende,'ADJ(vrij,basis,zonder)'),Cat,Root/[P0,P])),B,[],String,_).

user_transformation(r(det,l(read_from_treebank(Az,Bz,'WW(inf,vrij,zonder)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[zijn,'Zijn','ZIJN']).

user_transformation(r(REL,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,persoon,getal)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,Bz,'VNW(aanw,pron,stan,vol,3,getal)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[die,'Die','DIE']),
    member(REL,[su,obj1,obj2,hd]).


user_transformation(r(REL,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,3,ev)'),Cat,Root/[P0,P])),B,[],
		    r(REL,l(read_from_treebank(Az,Bz,'VNW(aanw,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT']),
    member(REL,[su,obj1,obj2,hd]).


user_transformation(r(cmp,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,3,ev)'),Cat,Root/[P0,P])),B,[],
		    r(cmp,l(read_from_treebank(Az,Bz,'VG(onder)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT']).

user_transformation(r(rhd,l(read_from_treebank(Az,Bz,'VG(onder)'),Cat,Root/[P0,P])),B,[],
		    r(rhd,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,3,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT']).

user_transformation(r(cmp,l(read_from_treebank(Az,Bz,'VNW(aanw,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],
		    r(cmp,l(read_from_treebank(Az,Bz,'VG(onder)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT']).

user_transformation(r(cmp,l(read_from_treebank(Az,Bz,'VNW(aanw,det,stan,prenom,zonder,evon)'),Cat,Root/[P0,P])),B,[],
		    r(cmp,l(read_from_treebank(Az,Bz,'VG(onder)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT']).

user_transformation(r(rhd,l(read_from_treebank(Az,Bz,'VNW(aanw,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],
		    r(rhd,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,3,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT']).

user_transformation(r(rhd,l(read_from_treebank(Az,Bz,'VNW(aanw,det,stan,prenom,zonder,evon)'),Cat,Root/[P0,P])),B,[],
		    r(rhd,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,3,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[dat,'Dat','DAT']).

user_transformation(r(det,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,persoon,getal)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'VNW(aanw,det,stan,prenom,zonder,rest)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[die,'Die','DIE']).

user_transformation(r(rhd,l(read_from_treebank(Az,Bz,'VNW(aanw,det,stan,prenom,zonder,rest)'),Cat,Root/[P0,P])),B,[],
		    r(rhd,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,persoon,getal)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[die,'Die','DIE']).

user_transformation(r(rhd,l(read_from_treebank(Az,Bz,'VNW(aanw,pron,stan,vol,3,getal)'),Cat,Root/[P0,P])),B,[],
		    r(rhd,l(read_from_treebank(Az,Bz,'VNW(betr,pron,stan,vol,persoon,getal)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[die,'Die','DIE']).

user_transformation(r(rhd,l(read_from_treebank(Az,Bz,'VNW(onbep,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],
		    r(rhd,l(read_from_treebank(Az,Bz,'VNW(vb,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[wat,'Wat','WAT']).

user_transformation(r(rhd,l(read_from_treebank(Az,Bz,'VNW(excl,pron,stan,vol,3,getal)'),Cat,Root/[P0,P])),B,[],
		    r(rhd,l(read_from_treebank(Az,Bz,'VNW(vb,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[wat,'Wat','WAT']).

user_transformation(r(whd,l(read_from_treebank(Az,Bz,'VNW(onbep,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],
		    r(whd,l(read_from_treebank(Az,Bz,'VNW(vb,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[wat,'Wat','WAT']).

user_transformation(r(whd,l(read_from_treebank(Az,Bz,'VNW(excl,pron,stan,vol,3,getal)'),Cat,Root/[P0,P])),B,[],
		    r(whd,l(read_from_treebank(Az,Bz,'VNW(vb,pron,stan,vol,3o,ev)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[wat,'Wat','WAT']).

user_transformation(r(det,l(read_from_treebank(Az,Bz,VNW1),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,VNW2),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Je),
    ppn(Je,VNW1,VNW2).

ppn(je,'VNW(pers,pron,nomin,red,2v,ev)','VNW(bez,det,stan,red,2v,ev,prenom,zonder,agr)').
ppn(het,'VNW(pers,pron,stan,red,3,ev,onz)','LID(bep,stan,evon)').
ppn('Je','VNW(pers,pron,nomin,red,2v,ev)','VNW(bez,det,stan,red,2v,ev,prenom,zonder,agr)').
ppn('Het','VNW(pers,pron,stan,red,3,ev,onz)','LID(bep,stan,evon)').
		      
ppn(de,'VNW(pers,pron,dial)','LID(bep,stan,rest)').
ppn('De','VNW(pers,pron,dial)','LID(bep,stan,rest)').

ppn(haar,'VNW(pers,pron,obl,vol,3,getal,fem)','VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)').
ppn('Haar','VNW(pers,pron,obl,vol,3,getal,fem)','VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)').

*/


/*
user_transformation(r(REL,CAT),B,Ds0,
		    r(REL,CAT),B,[VC|Ds],String,_) :-
    member(tree(r(hd,l(_,_,Root/_)),_,[]),Ds0),
    member(Root,[heb,ben]),
    VC0 = tree(r(vc,p(ppart)),_,_),
    select(VC0,Ds0,Ds),
    repair_cat(VC0,VC,String).

repair_cat(tree(r(vc,p(ppart)),A,Ds),
	   tree(r(vc,p(inf)),A,Ds),String) :-
    member(tree(r(hd,l(_,_,Root/[P0,P])),_,[]),Ds),
    alpino_treebank:get_root(P0,P,String,Word),
    checkl(Word,inf),
    \+ checkl(Word,psp).   
*/

%user_transformation(r(REL,l(POS,CAT,ROOT0/PS)),A,B,
%		    r(REL,l(POS,CAT,ROOT /PS)),A,B,_,_) :-
%    sub_atom(ROOT0,_,_,_,' '),
%    no_spaces(ROOT0,ROOT).


/*

user_transformation(r(REL,p(CAT)),B,Ds,
		    r(REL,p(mwu)),B,Ds,_,_) :-
    atom_concat('mwu([',_,CAT).

user_transformation(r(REL,p(CAT)),B,Ds0,
		    r(REL,p(CAT)),B,[HD|Ds],String,_) :-
    HD0 = tree(r(hd,_),_,[]),
    select(HD0,Ds0,Ds),
    repair_hd(CAT,HD0,HD,String).

user_transformation(r(det,l(read_from_treebank(Az,Bz,'WW(pv,tgw,mv)'),Cat,Root/[P0,P])),B,[],
		    r(det,l(read_from_treebank(Az,Bz,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)'),Cat,Root/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Zijn),
    member(Zijn,[zijn,'Zijn','ZIJN']).

user_transformation(r(hd,l(read_from_treebank(Az,Bz,'N(soort,ev,basis,onz,stan)'),Cat,bal/[P0,P])),B,[],
		    r(hd,l(read_from_treebank(Az,Bz,'N(soort,ev,basis,zijd,stan)'),Cat,bal/[P0,P])),B,[],String,_) :-
    alpino_treebank:get_root(P0,P,String,Bal),
    member(Bal,[bal,'Bal','BAL']).

repair_hd(Cat,tree(r(hd,l(read_from_treebank(Az,Bz,POS0),
			  CAT,ROOT/[P0,P])),B,[]),
	      tree(r(hd,l(read_from_treebank(Az,Bz,POS ),
			  CAT,ROOT/[P0,P])),B,[]),String) :-
    alpino_treebank:get_root(P0,P,String,Word),
    repair_hd_postag(Cat,POS0,POS,Word).

repair_hd_postag(inf,'WW(pv,tgw,mv)','WW(inf,vrij,zonder)',Word) :-
    checkl(Word,inf).
repair_hd_postag(inf,'WW(vd,vrij,zonder)','WW(inf,vrij,zonder)',Word) :-
    checkl(Word,inf).
repair_hd_postag(inf,'N(soort,mv,basis)','WW(inf,vrij,zonder)',Word) :-
    checkl(Word,inf).
repair_hd_postag(inf,'N(soort,ev,basis,onz,stan)','WW(inf,vrij,zonder)',Word) :-
    checkl(Word,inf).
repair_hd_postag(inf,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)','WW(inf,vrij,zonder)',zijn).

repair_hd_postag(ppart,'WW(pv,tgw,met-t)','WW(vd,vrij,zonder)',Word) :-
    checkl(Word,psp).
repair_hd_postag(ppart,'WW(pv,tgw,ev)','WW(vd,vrij,zonder)',Word) :-
    checkl(Word,psp).
repair_hd_postag(ppart,'WW(inf,vrij,zonder)','WW(vd,vrij,zonder)',Word) :-
    checkl(Word,psp).

repair_hd_postag(ssub,'WW(inf,vrij,zonder)','WW(pv,tgw,mv)',Word) :-
    checkl(Word,mv).
repair_hd_postag(ssub,'WW(vd,vrij,zonder)','WW(pv,tgw,met-t)',Word) :-
    checkl(Word,t).
repair_hd_postag(ssub,'WW(vd,vrij,zonder)','WW(pv,tgw,ev)',Word) :-
    checkl(Word,ev).
repair_hd_postag(ssub,'WW(vd,vrij,zonder)','WW(pv,tgw,mv)',Word) :-
    checkl(Word,mv).
repair_hd_postag(ssub,'WW(vd,vrij,zonder)','WW(pv,verl,mv)',Word) :-
    checkl(Word,verlmv).
repair_hd_postag(ssub,'WW(vd,vrij,zonder)','WW(pv,verl,ev)',Word) :-
    checkl(Word,verlev).
repair_hd_postag(ssub,'WW(vd,prenom,met-e)','WW(pv,verl,ev)',Word) :-
    checkl(Word,verlev).
repair_hd_postag(ssub,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)','WW(pv,tgw,mv)',zijn).
repair_hd_postag(ssub,'ADJ(vrij,basis,zonder)','WW(pv,verl,ev)',vroeg).

repair_hd_postag(smain,'WW(inf,vrij,zonder)','WW(pv,tgw,mv)',Word) :-
    checkl(Word,mv).
repair_hd_postag(smain,'WW(vd,vrij,zonder)','WW(pv,tgw,met-t)',Word) :-
    checkl(Word,t).
repair_hd_postag(smain,'WW(vd,vrij,zonder)','WW(pv,tgw,ev)',Word) :-
    checkl(Word,ev).
repair_hd_postag(smain,'WW(vd,vrij,zonder)','WW(pv,tgw,mv)',Word) :-
    checkl(Word,mv).
repair_hd_postag(smain,'WW(vd,vrij,zonder)','WW(pv,verl,mv)',Word) :-
    checkl(Word,verlmv).
repair_hd_postag(smain,'WW(vd,vrij,zonder)','WW(pv,verl,ev)',Word) :-
    checkl(Word,verlev).
repair_hd_postag(smain,'WW(vd,prenom,met-e)','WW(pv,verl,ev)',Word) :-
    checkl(Word,verlev).
repair_hd_postag(smain,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)','WW(pv,tgw,mv)',zijn).
repair_hd_postag(smain,'ADJ(vrij,basis,zonder)','WW(pv,verl,ev)',vroeg).


repair_hd_postag(sv1,'WW(inf,vrij,zonder)','WW(pv,tgw,mv)',Word) :-
    checkl(Word,mv).
repair_hd_postag(sv1,'WW(vd,vrij,zonder)','WW(pv,tgw,met-t)',Word) :-
    checkl(Word,t).
repair_hd_postag(sv1,'WW(vd,vrij,zonder)','WW(pv,tgw,ev)',Word) :-
    checkl(Word,ev).
repair_hd_postag(sv1,'WW(vd,vrij,zonder)','WW(pv,tgw,mv)',Word) :-
    checkl(Word,mv).
repair_hd_postag(sv1,'WW(vd,vrij,zonder)','WW(pv,verl,mv)',Word) :-
    checkl(Word,verlmv).
repair_hd_postag(sv1,'WW(vd,vrij,zonder)','WW(pv,verl,ev)',Word) :-
    checkl(Word,verlev).
repair_hd_postag(sv1,'WW(vd,prenom,met-e)','WW(pv,verl,ev)',Word) :-
    checkl(Word,verlev).
repair_hd_postag(sv1,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)','WW(pv,tgw,mv)',zijn).
repair_hd_postag(sv1,'ADJ(vrij,basis,zonder)','WW(pv,verl,ev)',vroeg).

repair_hd_postag(sv1,'N(soort,ev,basis,zijd,stan)','WW(pv,tgw,ev)',Word) :-
    checkl(Word,ev).
repair_hd_postag(sv1,'N(soort,ev,basis,onz,stan)','WW(pv,tgw,ev)',Word) :-
    checkl(Word,ev).

checkl(Word,V) :-
    alpino_lex:lexicon(verb(_,Inf,_),_,[Word],[],_,_),
    checklf(V,Inf).

checklf(mv,pl).
checklf(verlmv,past(pl)).
checklf(verlev,past(sg)).
checklf(t,sg3).
checklf(ev,sg1).
checklf(ev,sg).
checklf(inf,inf).
checklf(inf,inf(_)).
checklf(psp,psp).


*/

/*
%% lower conjunctions
user_transformation(r(REL,p(conj)),Index,Ds0,
		    r(REL,CAT),Index,[ tree(r(VC,p(conj)),_,ConjDs) | OrigDs],_,_) :-
    cnj_crd_ds(Ds0,Cnjs,[],Crds,[],CAT),
    coindexed_but_one(Cnjs,OrigDs,RealDs0),
    replace_rel_with_cnj(RealDs0,RealDs,VC),
    lists:append(RealDs,Crds,ConjDs).

cnj_crd_ds([],Cnjs,Cnjs,Crds,Crds,_).
cnj_crd_ds([H|T],Cnjs0,Cnjs,Crds0,Crds,Cat) :-
    cnj_crd_d(H,Cnjs0,Cnjs1,Crds0,Crds1,Cat),
    cnj_crd_ds(T,Cnjs1,Cnjs,Crds1,Crds,Cat).

cnj_crd_d(tree(r(Rel,Cat),Ix,Ds),Cnjs0,Cnjs,Crds0,Crds,Cat2) :-
    (   Rel == cnj
    ->  Cnjs0 = [tree(r(Rel,Cat),Ix,Ds)|Cnjs],
	Crds0 = Crds,
	Cat=Cat2
    ;   Rel == crd,
	Cnjs0 = Cnjs,
	Crds0 = [tree(r(Rel,Cat),Ix,Ds)|Crds]
    ).

%% OrigDs: the antencedents of the coindexed daughters
%% RealDs: the non-coindexed daughter of each conj
coindexed_but_one(Cnjs,Ds2,[Real|RealDs]) :-
    lists:select(tree(_,_,Ds),Cnjs,Cnjs1),  % select the "antecedent"
    lists:select(Real,Ds,Ds2),              % select a real d from the antecedent
    all_ds_coi(Ds2,Cnjs1,Cnjs2),      % remove all coindexed ones
    each_real(Cnjs2,Real,RealDs).     % remaining ones should be all reals


%% each D in Ds should have a coindexed alternative in each of Cnjs
all_ds_coi([],C,C).
all_ds_coi([H|T],C0,C) :-
    get_index_rel(H,IX,REL),
    all_d_coi(C0,IX,REL,C1),
    all_ds_coi(T,C1,C).

get_index_rel(tree(r(REL,i(IX,_)),_,_),IX,REL) :-
    nonvar(IX).
get_index_rel(tree(r(REL,i(IX)),_,[]),IX,REL) :-
    nonvar(IX).

all_d_coi([],_,_,[]).
all_d_coi([tree(A,B,Ds0)|T0],IX,REL,[tree(A,B,Ds)|T]) :-
    lists:select(tree(r(REL,i(IX)),_,[]),Ds0,Ds), 
    all_d_coi(T0,IX,REL,T).

%% every remaining D in DS should be unary tree with same relation as REAL
each_real([],_,[]).
each_real([tree(_,_,[tree(A,B,C)])|T],tree(A,D,E),[tree(A,B,C)|T2]) :-
    each_real(T,tree(A,D,E),T2).

replace_rel_with_cnj([],[],_).
replace_rel_with_cnj([tree(r(REL,CAT),Ix,Ds)|T0],[tree(r(cnj,CAT),Ix,Ds)|T],REL) :-
    replace_rel_with_cnj(T0,T,REL).




*/
