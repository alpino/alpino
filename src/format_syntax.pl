:- module(alpino_format_syntax, [ format_syntax_of_obj/1,
				  format_syntax_of_result/2,
				  format_some_syntax_of_result/3,
				  format_deriv_of_obj/1,
				  format_deriv_of_result/2,
				  format_left_corners_of_obj/1,
				  format_new_left_corners_of_obj/1,
				  format_left_corners_of_result/2,
				  format_new_left_corners_of_result/2,
				  format_frames_of_obj/1,
				  result_to_frames/3,
				  format_frames_of_result/1,
				  format_frames_of_result/2,
				  format_postags_of_obj/1,
				  format_postags_of_result/1,
				  format_postags_of_result/2

				]).

:- expects_dialect(sicstus).

%% otherwise, swi writes hd/mod as 'hd/ (mod)' etc
:- op(0,yfx,mod).

:- use_module(hdrug(hdrug_util)).
:- use_module(library(charsio)).
:- use_module(library(lists)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% BRACKETED STRING %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- public format_syntax_of_obj/1.

format_syntax_of_result(Result,Key) :-
    result_to_bracketed_string(Result,String,[]),
    format("~w|~s~n",[Key,String]).

format_syntax_of_obj(N) :-
    hdrug_flag(current_ref,Key),
    hdrug:object(N,o(Cat,_,_)),
    format_syntax_of_result(Cat,Key).

result_to_bracketed_string(Result,S0,S):-
    alpino_data:result_term(_,_,_,Tree,_,Result),
    tree_to_bracketed_string(Tree,S0,S).

tree_to_bracketed_string(tree(Node,_,Ds,_),S0,S) :-
    Node == robust,
    !,
    tree_to_bracketed_string_ds(Ds,S0,S).

tree_to_bracketed_string(tree(Node,_,Ds,_),S0,S) :-
    functor(Node,Cat,_),
    format_to_chars(" [ @~w",[Cat],S0,S1),
    tree_to_bracketed_string_ds(Ds,S1,S2),
    format_to_chars(" ]",[],S2,S).

tree_to_bracketed_string_ds(lex(ref(_,_,_,Surf,_,_,_,_,_,_,_)),S0,S) :-
    format_to_chars(" ~w",[Surf],S0,S).
tree_to_bracketed_string_ds([],S,S).
tree_to_bracketed_string_ds([H|T],S0,S) :-
    tree_to_bracketed_string(H,S0,S1),
    tree_to_bracketed_string_ds(T,S1,S).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% some syntax %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% only format *certain* brackets;
%% also allows to show non-matching brackets. Therefore, closing brackets
%% are named too.

show_bracket(open,np).
show_bracket(close,np).

:- public format_some_syntax_of_obj/1.

format_some_syntax_of_result(Result,Key,Sentence) :-
    result_to_some_bracketed_string(Result,String,Sentence),
    format("sentence(~q,~q).~n",[Key,String]).

format_some_syntax_of_obj(N) :-
    hdrug_flag(current_ref,Key),
    hdrug:object(N,o(Cat,Sentence,_)),
    format_some_syntax_of_result(Cat,Key,Sentence).

result_to_some_bracketed_string(Result,S,Sent):-
    alpino_data:result_term(_,_,_,Tree,_,Result),
    tree_to_some_bracketed_string(Tree,S,[],Sent).

tree_to_some_bracketed_string(tree(Node,_,Ds,_),S0,S,Sent) :-
    Node == robust,
    !,
    tree_to_some_bracketed_string_ds(Ds,S0,S,Sent).

tree_to_some_bracketed_string(tree(Node,_,Ds,_),S0,S,Sent) :-
    functor(Node,Cat,_),
    (   show_bracket(open,Cat)
    ->  atom_concat('@',Cat,Label),
	S0=['[',Label|S1]
    ;   S0=S1
    ),
    tree_to_some_bracketed_string_ds(Ds,S1,S2,Sent),
    (   show_bracket(close,Cat)
    ->  atom_concat('@',Cat,Label),
	S2=[']',Label|S]
    ;   S2=S
    ).

get_sub(P0,P,Sent,S0,S) :-
    length(Prefix,P0),
    ItLength is P-P0,
    length(It,ItLength),
    append(Prefix,Rest,Sent),
    append(It,_,Rest),
    append(It,S,S0).

tree_to_some_bracketed_string_ds(lex(ref(_,_,_,_,P0,P,_,_,_,_,_)),S0,S,Sent):-
    get_sub(P0,P,Sent,S0,S).
tree_to_some_bracketed_string_ds([],S,S,_).
tree_to_some_bracketed_string_ds([H|T],S0,S,Sent) :-
    tree_to_some_bracketed_string(H,S0,S1,Sent),
    tree_to_some_bracketed_string_ds(T,S1,S,Sent).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% with deriv %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- public format_deriv_of_obj/1.

format_deriv_of_obj(N) :-
    hdrug_flag(current_ref,Key),
    hdrug:object(N,o(Cat,_,_)),
    format_deriv_of_result(Cat,Key).

format_deriv_of_result(Result,Key) :-
    result_to_bracketed_string_deriv(Result,String,[]),
    format("~w|~s~n",[Key,String]).

result_to_bracketed_string_deriv(Result,S0,S):-
    alpino_data:result_term(_,_,_,Tree,_,Result),
    tree_to_bracketed_string_deriv(Tree,S0,S).

tree_to_bracketed_string_deriv(tree(Node,_,Ds,_),S0,S) :-
    Node == robust,
    !,
    tree_to_bracketed_string_ds_deriv(Ds,S0,S).

tree_to_bracketed_string_deriv(
          tree(Node,_,lex(ref(_,Tag,_,Surf,_,_,_,_,_,_,_)),_),S0,S) :-
    !,
    functor(Node,Cat,_),
    display_quoted(Tag,QTag),
    display_quoted(Cat,QCat),
    display_quoted(Surf,QSurf),
    format_to_chars("[('~s','~s'),'~s']",[QTag,QCat,QSurf],S0,S).

tree_to_bracketed_string_deriv(tree(Node,Id,Ds,_),S0,S) :-
    functor(Node,Cat,_),
    display_quoted(Id,QId),
    display_quoted(Cat,QCat),
    format_to_chars("[('~s','~s'),[",[QId,QCat],S0,S1),
    tree_to_bracketed_string_ds_deriv(Ds,S1,S2),
    format_to_chars("]]",[],S2,S).

tree_to_bracketed_string_ds_deriv([],S,S).
tree_to_bracketed_string_ds_deriv([H|T],S0,S) :-
    tree_to_bracketed_string_deriv(H,S0,S1),
    tree_to_bracketed_string_ds_deriv2(T,S1,S).

tree_to_bracketed_string_ds_deriv2([],S,S).
tree_to_bracketed_string_ds_deriv2([H|T],S0,S) :-
    format_to_chars(",",[],S0,S1),
    tree_to_bracketed_string_deriv(H,S1,S2),
    tree_to_bracketed_string_ds_deriv2(T,S2,S).

display_quoted(Term,String) :-
    format_to_chars("~q",[Term],String0),
    escape_q(String0,String).

escape_q([],[]).
escape_q([H|T],String) :-
    (	H == 39
    ->	String = [92,39|StringT]
    ;	H == 92
    ->	String = [92,92|StringT]
    ;   String = [H|StringT]
    ),
    escape_q(T,StringT).

escape_b(Term,String) :-
    format_to_chars("~w",[Term],String0),
    escape_bar(String0,String).

escape_bar([],[]).
escape_bar([H|T],String) :-
    (   H == 124
    ->  String = [95,95|String1]
    ;   String = [H|String1]
    ),
    escape_bar(T,String1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% left corners %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- public
    format_left_corners_of_obj/1,
    format_new_left_corners_of_obj/1.

format_left_corners_of_obj(Obj) :-
    hdrug_flag(current_ref,Key),
    hdrug:object(Obj,o(Cat,_,_)),
    format_left_corners_of_result(Cat,Key).

format_left_corners_of_result(Result,Key) :-
    result_to_deriv_tree(Result,Tree),
    findall(LC,tree_to_left_corners(Tree,LC),LCS),
    format_left_corners(LCS,Key).

format_new_left_corners_of_obj(Obj) :-
    hdrug_flag(current_ref,Key),
    hdrug:object(Obj,o(Cat,_,_)),
    format_new_left_corners_of_result(Cat,Key).

format_new_left_corners_of_result(Result,Key) :-
    result_to_deriv_tree(Result,Tree),
    tree_to_new_left_corners(Tree,LCS),
    format_left_corners(LCS,Key).

format_left_corners([],_).
format_left_corners([H|T],Key) :-
    format(user_error,"LEFTCORNER#",[]),
    format_left_corner(H,Key),
    format_left_corners(T,Key).

format_left_corner(lc(Goal,LC,His),Key) :-
    format(user_error,"~w|~q",[Key,Goal]),
    format_left_corner_his([LC|His]).

format_left_corner_his([]) :-
    format(user_error,"~n",[]).
format_left_corner_his([H|T]) :-
    format(user_error,"|~q",[H]),
    format_left_corner_his(T).

tree_to_new_left_corners(Tree,LCS) :-
    findall(LC,tree_to_left_corners(Tree,LC),LCS0),
    filter_new_left_corners(LCS0,LCS).

tree_to_left_corners(tree(robust,_,DS),LC) :-
    !,
    member(Tree,DS),
    tree_to_left_corners(Tree,LC).

tree_to_left_corners(tree(Rule,TopCat,Sub),lc(GoalCat,Start,Path)) :-
    find_goal_or_self(tree(Rule,TopCat,Sub),tree(Goal,GoalCat,GoalDs)),
    find_left_corner([tree(Goal,GoalCat,GoalDs)],Path0),
    reverse(Path0,[Start|Path]).

find_goal_or_self(Tree,Tree).
find_goal_or_self(Tree0,Tree) :-
    subtree(Tree0,tree(_,_,[_|Ds])),
    member(Tree,Ds).

subtree(ST,ST).
subtree(tree(_,_,List),Tree) :-
    member(Tree0,List),
    subtree(Tree0,Tree).

find_left_corner([],[]).
find_left_corner([tree(Name,_,Ds)|_],[Name|Rules]) :-
    find_left_corner(Ds,Rules).

result_to_deriv_tree(Result,DerivTree) :-
    alpino_data:result_term(_,_,_,Tree,_,Result),
    tree_to_deriv_tree(Tree,DerivTree).

tree_to_deriv_tree(tree(Term,R,Ds0,_),tree(Name,Fun,Ds)) :-
    functor(Term,Fun,_Ar),
    (   Ds0 = lex(Ref)
    ->  lex_rule_name(Term,Ref,Name),
	Ds=[]
    ;   Ds0 = [],
        R = call(R1)
    ->  Name = R1,
        Ds=[]
    ;   Ds0 = []
    ->  Name=gap(R),
	Ds=[]
    ;   Name=R,
	tree_to_deriv_tree_ds(Ds0,Ds)
    ).

tree_to_deriv_tree_ds([],[]).
tree_to_deriv_tree_ds([H0|T0],[H|T]) :-
    tree_to_deriv_tree(H0,H),
    tree_to_deriv_tree_ds(T0,T).

lex_rule_name(_,ref(_ClassTag,Tag0,_,_,_,_,_,_,_,N,_),lex(Tag)) :-
    alpino_guides:tr_tag(Tag0-N,Tag).
lex_rule_name(skip,_,skip).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% RESULT to POSTAGS %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% cgn/dcoi/lassy postags, not Alpino postags (these are called
%% frames here)

:- public format_postags_of_obj/1.

format_postags_of_obj(N) :-
    hdrug:object(N,o(Result,_,_)),
    format_postags_of_result(Result).

format_postags_of_result(Result) :-
    result_to_frames(Result,Frames,_),
    result_to_skips_and_cats(Result,Skips,Cats),
    frames_to_postags(Frames,Postags),
    format_postags(Postags,none,Skips,Cats).

format_postags_of_result(Result,Key) :-
    result_to_frames(Result,Frames,_),
    result_to_skips_and_cats(Result,Skips,Cats),
    frames_to_postags(Frames,Postags),
    format_postags(Postags,Key,Skips,Cats).

frames_to_postags([],[]).
frames_to_postags([Frame|Frames],[Postag|Postags]):-
    frame_to_postag(Frame,Postag),
    frames_to_postags(Frames,Postags).

frame_to_postag(Context-frame(P0,P,Q0,Q,Stem,Frame,Surf,His),
		Context-frame(P0,P,Q0,Q,Stem,Postag,Surf,His)):-
    (   alpino_postags:cgn_postag(Frame,Postag,Stem)
    ->  true
    ;   Frame = Postag,
	debug_message(1,
		      "warning: no mapping to postag for ~w~n",
		      [Frame]
		     )
    ).    

format_postags([],_,_,_).
format_postags([POSTAG|Fs],Key,Skips,Cats) :-
    format_postag(POSTAG,Key,Skips,Cats),
    format_postags(Fs,Key,Skips,Cats).

format_postag(Context-frame(P0,P,_,_,Stem0,Postag,Surf0,His),Key,Skips,Cats) :-
    escape_b(Surf0,Surf),
    escape_b(Stem0,Stem),
    format(user_error,
           "POSTAG#~s|~w|~w|~w|~w|~w|~w|~s|~w|~w~n",
	   [Surf,Postag,Key,P0,P,His,Context,Stem,Skips,Cats]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% RESULT to FRAMES %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- public format_frames_of_obj/1.

format_frames_of_obj(N) :-
    hdrug:object(N,o(Result,_,_)),
    format_frames_of_result(Result).

format_frames_of_result(Result) :-
    result_to_frames(Result,Frames,_),
    result_to_skips_and_cats(Result,Skips,Cats),
    format_frames(Frames,none,Skips,Cats).

format_frames_of_result(Result,Key) :-
    result_to_frames(Result,Frames,_),
    result_to_skips_and_cats(Result,Skips,Cats),
    format_frames(Frames,Key,Skips,Cats).

result_to_frames(Result,Fs,Fs1) :-
    alpino_data:result_term(_,_,_,_,Fs0,Result),
    expand_skips_frames(Fs0,Fs1),
    add_context_frames(Fs1,Fs,Result).

format_frames([],_,_,_).
format_frames([FRAME|Fs],Key,Skips,Cats) :-
    format_frame(FRAME,Key,Skips,Cats),
    format_frames(Fs,Key,Skips,Cats).

format_frame(Context-frame(P0,P,_,_,Stem0,Frame,Surf0,His),Key,Skips,Cats) :-
    escape_b(Surf0,Surf),
    escape_b(Stem0,Stem),
    prettyvars(Frame),
    format(user_error,
           "FRAME#~s|~q|~w|~w|~w|~w|~w|~s|~w|~w~n",
	   [Surf,Frame,Key,P0,P,His,Context,Stem,Skips,Cats]).

expand_skips_frames([],[]).
expand_skips_frames([H|T],Fs) :-
    expand_skips_frame(H,Fs,Fs0),
    expand_skips_frames(T,Fs0).

expand_skips_frame(frame(P0,P,Q0,Q,Stem,Frame,Surf,skip(_,L,R,His)),Fs0,Fs) :-
    !,
    expand_skips_(L,P0,P1,Fs0,Fs1),
    Tag = frame(P1,P2,Q0,Q,Stem,Frame,Surf,His),
    Fs1=[Tag|Fs2],
    length(R,Rlen),P2 is P-Rlen,
    expand_skips_(R,P2,P,Fs2,Fs).

expand_skips_frame(F,[F|Fs],Fs).

expand_skips_([],P,P,Fs,Fs).
expand_skips_([Token|Skips],P0,P,
		  [frame(P0,P1,P0,P1,Token,skip,Token,skip)|Fs0],Fs):-
    P1 is P0+1,
    expand_skips_(Skips,P1,P,Fs0,Fs).

result_to_skips_and_cats(Result,Skips,Cats) :-
    alpino_data:result_term(_,_,_,tree(robust,robust,Ds,_),_,Result),
    result_to_skips_and_cats(Ds,0,Skips,0,Cats),
    !.
result_to_skips_and_cats(_,0,0).

result_to_skips_and_cats([],S,S,C,C).
result_to_skips_and_cats([tree(_,R,_,_)|T],S0,S,C0,C) :-
    (   nonvar(R),
	R = robust_skips(_)
    ->  S1 is S0+1,
        C1 is C0
    ;   S1 is S0,
	C1 is C0+1
    ),
    result_to_skips_and_cats(T,S1,S,C1,C).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% ADD CONTEXT FRAMES %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

add_context_frames(Frs0,Frs,Result) :-
    alpino_data:result_term(_,_,_,Tree,_,Result),
    find_context_frame_tree(Tree,pre,_,CFrames,[]),
    add_context_frames_(Frs0,Frs,CFrames).

add_context_frames_([],[],_).
add_context_frames_([  frame(P0,P,Q0,Q,X,Frame,Surf,His)|T],
		   [C-frame(P0,P,Q0,Q,X,Frame,Surf,His)|T2],Cs) :-
    (   member(f(P0,P,C),Cs)
    ->  true
    ;   member(f(Z0,Z,C),Cs),
	Z0 =< P0,
	 P =< Z
    ->  true
    ;   C=pre
    ),
    add_context_frames_(T,T2,Cs).

find_context_frame_tree_each([],_,Cs,Cs).
find_context_frame_tree_each([H|T],C0,Cs0,Cs) :-
    find_context_frame_tree(H,C0,_,Cs0,Cs1),
    find_context_frame_tree_each(T,C0,Cs1,Cs).

find_context_frame_tree(tree(Node,_,Ds,_),C0,_C,Cs0,Cs) :-
    Node == robust,
    !,
    find_context_frame_tree_each(Ds,C0,Cs0,Cs).
find_context_frame_tree(tree(Node,_,_,_),_,_,Cs,Cs) :-
    Node == skip,
    !.

find_context_frame_tree(tree(Node,_,Ds,_),C0,C,Cs0,Cs) :-
    context_type_of_node(Node,Cx),
    find_context_frame_tree(Cx,Ds,C0,C,Cs0,Cs).

find_context_frame_tree(embed,Ds,C,C,Cs0,Cs) :-
    find_context_frame_tree_ds(Ds,pre,_,Cs0,Cs).
find_context_frame_tree(swap,Ds,_,post,Cs0,Cs):-
    find_context_frame_tree_ds(Ds,post,_C,Cs0,Cs).
find_context_frame_tree(cont,Ds,C0,C,Cs0,Cs) :-
    find_context_frame_tree_ds(Ds,C0,C,Cs0,Cs).

find_context_frame_tree_ds(lex(ref(_,_,_,_,P0,P,_,_,_,_,_)),
			   C,C,[f(P0,P,C)|Cs],Cs).
find_context_frame_tree_ds([],C,C,Cs,Cs).
find_context_frame_tree_ds([H|T],C0,C,Cs0,Cs) :-
    find_context_frame_tree(H,C0,C1,Cs0,Cs1),
    find_context_frame_tree_ds(T,C1,C,Cs1,Cs).

context_type_of_node(Node,Type) :-
    (   alpino_data:context_embed_node(Node)
    ->  Type = embed
    ;   alpino_data:context_swap_node(Node)
    ->  Type = swap
    ;   Type = cont
    ).

filter_new_left_corners([],[]).
filter_new_left_corners([H|T],L) :-
    filter_new_left_corner(H,L,L1),
    filter_new_left_corners(T,L1).

filter_new_left_corner(H,L,L1):-
    (   not_new_left_corner(H)
    ->  L=L1
    ;   L = [H|L1]
    ).

not_new_left_corner(lc(Goal,First,Rest)) :-
    rewrite_first(First,First2),
    lists:append(Rest,[finish],Rest2),
    not_new_left_corner(Rest2,Goal,First2).

rewrite_first(put_val,put_val).
rewrite_first(get_val,get_val).
rewrite_first(gap(X),gap(X)).
rewrite_first(lex(Cat),lex(Cat2)) :-
    alpino_guides:tr_tag(Cat,Cat2).

not_new_left_corner([H|T],Goal,First) :-
    lists:reverse([First,H|T],List),
    alpino_guides:check_connect(Goal,List).

