%% General predicates for dealing with character encodings.  Right now we
%% handle all of latin1.  When the system is extended to deal with mixed
%% Burmese/Cherokee texts, this will require modification.
%%
%% as of today, we use latin9. I don't think any changes are required in
%% this file.
%%
%% now we use UTF-8. Still, no changes required, although perhaps there are
%% upper characters in Cherokee that we haven't captured.

:- module(alpino_latin1, [isupper/1,
			  islower/1,
			  isdigit/1,
			  isalpha/1,
			  isaccented/1,
			  accent_chars/2,
			  deaccent_chars/2, deaccent_chars/4, deaccent/2,
			  toupper/2, 
			  tolower/2]).

:- expects_dialect(sicstus).

isupper(65).  isupper(66). isupper(67). isupper(68). isupper(69). isupper(70).
isupper(71).  isupper(72). isupper(73). isupper(74). isupper(75). isupper(76).
isupper(77).  isupper(78). isupper(79). isupper(80). isupper(81). isupper(82).
isupper(83).  isupper(84). isupper(85). isupper(86). isupper(87). isupper(88).
isupper(89).  isupper(90). isupper(192). isupper(193). isupper(194). 
isupper(195). isupper(196). isupper(197). isupper(198). isupper(199). 
isupper(200). isupper(201). isupper(202). isupper(203). isupper(204). 
isupper(205). isupper(206). isupper(207). isupper(208). isupper(209).
isupper(210). isupper(211). isupper(212). isupper(213). isupper(214).
isupper(216). isupper(217). isupper(218). isupper(219). isupper(220).
isupper(221). isupper(222).

islower(97). islower(98). islower(99). islower(100). islower(101).
islower(102). islower(103). islower(104). islower(105). islower(106).
islower(107). islower(108). islower(109). islower(110). islower(111).
islower(112). islower(113). islower(114). islower(115). islower(116).
islower(117). islower(118). islower(119). islower(120). islower(121).
islower(122). islower(223). islower(224). islower(225). islower(226).
islower(227). islower(228). islower(229). islower(230). islower(231).
islower(232). islower(233). islower(234). islower(235). islower(236).
islower(237). islower(238). islower(239). islower(240). islower(241).
islower(242). islower(243). islower(244). islower(245). islower(246).
islower(248). islower(249). islower(250). islower(251). islower(252).
islower(253). islower(254). islower(255).

isdigit(48). isdigit(49). isdigit(50). isdigit(51). isdigit(52).
isdigit(53). isdigit(54). isdigit(55). isdigit(56). isdigit(57).

isalpha(65). isalpha(66). isalpha(67). isalpha(68). isalpha(69).
isalpha(70). isalpha(71). isalpha(72). isalpha(73). isalpha(74).
isalpha(75). isalpha(76). isalpha(77). isalpha(78). isalpha(79).
isalpha(80). isalpha(81). isalpha(82). isalpha(83).
isalpha(84). isalpha(85). isalpha(86). isalpha(87). isalpha(88).
isalpha(89). isalpha(90). isalpha(97). isalpha(98). isalpha(99).
isalpha(100). isalpha(101). isalpha(102). isalpha(103). isalpha(104).
isalpha(105). isalpha(106). isalpha(107). isalpha(108). isalpha(109).
isalpha(110). isalpha(111). isalpha(112). isalpha(113). isalpha(114).
isalpha(115). isalpha(116). isalpha(117). isalpha(118). isalpha(119).
isalpha(120). isalpha(121). isalpha(122). isalpha(192). isalpha(193).
isalpha(194). isalpha(195). isalpha(196). isalpha(197). isalpha(198).
isalpha(199). isalpha(200). isalpha(201). isalpha(202). isalpha(203).
isalpha(204). isalpha(205). isalpha(206). isalpha(207). isalpha(208).
isalpha(209). isalpha(210). isalpha(211). isalpha(212). isalpha(213).
isalpha(214). isalpha(216). isalpha(217). isalpha(218). isalpha(219).
isalpha(220). isalpha(221). isalpha(222). isalpha(223). isalpha(224).
isalpha(225). isalpha(226). isalpha(227). isalpha(228). isalpha(229).
isalpha(230). isalpha(231). isalpha(232). isalpha(233). isalpha(234).
isalpha(235). isalpha(236). isalpha(237). isalpha(238). isalpha(239).
isalpha(240). isalpha(241). isalpha(242). isalpha(243). isalpha(244).
isalpha(245). isalpha(246). isalpha(248). isalpha(249). isalpha(250).
isalpha(251). isalpha(252). isalpha(253). isalpha(254). isalpha(255).

isaccented(192). isaccented(193). isaccented(194). isaccented(195).
isaccented(196). isaccented(197). isaccented(199). isaccented(200).
isaccented(201). isaccented(202). isaccented(203). isaccented(204).
isaccented(205). isaccented(206). isaccented(207). isaccented(209).
isaccented(210). isaccented(211). isaccented(212). isaccented(213).
isaccented(214). isaccented(216). isaccented(217). isaccented(218).
isaccented(219). isaccented(220). isaccented(221). isaccented(224).
isaccented(225). isaccented(226). isaccented(227). isaccented(228).
isaccented(229). isaccented(231). isaccented(232). isaccented(233).
isaccented(234). isaccented(235). isaccented(236). isaccented(237).
isaccented(238). isaccented(239). isaccented(241). isaccented(242).
isaccented(243). isaccented(244). isaccented(245). isaccented(246).
isaccented(248). isaccented(249). isaccented(250). isaccented(251).
isaccented(252). isaccented(253). isaccented(255). 

toupper(0,0). toupper(1,1). toupper(2,2). toupper(3,3). toupper(4,4).
toupper(5,5). toupper(6,6). toupper(7,7). toupper(8,8). toupper(9,9).
toupper(10,10). toupper(11,11). toupper(12,12). toupper(13,13). toupper(14,14).
toupper(15,15). toupper(16,16). toupper(17,17). toupper(18,18). toupper(19,19).
toupper(20,20). toupper(21,21). toupper(22,22). toupper(23,23). toupper(24,24).
toupper(25,25). toupper(26,26). toupper(27,27). toupper(28,28). toupper(29,29).
toupper(30,30). toupper(31,31). toupper(32,32). toupper(33,33). toupper(34,34).
toupper(35,35). toupper(36,36). toupper(37,37). toupper(38,38). toupper(39,39).
toupper(40,40). toupper(41,41). toupper(42,42). toupper(43,43). toupper(44,44).
toupper(45,45). toupper(46,46). toupper(47,47). toupper(48,48). toupper(49,49).
toupper(50,50). toupper(51,51). toupper(52,52). toupper(53,53). toupper(54,54).
toupper(55,55). toupper(56,56). toupper(57,57). toupper(58,58). toupper(59,59).
toupper(60,60). toupper(61,61). toupper(62,62). toupper(63,63). toupper(64,64).
toupper(65,65). toupper(66,66). toupper(67,67). toupper(68,68). toupper(69,69).
toupper(70,70). toupper(71,71). toupper(72,72). toupper(73,73). toupper(74,74).
toupper(75,75). toupper(76,76). toupper(77,77). toupper(78,78). toupper(79,79).
toupper(80,80). toupper(81,81). toupper(82,82). toupper(83,83). toupper(84,84).
toupper(85,85). toupper(86,86). toupper(87,87). toupper(88,88). toupper(89,89).
toupper(90,90). toupper(91,91). toupper(92,92). toupper(93,93). toupper(94,94).
toupper(95,95). toupper(96,96). toupper(97,65). toupper(98,66). toupper(99,67).
toupper(100,68). toupper(101,69). toupper(102,70). toupper(103,71).
toupper(104,72). toupper(105,73). toupper(106,74). toupper(107,75).
toupper(108,76). toupper(109,77). toupper(110,78). toupper(111,79).
toupper(112,80). toupper(113,81). toupper(114,82). toupper(115,83).
toupper(116,84). toupper(117,85). toupper(118,86). toupper(119,87).
toupper(120,88). toupper(121,89). toupper(122,90). toupper(123,123).
toupper(124,124). toupper(125,125). toupper(126,126). toupper(127,127).
toupper(128,128). toupper(129,129). toupper(130,130). toupper(131,131).
toupper(132,132). toupper(133,133). toupper(134,134). toupper(135,135).
toupper(136,136). toupper(137,137). toupper(138,138). toupper(139,139).
toupper(140,140). toupper(141,141). toupper(142,142). toupper(143,143).
toupper(144,144). toupper(145,145). toupper(146,146). toupper(147,147).
toupper(148,148). toupper(149,149). toupper(150,150). toupper(151,151).
toupper(152,152). toupper(153,153). toupper(154,154). toupper(155,155).
toupper(156,156). toupper(157,157). toupper(158,158). toupper(159,159).
toupper(160,160). toupper(161,161). toupper(162,162). toupper(163,163).
toupper(164,164). toupper(165,165). toupper(166,166). toupper(167,167).
toupper(168,168). toupper(169,169). toupper(170,170). toupper(171,171).
toupper(172,172). toupper(173,173). toupper(174,174). toupper(175,175).
toupper(176,176). toupper(177,177). toupper(178,178). toupper(179,179).
toupper(180,180). toupper(181,181). toupper(182,182). toupper(183,183).
toupper(184,184). toupper(185,185). toupper(186,186). toupper(187,187).
toupper(188,188). toupper(189,189). toupper(190,190). toupper(191,191).
toupper(192,192). toupper(193,193). toupper(194,194). toupper(195,195).
toupper(196,196). toupper(197,197). toupper(198,198). toupper(199,199).
toupper(200,200). toupper(201,201). toupper(202,202). toupper(203,203).
toupper(204,204). toupper(205,205). toupper(206,206). toupper(207,207).
toupper(208,208). toupper(209,209). toupper(210,210). toupper(211,211).
toupper(212,212). toupper(213,213). toupper(214,214). toupper(215,215).
toupper(216,216). toupper(217,217). toupper(218,218). toupper(219,219).
toupper(220,220). toupper(221,221). toupper(222,222). toupper(223,223).
toupper(224,192). toupper(225,193). toupper(226,194). toupper(227,195).
toupper(228,196). toupper(229,197). toupper(230,198). toupper(231,199).
toupper(232,200). toupper(233,201). toupper(234,202). toupper(235,203).
toupper(236,204). toupper(237,205). toupper(238,206). toupper(239,207).
toupper(240,208). toupper(241,209). toupper(242,210). toupper(243,211).
toupper(244,212). toupper(245,213). toupper(246,214). toupper(247,247).
toupper(248,216). toupper(249,217). toupper(250,218). toupper(251,219).
toupper(252,220). toupper(253,221). toupper(254,222). toupper(255,255).

tolower(0,0). tolower(1,1). tolower(2,2). tolower(3,3). tolower(4,4).
tolower(5,5). tolower(6,6). tolower(7,7). tolower(8,8). tolower(9,9).
tolower(10,10). tolower(11,11). tolower(12,12). tolower(13,13). tolower(14,14).
tolower(15,15). tolower(16,16). tolower(17,17). tolower(18,18). tolower(19,19).
tolower(20,20). tolower(21,21). tolower(22,22). tolower(23,23). tolower(24,24).
tolower(25,25). tolower(26,26). tolower(27,27). tolower(28,28). tolower(29,29).
tolower(30,30). tolower(31,31). tolower(32,32). tolower(33,33). tolower(34,34).
tolower(35,35). tolower(36,36). tolower(37,37). tolower(38,38). tolower(39,39).
tolower(40,40). tolower(41,41). tolower(42,42). tolower(43,43). tolower(44,44).
tolower(45,45). tolower(46,46). tolower(47,47). tolower(48,48). tolower(49,49).
tolower(50,50). tolower(51,51). tolower(52,52). tolower(53,53). tolower(54,54).
tolower(55,55). tolower(56,56). tolower(57,57). tolower(58,58). tolower(59,59).
tolower(60,60). tolower(61,61). tolower(62,62). tolower(63,63). tolower(64,64).
tolower(65,97). tolower(66,98). tolower(67,99). tolower(68,100).
tolower(69,101). tolower(70,102). tolower(71,103). tolower(72,104).
tolower(73,105). tolower(74,106). tolower(75,107). tolower(76,108).
tolower(77,109). tolower(78,110). tolower(79,111). tolower(80,112).
tolower(81,113). tolower(82,114). tolower(83,115). tolower(84,116).
tolower(85,117). tolower(86,118). tolower(87,119). tolower(88,120).
tolower(89,121). tolower(90,122). tolower(91,91). tolower(92,92).
tolower(93,93). tolower(94,94). tolower(95,95). tolower(96,96).
tolower(97,97). tolower(98,98). tolower(99,99). tolower(100,100).
tolower(101,101). tolower(102,102). tolower(103,103). tolower(104,104).
tolower(105,105). tolower(106,106). tolower(107,107). tolower(108,108).
tolower(109,109). tolower(110,110). tolower(111,111). tolower(112,112).
tolower(113,113). tolower(114,114). tolower(115,115). tolower(116,116).
tolower(117,117). tolower(118,118). tolower(119,119). tolower(120,120).
tolower(121,121). tolower(122,122). tolower(123,123). tolower(124,124).
tolower(125,125). tolower(126,126). tolower(127,127). tolower(128,128).
tolower(129,129). tolower(130,130). tolower(131,131). tolower(132,132).
tolower(133,133). tolower(134,134). tolower(135,135). tolower(136,136).
tolower(137,137). tolower(138,138). tolower(139,139). tolower(140,140).
tolower(141,141). tolower(142,142). tolower(143,143). tolower(144,144).
tolower(145,145). tolower(146,146). tolower(147,147). tolower(148,148).
tolower(149,149). tolower(150,150). tolower(151,151). tolower(152,152).
tolower(153,153). tolower(154,154). tolower(155,155). tolower(156,156).
tolower(157,157). tolower(158,158). tolower(159,159). tolower(160,160).
tolower(161,161). tolower(162,162). tolower(163,163). tolower(164,164).
tolower(165,165). tolower(166,166). tolower(167,167). tolower(168,168).
tolower(169,169). tolower(170,170). tolower(171,171). tolower(172,172).
tolower(173,173). tolower(174,174). tolower(175,175). tolower(176,176).
tolower(177,177). tolower(178,178). tolower(179,179). tolower(180,180).
tolower(181,181). tolower(182,182). tolower(183,183). tolower(184,184).
tolower(185,185). tolower(186,186). tolower(187,187). tolower(188,188).
tolower(189,189). tolower(190,190). tolower(191,191). tolower(192,224).
tolower(193,225). tolower(194,226). tolower(195,227). tolower(196,228).
tolower(197,229). tolower(198,230). tolower(199,231). tolower(200,232).
tolower(201,233). tolower(202,234). tolower(203,235). tolower(204,236).
tolower(205,237). tolower(206,238). tolower(207,239). tolower(208,240).
tolower(209,241). tolower(210,242). tolower(211,243). tolower(212,244).
tolower(213,245). tolower(214,246). tolower(215,215). tolower(216,248).
tolower(217,249). tolower(218,250). tolower(219,251). tolower(220,252).
tolower(221,253). tolower(222,254). tolower(223,223). tolower(224,224).
tolower(225,225). tolower(226,226). tolower(227,227). tolower(228,228).
tolower(229,229). tolower(230,230). tolower(231,231). tolower(232,232).
tolower(233,233). tolower(234,234). tolower(235,235). tolower(236,236).
tolower(237,237). tolower(238,238). tolower(239,239). tolower(240,240).
tolower(241,241). tolower(242,242). tolower(243,243). tolower(244,244).
tolower(245,245). tolower(246,246). tolower(247,247). tolower(248,248).
tolower(249,249). tolower(250,250). tolower(251,251). tolower(252,252).
tolower(253,253). tolower(254,254). tolower(255,255).

accent_chars([],[]).
accent_chars([In0|Ins],[Out0|Outs]) :-
    deaccent(Out0,In0,_,_),
    isaccented(Out0),
    accent_chars(Ins,Outs).
accent_chars([X|Ins],[X|Outs]) :-
    integer(X),
    accent_chars(Ins,Outs).

deaccent_chars([],[]).
deaccent_chars([In0|Ins],[Out0|Outs]) :-
    deaccent(In0,Out0,_,_),
    deaccent_chars(Ins,Outs).

deaccent_chars([],[],F,F).
deaccent_chars([In0|Ins],[Out0|Outs],F0,F) :-
    deaccent(In0,Out0,F0,F1),
    deaccent_chars(Ins,Outs,F1,F).

deaccent(C0,C,_,1) :-
    deaccent(C0,C).

deaccent(X,X,Flag,Flag) :-
     \+ isaccented(X).

deaccent(192, 65).		% À -> A
deaccent(193, 65).		% Á -> A
deaccent(194, 65).		% Â -> A
deaccent(195, 65).		% Ã -> A
deaccent(196, 65).		% Ä -> A
deaccent(197, 65).		% Å -> A

deaccent(199, 67).		% Ç -> C

deaccent(200, 69).		% È -> E
deaccent(201, 69).		% É -> E
deaccent(202, 69).		% Ê -> E
deaccent(203, 69).		% Ë -> E

deaccent(204, 73).		% Ì -> I
deaccent(205, 73).		% Í -> I
deaccent(206, 73).		% Î -> I
deaccent(207, 73).		% Ï -> I

deaccent(209, 78).		% Ñ -> N

deaccent(210, 79).		% Ò -> O
deaccent(211, 79).		% Ó -> O
deaccent(212, 79).		% Ô -> O
deaccent(213, 79).		% Õ -> O
deaccent(214, 79).		% Ö -> O

deaccent(216, 79).		% Ø -> O

deaccent(217, 85).		% Ù -> U
deaccent(218, 85).		% Ú -> U
deaccent(219, 85).		% Û -> U
deaccent(220, 85).		% Ü -> U

deaccent(221, 89).		% Ý -> Y

deaccent(224, 97).		% à -> a
deaccent(225, 97).		% á -> a
deaccent(226, 97).		% â -> a 
deaccent(227, 97).		% ã -> a
deaccent(228, 97).		% ä -> a
deaccent(229, 97).		% å -> a

deaccent(231, 99).		% ç -> c

deaccent(232, 101).		% è -> e
deaccent(233, 101).		% é -> e
deaccent(234, 101).		% ê -> e
deaccent(235, 101).		% ë -> e

deaccent(236, 105).		% ì -> i
deaccent(237, 105).		% í -> i
deaccent(238, 105).		% î -> i
deaccent(239, 105).		% ï -> i

deaccent(241, 110).		% ñ -> n

deaccent(242, 111).		% ò -> o
deaccent(243, 111).		% ó -> o
deaccent(244, 111).		% ô -> o
deaccent(245, 111).		% õ -> o
deaccent(246, 111).		% ö -> o

deaccent(248, 111).		% ø -> o

deaccent(249, 117).		% ù -> u
deaccent(250, 117).		% ú -> u
deaccent(251, 117).		% û -> u
deaccent(252, 117).		% ü -> u

deaccent(253, 121).		% ý -> y
deaccent(255, 121).		% ÿ -> y

