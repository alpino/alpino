:- module(alpino_postags, [ postag_of_frame/2,
			    postag_of_frame/3,
			    postag_of_frame/5,
			    attribute_of_frame/3,
			    attribute_of_frame/4
			  ]).

:- expects_dialect(sicstus).

:- use_module(hdrug(hdrug_util)).

%% this is used for properly formatting frames in automatic XML output
%% cf treebank.pl
%% it is also used for creating/manipulating triples, cf dt.pl

% attribute_of_frame(+Frame,+Att,?Val)
attribute_of_frame(Frame,Att,Val) :-
    attribute_of_frame(Frame,Att,_,Val).

% attribute_of_frame(+Frame,+Att,?Postag,?Val)

attribute_of_frame(Frame,Att,PosTag,Val) :-
    postag_of_frame(Frame,PosTag,AttList),
    lists:memberchk(Att=Val,AttList).

postag_of_frame(Frame,PosTag,AttList) :-
    postag_of_frame(Frame,_,PosTag,AttList,'N/A').

postag_of_frame(Frame,PosTag) :-
    postag_of_frame(Frame,_,PosTag,_,'N/A').

% postag_of_frame(Frame,PosTag,AttValList).
% this should NOT introduce attributes that already exist in xml!!!
% case 1: for "obsolete" xml files
postag_of_frame(Var,_,_,_,_) :-
    var(Var),
    !,
    fail.
postag_of_frame(read_from_treebank(PosTag0),none,PosTag,[],_) :-
    !,
    functor(PosTag0,PosTag,_).
postag_of_frame(read_from_treebank(PosTag,Frame,Lemma,CgnTag),Frame,PosTag,
                [lemma=Lemma,postag=CgnTag|AttVal],_Root) :-
    !,
    lassy_postag_atts(CgnTag,AttVal,Atts),
    (   p_of_f(Frame,PosTag0,Atts)
    ->  PosTag0=PosTag
    ;   debug_message(1,
	  "warning: no postag_of_frame for ~w~n",[Frame]),
	functor(Frame,PosTag,_),
	Atts=[]
    ).

postag_of_frame(read_from_treebank(PosTag0,Lemma,CgnTag),none,PosTag,
                [lemma=Lemma,postag=CgnTag|AttVal],_) :-
    !,
    functor(PosTag0,PosTag,_),
    lassy_postag_atts(CgnTag,AttVal,[]).

postag_of_frame(read_from_treebank(PosTag0,CgnTag),none,PosTag,
                [postag=CgnTag|AttVal],_) :-
    !,
    functor(PosTag0,PosTag,_),
    lassy_postag_atts(CgnTag,AttVal,[]).

postag_of_frame(Frame,Frame,PosTag,AttValList,Root) :-
    try_cgn_postag(Frame,AttValList,AttValList0,Root),
    (   p_of_f(Frame,PosTag0,AttValList0)
    ->  PosTag0=PosTag
    ;   debug_message(1,
	  "warning: no postag_of_frame for ~w~n",[Frame]),
	functor(Frame,PosTag,_),
	AttValList0=[]
    ).

try_cgn_postag(Frame,L0,L,Root):-
    (   Root == 'N/A'
    ->  L0 = L
    ;   cgn_postag(Frame,CgnTag,Root)
    ->  L0 = [postag=CgnTag|L]
    ;   debug_message(1,
		      "warning: no mapping to postag for ~w~n",
		      [Frame]
		     ),
	L0 = L
    ).

cgn_postag(adjective(het_st(_)),Tag,het) :-
    !,
    Tag='LID(bep,stan,evon)'.
cgn_postag(adjective(het_st(_),_),Tag,het) :-
    !,
    Tag='LID(bep,stan,evon)'.
cgn_postag(pronoun(nwh,thi,sg,de,nom,def),Tag,men) :-
    !,
    Tag='VNW(pers,pron,nomin,red,3p,ev,masc)'.
cgn_postag(pronoun(nwh,thi,sg,de,nom,def),Tag,hij) :-
    !,
    Tag='VNW(pers,pron,nomin,vol,3,ev,masc)'.
cgn_postag(determiner(de,nwh,nmod,pro,nparg),Tag,deze) :-
    !,
    Tag='VNW(aanw,det,stan,prenom,met-e,rest)'.
cgn_postag(determiner(de,nwh,nmod,pro,nparg),Tag,die) :-
    !,
    Tag='VNW(aanw,det,stan,prenom,zonder,rest)'.
cgn_postag(determiner(het,nwh,nmod,pro,nparg),Tag,dit) :-
    !,
    Tag='VNW(aanw,pron,stan,vol,3o,ev)'.
cgn_postag(determiner(het,nwh,nmod,pro,nparg),Tag,dat) :-
    !,
    Tag='VNW(aanw,det,stan,prenom,zonder,evon)'.
cgn_postag(determiner(pron),Tag,Word) :-
    !,
    cgn_pron(Word,Tag).
cgn_postag(determiner(pron,rwh),Tag,wiens) :-
    !,
    Tag='VNW(vb,pron,gen,vol,3m,ev)'.
cgn_postag(determiner(pron,rwh),Tag,wier) :-
    !,
    Tag='VNW(betr,pron,gen,vol,3o,getal)'.
cgn_postag(complementizer(aan_het),Tag,aan) :-
    !,
    Tag='VZ(init)'.
cgn_postag(complementizer(aan_het),Tag,het) :-
    !,
    Tag='LID(bep,stan,evon)'.

cgn_postag(Frame,Tag,_) :-
    cgn_postag(Frame,Tag).

cgn_postag(determiner(de),                  'LID(bep,stan,rest)').
cgn_postag(determiner(een),                 'LID(onbep,stan,agr)').
cgn_postag(determiner(der),                 'LID(bep,gen,rest3)').
cgn_postag(determiner(des),                 'LID(bep,gen,evmo)').

cgn_postag(determiner(onze),                'VNW(bez,det,stan,vol,1,mv,prenom,met-e,rest)').
cgn_postag(determiner(de,nwh,mod,pro,yparg),'ADJ(prenom,basis,zonder)').
cgn_postag(determiner(de,nwh,nmod,pro,nparg),'VNW(aanw,det,stan,prenom,met-e,rest)').
cgn_postag(determiner(een,nwh,mod,pro,yparg),'ADJ(prenom,basis,zonder)').
cgn_postag(determiner(elke,nwh,mod),'VNW(onbep,det,stan,prenom,met-e,evz)').
cgn_postag(determiner(geen,nwh,mod,pro,yparg,nwkpro,geen),'VNW(onbep,det,stan,prenom,zonder,agr)').
cgn_postag(determiner(het),'VNW(bez,det,stan,vol,1,mv,prenom,zonder,evon)').
cgn_postag(determiner(het,nwh,mod,pro,yparg),'ADJ(prenom,basis,zonder)').
cgn_postag(determiner(het,nwh,nmod,pro,nparg),'VNW(aanw,pron,stan,vol,3o,ev)').
cgn_postag(determiner(het,nwh,nmod,pro,nparg,wkpro),'LID(bep,stan,evon)').
cgn_postag(determiner(pl_num),'LID(onbep,stan,agr)').
cgn_postag(determiner(pl_num,nwh,nmod,pro,yparg),'VNW(onbep,det,stan,prenom,met-e,rest)').
cgn_postag(determiner(wat),'ADJ(prenom,basis,zonder)').
cgn_postag(determiner(wat,nwh,mod,pro,nparg,ntopicpro),'VNW(onbep,pron,stan,vol,3o,ev)').
cgn_postag(determiner(welke),'VNW(onbep,det,stan,prenom,met-e,rest)').
cgn_postag(determiner(welke,rwh,nmod,pro,yparg),'VNW(vb,det,stan,prenom,met-e,rest)').
cgn_postag(determiner(alle,nwh,mod,pro,nparg),'VNW(onbep,det,stan,prenom,met-e,agr)').
cgn_postag(determiner(welk,rwh,nmod,pro,yparg),'VNW(vb,det,stan,prenom,zonder,evon)').
cgn_postag(determiner(welk),'VNW(onbep,det,stan,prenom,zonder,evon)').
cgn_postag(pre_det_quant(al),'VNW(onbep,det,stan,vrij,zonder)').
cgn_postag(determiner(zulke,nwh,nmod,pro,yparg),'VNW(aanw,det,stan,prenom,met-e,rest)').

cgn_postag(tmp_determiner,'VNW(onbep,det,stan,prenom,met-e,evz)').

cgn_postag(determiner(pron,wh),'VNW(vb,pron,stan,vol,3o,ev)').
cgn_postag(determiner(pron),'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)').

cgn_postag(gen_determiner(sg),'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)').
cgn_postag(gen_determiner(pl),'VNW(bez,det,stan,vol,3,mv,prenom,zonder,agr)').


cgn_postag(number(hoofd(pl_num)),'TW(hoofd,prenom,stan)').
cgn_postag(number(rang),'TW(rang,prenom,stan)').
cgn_postag(adj_number(enkele),'VNW(onbep,det,stan,prenom,met-e,rest)').
cgn_postag(number(hoofd(both)),'TW(hoofd,prenom,stan)').
cgn_postag(adj_number(pl_num),'VNW(onbep,grad,stan,prenom,met-e,mv,basis)').
cgn_postag(number(hoofd(sg_num)),'TW(hoofd,vrij)').

cgn_postag(pre_num_adv(both),'VNW(onbep,grad,stan,vrij,zonder,comp)').
cgn_postag(pre_num_adv(pl),'VNW(onbep,det,stan,prenom,zonder,agr)').
cgn_postag(pre_num_adv(pl_indef),'VNW(aanw,det,stan,prenom,zonder,agr)').

cgn_postag(punct(_),                        'LET()').

cgn_postag(sbar,                            'BW()').
cgn_postag(pp,                              'BW()').
cgn_postag(pp(_),                           'BW()').
cgn_postag(adverb,                          'BW()').
cgn_postag(modal_adverb,                    'BW()').
cgn_postag(dip_sbar_adverb,                 'BW()').
cgn_postag(modal_adverb(_),                 'BW()').
cgn_postag(predm_adverb,                    'BW()').
cgn_postag(tmp_adverb,                      'BW()').
cgn_postag(loc_adverb,                      'BW()').
cgn_postag(dir_adverb,                      'BW()').
cgn_postag(postnp_adverb,                   'BW()').
cgn_postag(post_wh_adverb,                  'BW()').
cgn_postag(sentence_adverb,                 'BW()').
cgn_postag(waar_adverb(_),                  'BW()').
cgn_postag(er_adverb(_),                    'BW()').
cgn_postag(adv_tag,                         'BW()').
cgn_postag(intensifier,                     'BW()').
cgn_postag(intensifier(e),                  'ADJ(prenom,basis,met-e,stan)').
cgn_postag(me_intensifier,                  'BW()').
cgn_postag(als_me_intensifier,              'BW()').
cgn_postag(vp_om_intensifier,               'BW()').
cgn_postag(hoe_adv,                         'BW()').
cgn_postag(postadj_adverb,                  'BW()').
cgn_postag(postadv_adverb,                  'BW()').
cgn_postag(om_postadj_adverb,               'BW()').
cgn_postag(num_predm_adverb,                'BW()').
cgn_postag(zo_mogelijk_zo,                  'BW()').
cgn_postag(zo_mogelijk_mogelijk(no_e),      'ADJ(vrij,basis,zonder)').
cgn_postag(zo_mogelijk_mogelijk(e),         'ADJ(prenom,basis,met-e,stan)').
cgn_postag(adjective(anders),               'BW()').
cgn_postag(comp_adverb(_),                  'BW()').
cgn_postag(wh_me_adjective,                 'BW()').
cgn_postag(eenmaal_adverb,                  'BW()').
cgn_postag(pre_wh_adverb,                   'BW()').
cgn_postag(vandaar_adverb,                  'BW()').
cgn_postag(postp_adverb,                    'BW()').
cgn_postag(postn_adverb,                    'BW()').
cgn_postag(zo_van_adverb,                   'BW()').

cgn_postag(post_n_n,'N(soort,ev,basis,onz,stan)').

cgn_postag(np_me_adjective(no_e(adv)),'ADJ(vrij,basis,zonder)').
cgn_postag(pred_np_me_adjective(tmpadv),'BW()').
cgn_postag(np_me_adjective(e),'ADJ(prenom,basis,met-e,stan)').
cgn_postag(adjective(prefix),'ADJ(prenom,basis,zonder)').
cgn_postag(me_adjective(no_e(odet_adv)),'VNW(onbep,grad,stan,vrij,zonder,basis)').

cgn_postag(er_loc_adverb,                   'VNW(aanw,adv-pron,obl,vol,3o,getal)').
cgn_postag(er_wh_loc_adverb,                'VNW(vb,adv-pron,obl,vol,3o,getal)').
cgn_postag(er_vp_adverb,                    'VNW(aanw,adv-pron,stan,red,3,getal)').

cgn_postag(noun(de,_,sg),                   'N(soort,ev,basis,zijd,stan)').
cgn_postag(tmp_noun(de,_,sg),               'N(soort,ev,basis,zijd,stan)').
cgn_postag(noun(de,_,sg,_),                 'N(soort,ev,basis,zijd,stan)').
cgn_postag(tmp_noun(de,_,sg,_),             'N(soort,ev,basis,zijd,stan)').
cgn_postag(noun(both,_,sg),                 'N(soort,ev,basis,zijd,stan)').
cgn_postag(tmp_noun(both,_,sg),             'N(soort,ev,basis,zijd,stan)').
cgn_postag(noun(both,_,sg,_),               'N(soort,ev,basis,zijd,stan)').
cgn_postag(tmp_noun(both,_,sg,_),           'N(soort,ev,basis,zijd,stan)').
cgn_postag(noun(het,_,sg),                  'N(soort,ev,basis,onz,stan)').
cgn_postag(tmp_noun(het,_,sg),              'N(soort,ev,basis,onz,stan)').
cgn_postag(noun(het,_,sg,_),                'N(soort,ev,basis,onz,stan)').
cgn_postag(tmp_noun(het,_,sg,_),            'N(soort,ev,basis,onz,stan)').
cgn_postag(noun(_,_,pl),                    'N(soort,mv,basis)').
cgn_postag(tmp_noun(_,_,pl),                'N(soort,mv,basis)').
cgn_postag(noun(_,_,pl,_),                  'N(soort,mv,basis)').
cgn_postag(tmp_noun(_,_,pl,_),              'N(soort,mv,basis)').

cgn_postag(tmp_app_noun,                    'N(soort,ev,basis,onz,stan)').

cgn_postag(noun(both,_,both),               'N(soort,ev,basis,zijd,stan)').
cgn_postag(noun(both,_,both,_),             'N(soort,ev,basis,zijd,stan)').
cgn_postag(mod_noun(het,count,sg),          'N(soort,ev,basis,onz,stan)').
cgn_postag(mod_noun(het,count,pl),          'N(soort,mv,basis)').
cgn_postag(mod_noun(het,count,sg,_),        'N(soort,ev,basis,onz,stan)').
cgn_postag(mod_noun(het,count,pl,_),        'N(soort,mv,basis)').

cgn_postag(tmp_noun(het,count,meas),        'N(soort,ev,basis,onz,stan)').
cgn_postag(tmp_noun(de,count,meas),         'N(soort,ev,basis,zijd,stan)').
cgn_postag(tmp_noun(het,count,meas,_),      'N(soort,ev,basis,onz,stan)').
cgn_postag(tmp_noun(de,count,meas,_),       'N(soort,ev,basis,zijd,stan)').

cgn_postag(tmp_np,                          'TW(hoofd,vrij)').
cgn_postag(np,                              'TW(hoofd,vrij)').
cgn_postag(np(year),                        'TW(hoofd,vrij)').

cgn_postag(meas_mod_noun(de,_,meas),        'N(soort,ev,basis,zijd,stan)').
cgn_postag(meas_mod_noun(de,_,meas,_),      'N(soort,ev,basis,zijd,stan)').
cgn_postag(meas_mod_noun(both,_,meas),      'N(soort,ev,basis,onz,stan)').
cgn_postag(meas_mod_noun(both,_,meas,_),    'N(soort,ev,basis,onz,stan)').
cgn_postag(meas_mod_noun(both,_,bare_meas), 'N(soort,ev,basis,onz,stan)').
cgn_postag(meas_mod_noun(both,_,bare_meas,_),'N(soort,ev,basis,onz,stan)').
cgn_postag(meas_mod_noun(both,_,sg),        'N(soort,ev,basis,onz,stan)').
cgn_postag(meas_mod_noun(both,_,sg,_),      'N(soort,ev,basis,onz,stan)').
cgn_postag(meas_mod_noun(het,mass,sg),      'VNW(onbep,pron,stan,vol,3o,ev)').
cgn_postag(meas_mod_noun(het,mass,sg,_),    'VNW(onbep,pron,stan,vol,3o,ev)').
cgn_postag(meas_mod_noun(de,_,sg),          'N(soort,ev,basis,zijd,stan)').
cgn_postag(meas_mod_noun(de,_,sg,_),        'N(soort,ev,basis,zijd,stan)').
cgn_postag(meas_mod_noun(_,_,pl),           'N(soort,mv,basis)').
cgn_postag(meas_mod_noun(_,_,pl,_),         'N(soort,mv,basis)').
cgn_postag(meas_mod_noun(het,count,sg),     'N(soort,ev,basis,onz,stan)').
cgn_postag(meas_mod_noun(het,count,sg,_),   'N(soort,ev,basis,onz,stan)').

cgn_postag(amount_meas_mod_noun(both,count,bare_meas),
	                                    'N(soort,ev,basis,zijd,stan)').
cgn_postag(amount_meas_mod_noun(both,count,bare_meas,measure),
	                                    'N(soort,ev,basis,zijd,stan)').

cgn_postag(noun(de,count,bare_meas),'N(soort,ev,basis,zijd,stan)').
cgn_postag(noun(de,count,bare_meas,measure),'N(soort,ev,basis,zijd,stan)').

cgn_postag(ge_v_noun(intransitive),'N(soort,ev,basis,onz,stan)').


cgn_postag(iets_noun,                       'VNW(onbep,pron,stan,vol,3o,ev)').
cgn_postag(iets_adverb,                     'VNW(onbep,adv-pron,obl,vol,3o,getal)').
cgn_postag(wh_iets_anders_noun,             'VNW(vb,pron,stan,vol,3p,getal)').

cgn_postag(het_noun,                        'VNW(pers,pron,stan,red,3,ev,onz)').
cgn_postag(cleft_het_noun,                  'VNW(aanw,pron,stan,vol,3o,ev)').

cgn_postag(proper_name(_),                  'SPEC(deeleigen)').
cgn_postag(proper_name(_,_),                'SPEC(deeleigen)').
cgn_postag(name_determiner(pron,_),         'SPEC(deeleigen)').


cgn_postag(preposition(_,_),                'VZ(init)').
cgn_postag(preposition(_,_,_),              'VZ(init)').

cgn_postag(particle(_),                     'VZ(fin)').

cgn_postag(conj(_),                         'VG(neven)').
cgn_postag(left_conj(_),                    'VG(neven)').
cgn_postag(right_conj(als),                 'VG(onder)').

cgn_postag(comp_noun(het,_,sg,_),           'VNW(onbep,pron,stan,vol,3o,ev)').

cgn_postag(comparative(_),                  'VG(onder)').

cgn_postag(complementizer(a),               'VG(onder)').
cgn_postag(complementizer(adv),             'VG(onder)').
cgn_postag(complementizer(al),              'BW()').
cgn_postag(complementizer(als),             'VG(onder)').
cgn_postag(complementizer(alsof),           'VG(onder)').
cgn_postag(complementizer(dat),             'VG(onder)').
cgn_postag(complementizer(inf),             'VZ(init)').
cgn_postag(complementizer(naar),            'VZ(init)').
cgn_postag(complementizer(np),              'VG(onder)').
cgn_postag(complementizer(of),              'VG(onder)').
cgn_postag(complementizer(om),              'VZ(init)').
cgn_postag(complementizer(pp),              'VG(onder)').
cgn_postag(complementizer(root),            'VG(neven)').
cgn_postag(complementizer(sbar),            'VG(onder)').
cgn_postag(complementizer(start),           'VG(onder)').
cgn_postag(complementizer(te),              'VZ(init)').
cgn_postag(complementizer(van),             'VZ(init)').
cgn_postag(complementizer(vp),              'BW()').
cgn_postag(complementizer(zoals),           'VG(onder)').
cgn_postag(complementizer,                  'VG(onder)').

cgn_postag(reflexive(u_thi,both),           'VNW(refl,pron,obl,red,3,getal)').
cgn_postag(reflexive(fir,pl),               'VNW(pr,pron,obl,vol,1,mv)').
cgn_postag(reflexive(fir,sg),               'VNW(pr,pron,obl,red,1,ev)').
cgn_postag(reflexive(je,both),              'VNW(pers,pron,nomin,red,2v,ev)').

%% vrij/prenom/nom depends on context
%% we simple gamble for "vrij"
cgn_postag(adjective(e),                    'ADJ(prenom,basis,met-e,stan)').
cgn_postag(adjective(e,_),                  'ADJ(prenom,basis,met-e,stan)').
cgn_postag(adjective(ende),                 'WW(od,prenom,met-e)').
cgn_postag(adjective(ende,_),               'WW(od,prenom,met-e)').
cgn_postag(adjective(ende(_)),              'WW(od,prenom,met-e)').
cgn_postag(adjective(ende(_),_),            'WW(od,prenom,met-e)').
cgn_postag(adjective(ere),                  'ADJ(prenom,comp,met-e,stan)').
cgn_postag(adjective(ere,_),                'ADJ(prenom,comp,met-e,stan)').
cgn_postag(adjective(ste),                  'ADJ(prenom,sup,met-e,stan)').
cgn_postag(adjective(ste,_),                'ADJ(prenom,sup,met-e,stan)').

cgn_postag(adjective(ge_e),                 'WW(vd,prenom,met-e)').
cgn_postag(adjective(ge_no_e(_)),           'WW(vd,vrij,zonder)').
cgn_postag(adjective(ge_both(_)),           'WW(vd,vrij,zonder)').
cgn_postag(adjective(ge_e,_),               'WW(vd,prenom,met-e)').
cgn_postag(adjective(ge_no_e(_),_),         'WW(vd,vrij,zonder)').
cgn_postag(adjective(ge_both(_),_),         'WW(vd,vrij,zonder)').

cgn_postag(adjective(no_e(_)),              'ADJ(vrij,basis,zonder)').
cgn_postag(adjective(no_e(_),_),            'ADJ(vrij,basis,zonder)').
cgn_postag(adjective(er(_)),                'ADJ(vrij,comp,zonder)').
cgn_postag(adjective(er(_),_),              'ADJ(vrij,comp,zonder)').
cgn_postag(adjective(st(_)),                'ADJ(vrij,sup,zonder)').
cgn_postag(adjective(st(_),_),              'ADJ(vrij,sup,zonder)').
cgn_postag(adjective(end(_)),               'WW(od,vrij,zonder)').
cgn_postag(adjective(end(_),_),             'WW(od,vrij,zonder)').

cgn_postag(adjective(both(_)),              'ADJ(prenom,basis,zonder)').
cgn_postag(adjective(both(_),_),            'ADJ(prenom,basis,zonder)').

cgn_postag(adjective(stof),                 'ADJ(prenom,basis,zonder)').

cgn_postag(post_adjective(no_e),            'ADJ(postnom,basis,met-s)').
cgn_postag(post_adjective(er),              'ADJ(postnom,comp,met-s)').
cgn_postag(post_adjective(no_e,_),          'ADJ(postnom,basis,met-s)').
cgn_postag(post_adjective(er,_),            'ADJ(postnom,comp,met-s)').
cgn_postag(post_adjective_anders(er),       'ADJ(postnom,basis,met-s)').

cgn_postag(adjective(het_st(_)),            'ADJ(vrij,sup,zonder)').
cgn_postag(adjective(het_st(_),_),          'ADJ(vrij,sup,zonder)').

cgn_postag(nominalized_adjective,           'ADJ(nom,basis,met-e,mv-n)').
cgn_postag(nominalized_adjective(_),        'ADJ(nom,basis,met-e,mv-n)').
cgn_postag(nominalized_compar_adjective,    'ADJ(nom,comp,met-e,mv-n)').
cgn_postag(nominalized_compar_adjective_sg, 'ADJ(nom,comp,met-e,zonder-n)').
cgn_postag(nominalized_super_adjective,     'ADJ(nom,sup,met-e,mv-n)').

cgn_postag(adjective(pred(adv)),            'BW()').
cgn_postag(adjective(pred(adv),_),          'BW()').
cgn_postag(adjective(pred(nonadv)),         'ADJ(vrij,basis,zonder)').
cgn_postag(adjective(pred(padv),_),         'ADJ(vrij,basis,zonder)').
cgn_postag(adjective(pred(padv)),           'ADJ(vrij,basis,zonder)').
cgn_postag(adjective(pred(nonadv),_),       'ADJ(vrij,basis,zonder)').
cgn_postag(adjective(pred(both)),           'BW()').
cgn_postag(adjective(pred(both),_),         'BW()').
cgn_postag(adjective(pred(dir_locadv)),     'VZ(init)').
cgn_postag(adjective(pred(dir_locadv),_),   'VZ(init)').


cgn_postag(adjective(postn_no_e(_)),'ADJ(vrij,basis,zonder)').
cgn_postag(adjective(postn_no_e(_,_)),'ADJ(vrij,basis,zonder)').

cgn_postag(np_adjective,'BW()').
cgn_postag(np_adjective(_),'BW()').
cgn_postag(het_np_adjective(_),'BW()').
cgn_postag(subject_vp_pred_np_adjective,'BW()').
cgn_postag(subject_sbar_pred_np_adjective,'ADJ(vrij,basis,zonder)').
cgn_postag(clause_np_adjective(pp(met)),'ADJ(vrij,basis,zonder)').
cgn_postag(so_np_subject_sbar,'ADJ(vrij,basis,zonder)').
cgn_postag(so_np_subject_vp,'ADJ(vrij,basis,zonder)').
cgn_postag(so_pp_subject_sbar(_),'ADJ(vrij,basis,zonder)').
cgn_postag(so_pp_subject_vp(_),'ADJ(vrij,basis,zonder)').

cgn_postag(sbar_pred_adjective(_),'BW()').

cgn_postag(wh_adjective,'BW()').
cgn_postag(wh_adjective(odet_adv),'TW(hoofd,vrij)').

cgn_postag(wh_iets_noun,'TW(hoofd,prenom,stan)').
cgn_postag(e_als_adjective(no_e(odet_adv)),'TW(hoofd,prenom,stan)').
cgn_postag(als_adjective(no_e(odet_adv)),'TW(hoofd,prenom,stan)').

cgn_postag(within_word_conjunct,'SPEC(afgebr)').

cgn_postag(etc,                 'SPEC(afk)').

cgn_postag(wh_tmp_adverb,'VG(onder)').

cgn_postag(adjective(meer),'VNW(onbep,grad,stan,vrij,zonder,comp)').

cgn_postag(comp_determiner(_,_),'ADJ(prenom,basis,zonder)').

cgn_postag(wh_iets_adverb,'VNW(vb,adv-pron,obl,vol,3o,getal)').

cgn_postag(verb(_,psp,_),                   'WW(vd,vrij,zonder)').
cgn_postag(verb(_,inf,_),                   'WW(inf,vrij,zonder)').
cgn_postag(verb(_,inf(no_e),_),             'WW(inf,vrij,zonder)').
cgn_postag(verb(_,inf(e),_),                'WW(inf,prenom,met-e)').
cgn_postag(verb(_,past(sg),_),              'WW(pv,verl,ev)').
cgn_postag(verb(_,past(pl),_),              'WW(pv,verl,mv)').
cgn_postag(verb(_,sg_heeft,_),              'WW(pv,tgw,ev)').
cgn_postag(verb(_,modal_inv,_),             'WW(pv,tgw,ev)').
cgn_postag(verb(_,sg_hebt,_),               'WW(pv,tgw,met-t)').
cgn_postag(verb(_,modal_not_u,_),           'WW(pv,tgw,ev)').
cgn_postag(verb(_,imp(sg1),_),              'WW(pv,tgw,ev)').
cgn_postag(verb(_,sg1,_),                   'WW(pv,tgw,ev)').
cgn_postag(verb(_,sg3,_),                   'WW(pv,tgw,met-t)').
cgn_postag(verb(_,subjunctive,_),           'WW(pv,conj,ev)').
cgn_postag(verb(_,sg,_),                    'WW(pv,tgw,ev)').
cgn_postag(verb(_,imp(sg),_),               'WW(pv,tgw,ev)').
cgn_postag(verb(_,pl,_),                    'WW(pv,tgw,mv)').

cgn_postag(v_noun(_),                       'WW(inf,nom,zonder,zonder-n)').

cgn_postag(tag,                             'TSW()').

cgn_postag(rel_pronoun(de,no_obl),          'VNW(betr,pron,stan,vol,persoon,getal)').
cgn_postag(rel_pronoun(het,no_obl),         'VNW(betr,pron,stan,vol,3,ev)').
cgn_postag(rel_pronoun(het,both),           'VNW(vb,pron,stan,vol,3o,ev)').
cgn_postag(rel_pronoun(both,obl),           'VNW(vb,pron,stan,vol,3p,getal)').
cgn_postag(rel_pronoun(both,no_obl),        'VNW(betr,det,stan,nom,zonder,zonder-n)').

cgn_postag(pronoun(nwh,fir,sg,de,nom,def),  'VNW(pers,pron,nomin,vol,1,ev)').

cgn_postag(pronoun(nwh,fir,both,de,dat_acc,def),'VNW(pr,pron,obl,nadr,1,ev)').
cgn_postag(pronoun(nwh,fir,pl,de,dat_acc,def),'VNW(pr,pron,obl,vol,1,mv)').
cgn_postag(pronoun(nwh,fir,pl,de,nom,def),'VNW(pers,pron,nomin,vol,1,mv)').
cgn_postag(pronoun(nwh,fir,pl,de,nom,def,wkpro),'VNW(pers,pron,nomin,red,1,mv)').
cgn_postag(pronoun(nwh,fir,sg,de,dat_acc,def),'VNW(pr,pron,obl,vol,1,ev)').
cgn_postag(pronoun(nwh,fir,sg,de,dat_acc,def,wkpro),'VNW(pr,pron,obl,red,1,ev)').
cgn_postag(pronoun(nwh,fir,sg,de,nom,def),'VNW(pers,pron,nomin,vol,1,ev)').
cgn_postag(pronoun(nwh,je,both,de,dat_acc,def),'VNW(pr,pron,obl,nadr,2v,getal)').
cgn_postag(pronoun(nwh,je,pl,de,both,def),'VNW(pers,pron,stan,nadr,2v,mv)').
cgn_postag(pronoun(nwh,je,sg,de,both,def,wkpro),'VNW(pers,pron,nomin,red,2v,ev)').
cgn_postag(pronoun(nwh,je,sg,de,dat_acc,def),'VNW(pers,pron,obl,vol,2v,ev)').
cgn_postag(pronoun(nwh,je,sg,de,nom,def),'VNW(pers,pron,nomin,vol,2v,ev)').
cgn_postag(pronoun(nwh,thi,both,de,both,def,wkpro),'VNW(pers,pron,stan,red,3,mv)').
cgn_postag(pronoun(nwh,thi,both,de,dat_acc,def),'VNW(refl,pron,obl,nadr,3,getal)').
cgn_postag(pronoun(nwh,thi,both,de,dat_acc,def,wkpro),'VNW(refl,pron,obl,red,3,getal)').
cgn_postag(pronoun(nwh,thi,both,de,nom,def),'VNW(pers,pron,nomin,vol,3p,mv)').
cgn_postag(pronoun(nwh,thi,pl,de,both,def,strpro),'VNW(aanw,det,stan,nom,met-e,mv-n)').
cgn_postag(pronoun(nwh,thi,pl,de,both,indef),'VNW(onbep,grad,stan,nom,met-e,mv-n,basis)').
cgn_postag(pronoun(nwh,thi,pl,de,dat_acc,def),'VNW(pers,pron,obl,vol,3p,mv)').
cgn_postag(pronoun(nwh,thi,sg,both,both,indef,strpro),'TW(hoofd,vrij)').
cgn_postag(pronoun(nwh,thi,sg,de,both,def),'VNW(onbep,det,stan,vrij,zonder)').
cgn_postag(pronoun(nwh,thi,sg,de,both,def,strpro),'VNW(onbep,pron,stan,vol,3p,ev)').
cgn_postag(pronoun(nwh,thi,sg,de,both,indef,strpro),'VNW(onbep,pron,stan,vol,3p,ev)').
cgn_postag(pronoun(nwh,thi,sg,de,dat_acc,def),'VNW(pers,pron,obl,vol,3,ev,masc)').
cgn_postag(pronoun(nwh,thi,sg,het,both,indef,strpro),'VNW(onbep,pron,stan,vol,3o,ev)').
cgn_postag(pronoun(nwh,u,sg,de,both,def),'VNW(pers,pron,nomin,vol,2b,getal)').
cgn_postag(pronoun(ywh,thi,both,de,both,indef),'VNW(vb,pron,stan,vol,3p,getal)').
cgn_postag(pronoun(ywh,thi,sg,het,both,def),'VNW(betr,det,stan,nom,zonder,zonder-n)').
cgn_postag(pronoun(ywh,thi,sg,het,both,indef,nparg),'VNW(vb,pron,stan,vol,3o,ev)').


cgn_pron(zijn,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)').
cgn_pron(haar,'VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)').
cgn_pron(hun,'VNW(bez,det,stan,vol,3,mv,prenom,zonder,agr)').
cgn_pron(mijn,'VNW(bez,det,stan,vol,1,ev,prenom,zonder,agr)').
cgn_pron(diens,'VNW(aanw,pron,gen,vol,3m,ev)').
cgn_pron(je,'VNW(bez,det,stan,red,2v,ev,prenom,zonder,agr)').
cgn_pron('z\'n','VNW(bez,det,stan,red,3,ev,prenom,zonder,agr)').
cgn_pron(u,'VNW(bez,det,stan,vol,2,getal,prenom,zonder,agr)').
cgn_pron(uw,'VNW(bez,det,stan,vol,2,getal,prenom,zonder,agr)').
cgn_pron('uw aller','VNW(bez,det,stan,vol,2,getal,prenom,zonder,agr)').
cgn_pron(jouw,'VNW(bez,det,stan,vol,2v,ev,prenom,zonder,agr)').
cgn_pron(jullie,'VNW(bez,det,stan,nadr,2v,mv,prenom,zonder,agr)').

%% robust_skip:
%% never used for end results, but only during frame_features in
%% penalties.pl
p_of_f(robust_skip,robust_skip,[]).

p_of_f(pronoun(Wh,Per,Num,Gen,Case,Def),
		pron,
		[wh=Wh,per=Per,num=Num,gen=Gen,case=Case,def=Def]).
p_of_f(pronoun(Wh,Per,Num,Gen,Case,Def,Special),
		pron,
		[wh=Wh,per=Per,num=Num,gen=Gen,case=Case,def=Def,special=Special]).
p_of_f(reflexive(Per,Num),pron,[per=Per,num=Num,refl=refl]).
p_of_f(rel_pronoun(Gen,Case),pron,[gen=Gen,case=Case,wh=rel]).
p_of_f(rel_pred,pron,[wh=rel,special=pred]).
p_of_f(zoals_rel_adverb,pron,[wh=rel,special=adv]).

p_of_f(preposition(_,_),prep,[]).
p_of_f(preposition(_,_,Sc),prep,[sc=Sc]).
p_of_f(mod_postposition,prep,[special=mod_post]).
p_of_f(pp,pp,[]).
p_of_f(pp(_),pp,[]).
p_of_f(waar_adverb(_),pp,[special=waar]).
p_of_f(er_adverb(_),pp,[special=er]).

p_of_f(adverb,adv,[]).
p_of_f(er_adverb,adv,[special=er]).
p_of_f(post_wh_adverb,adv,[special=post_wh]).
p_of_f(pre_wh_adverb,adv,[special=pre_wh]).
p_of_f(comp_adverb(Sub),adv,[special=comp,comparative=Sub]).
p_of_f(intensifier,adv,[special=intensifier]).
p_of_f(intensifier(e),adv,[special=intensifier]).
p_of_f(vp_om_intensifier,adv,[special=intensifier,sc=vp_om]).
p_of_f(me_intensifier,adv,[special=me_intensifier]).
p_of_f(als_me_intensifier,adv,[special=me_intensifier,sc=als]).
p_of_f(vp_om_me_intensifier,adv,[special=me_intensifier,sc=vp_om]).
p_of_f(wh_adverb,adv,[wh=ywh]).
p_of_f(wh_tmp_adverb,adv,[wh=ywh,special=tmp]).
p_of_f(wh_loc_adverb,adv,[wh=ywh,special=loc]).
p_of_f(er_wh_loc_adverb,adv,[wh=ywh,special=er_loc]).
p_of_f(rwh_loc_adverb,adv,[wh=rwh,special=loc]).
p_of_f(predm_adverb,adv,[special=predm]).
p_of_f(eenmaal_adverb,adv,[special=eenmaal]).
p_of_f(postadj_adverb,adv,[special=postadj]).
p_of_f(postadv_adverb,adv,[special=postadv]).
p_of_f(post_loc_adv_adv,adv,[special=postlocadv]).
p_of_f(postnp_adverb,adv,[special=postnp]).
p_of_f(postn_adverb,adv,[special=postn]).
p_of_f(postp_adverb,adv,[special=postp]).
p_of_f(sentence_adverb,adv,[special=sentence]).
p_of_f(er_vp_adverb,adv,[special=er]).
p_of_f(loc_adverb,adv,[special=loc]).
p_of_f(er_loc_adverb,adv,[special=er_loc]).
p_of_f(wk_tmp_adverb,adv,[special=tmp,wk=yes]).
p_of_f(tmp_adverb,adv,[special=tmp]).
p_of_f(dir_adverb,adv,[special=dir]).
p_of_f(modal_adverb,adv,[sc=modal]).
p_of_f(modal_adverb(Sc),adv,[sc=Sc]).
p_of_f(dip_sbar_adverb,adv,[sc=dip_sbar]).
p_of_f(hoe_adv,adv,[special=hoe]).
p_of_f(om_postadj_adverb,adv,[special=postadj,sc=om]).
p_of_f(vp_adverb,adv,[sc=vp]).
p_of_f(vp_om_adverb,adv,[sc=vp_om]).
p_of_f(zo_mogelijk_zo,adv,[sc=zo_mogelijk]).
p_of_f(iets_adverb,adv,[sc=iets]).
p_of_f(wh_iets_adverb,adv,[sc=iets,wh=ywh]).
p_of_f(vandaar_adverb,adv,[sc=vandaar]).
p_of_f(zo_van_adverb,adv,[sc=zo_van]).

p_of_f(post_p(_),prep,[special=post]).

p_of_f(v_noun(Sc),verb,[sc=Sc,special=v_noun]).
p_of_f(ge_v_noun(Sc),noun,[sc=Sc,special=ge_v_noun]).
p_of_f(verb(_,Infl0,Sc0),verb,[sc=Sc,infl=Infl|Rest]) :-
    verb_infl(Infl0,Infl,Rest),
    sc(Sc0,Sc).
p_of_f(denk_ik,verb,[special=denk_ik]).
p_of_f(denk_ik_dip,verb,[special=denk_ik_dip]).

p_of_f(pred_np_me_adjective(_),adj,[sc=pred_np_me]).
p_of_f(subject_sbar_pred_np_me_adjective,adj,[sc=subject_sbar_pred_np_me]).
p_of_f(subject_sbar_pred_np_adjective,adj,[sc=subject_sbar_pred_np]).
p_of_f(subject_vp_pred_np_adjective,adj,[sc=subject_vp_pred_np]).
p_of_f(np_me_adjective(Infl0),adj,[sc=np_me|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(np_me_adjective(_,Infl0),adj,[sc=np_me|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(clause_np_adjective,adj,[special=clause_np]).
p_of_f(clause_np_adjective(Sc),adj,[special=clause_np,sc=Sc]).
p_of_f(np_adjective,adj,[special=np]).
p_of_f(np_adjective(Sc),adj,[special=np,sc=Sc]).
p_of_f(het_np_adjective,adj,[special=het_np]).
p_of_f(het_np_adjective(Sc),adj,[special=het_np,sc=Sc]).
p_of_f(adjective(Infl0),adj,Atts) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(adjective(Infl0,Sc),adj,[sc=Sc|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(adjective(Infl0,_,Sc),adj,[sc=Sc|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(post_adjective(Infl0,Sc),adj,[special=iets,sc=Sc|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(post_adjective(Infl0),adj,[special=iets|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(post_adjective_anders(er),adj,[special=anders,infl=er,aform=compar]).
p_of_f(wh_adjective,adj,[wh=ywh]).
p_of_f(wh_adjective(_),adj,[wh=ywh]).
p_of_f(wh_me_adjective,adj,[wh=ywh]).
p_of_f(vp_om_me_adjective(Infl0),adj,[sc=vp_om_me|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(me_adjective(Infl0),adj,[sc=me|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(nominalized_adjective,adj,[num=pl,special=a_noun,aform=base]).
p_of_f(nominalized_adjective(Sc),adj,[num=pl,special=a_noun,sc=Sc,aform=base]).
p_of_f(nominalized_compar_adjective,adj,[num=pl,special=a_noun,aform=compar]).
p_of_f(nominalized_compar_adjective_sg,adj,[num=sg,special=a_noun,aform=compar]).
p_of_f(nominalized_super_adjective,adj,[num=pl,special=a_noun,aform=super]).
p_of_f(nominalized_adjective_sg,adj,[num=sg,special=a_noun,aform=base]).
p_of_f(sbar_adjective(Infl0),adj,[sc=sbar|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(sbar_pred_adjective(_),adj,[sc=sbar,infl=pred]).
p_of_f(vp_pred_adjective(_),adj,[sc=vp,infl=pred]).
p_of_f(zo_mogelijk_mogelijk(Infl0),adj,[sc=zo_mogelijk|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(als_adjective(Infl0),adj,[sc=als|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).
p_of_f(e_als_adjective(Infl0),adj,[sc=e_als|Atts]) :-
    functor(Infl0,Infl,_),
    adj_form(Infl,Atts).

p_of_f(noun(Gen,_,Num),noun,[gen=Gen,num=Num]).
p_of_f(noun(Gen,_,Num,Sc),noun,[gen=Gen,num=Num,sc=Sc]).

p_of_f(tmp_noun(A,B,C),Pos,[special=tmp|Atts]) :-
    p_of_f(noun(A,B,C),Pos,Atts).
p_of_f(tmp_noun(A,B,C,D),Pos,[special=tmp|Atts]) :-
    p_of_f(noun(A,B,C,D),Pos,Atts).

p_of_f(meas_mod_noun(A,B,C),Pos,[special=meas_mod|Atts]) :-
    p_of_f(noun(A,B,C),Pos,Atts).
p_of_f(meas_mod_noun(A,B,C,D),Pos,[special=meas_mod|Atts]) :-
    p_of_f(noun(A,B,C,D),Pos,Atts).

p_of_f(amount_meas_mod_noun(A,B,C),Pos,[special=amount_meas_mod|Atts]) :-
    p_of_f(noun(A,B,C),Pos,Atts).
p_of_f(amount_meas_mod_noun(A,B,C,D),Pos,[special=amount_meas_mod|Atts]) :-
    p_of_f(noun(A,B,C,D),Pos,Atts).

p_of_f(mod_noun(A,B,C),Pos,[special=mod|Atts]) :-
    p_of_f(noun(A,B,C),Pos,Atts).
p_of_f(mod_noun(A,B,C,D),Pos,[special=mod|Atts]) :-
    p_of_f(noun(A,B,C,D),Pos,Atts).

p_of_f(comp_noun(Gen,_,Num,Dan),noun,[gen=Gen,num=Num,special=comp,
				      comparative=Dan]).
p_of_f(cleft_het_noun,noun,[special=cleft_het]).
p_of_f(wh_cleft_het_noun,noun,[special=cleft_het,wh=ywh]).
p_of_f(het_noun,noun,[special=het]).
p_of_f(iets_noun,noun,[sc=iets]).
p_of_f(wh_iets_noun,noun,[wh=ywh,sc=iets]).
p_of_f(iets_anders_noun,noun,[sc=iets_anders_noun]).
p_of_f(wh_iets_anders_noun,noun,[sc=iets_anders_noun,wh=ywh]).
p_of_f(er_noun,noun,[special=er]).
p_of_f(wh_er_noun,noun,[special=er,wh=ywh]).
p_of_f(tmp_app_noun,adv,[special=tmp_app]).
p_of_f(np,noun,[]).
p_of_f(np_TEMP,noun,[]).
p_of_f(np(Y),noun,[neclass=Y]).
p_of_f(tmp_np,noun,[special=tmp]).
p_of_f(functn,noun,[special=functn]).
p_of_f(post_n_n,noun,[special=post_n_n]).

p_of_f(determiner(Infl),det,[infl=Infl]).
p_of_f(name_determiner(Infl),det,[infl=Infl,special=name]).
p_of_f(name_determiner(Infl,Class),det,[infl=Infl,special=name,neclass=Class]).
p_of_f(determiner(Infl,Wh),det,[infl=Infl,wh=Wh]).
p_of_f(determiner(Infl,Wh,_),det,[infl=Infl,wh=Wh]).
p_of_f(determiner(Infl,Wh,_,_),det,[infl=Infl,wh=Wh]).
p_of_f(determiner(Infl,Wh,_,_,_),det,[infl=Infl,wh=Wh]).
p_of_f(determiner(Infl,Wh,_,_,_,_),det,[infl=Infl,wh=Wh]).
p_of_f(determiner(Infl,Wh,_,_,_,_,_),det,[infl=Infl,wh=Wh]).
p_of_f(determiner(Infl,Wh,_,_,_,_,_,_),det,[infl=Infl,wh=Wh]).
p_of_f(comp_determiner(Infl,Sc),det,[infl=Infl,special=comp,comparative=Sc]).
p_of_f(comp_determiner(Infl),det,[infl=Infl,special=comp]).
p_of_f(pre_num_adv(Infl),det,[infl=Infl,special=pre_num_adv]).
p_of_f(pre_det_quant(Infl),det,[infl=Infl,special=pre_det_quant]).

p_of_f(adj_number(Infl),det,[infl=Infl,special=adj_number]).

p_of_f(gen_determiner(Infl),det,[infl=Infl,special=gen]).
p_of_f(tmp_determiner,det,[special=tmp]).
p_of_f(num_predm_adverb,det,[special=num_predm]).

p_of_f(number(hoofd(Infl)),num,[infl=Infl,special=hoofd]).
p_of_f(number(rang),num,[special=rang]).
p_of_f(num_na,num,[special=na]). % de drie na mooiste, only with_dt?
p_of_f(wh_number(rang),num,[wh=wh,special=rang]).
p_of_f(score_cat,num,[special=score]).
p_of_f(pre_np_adverb,adv,[special=pre_np]).
p_of_f(enumeration,num,[special=enumeration]).

p_of_f(sbar,comp,[]).
p_of_f(complementizer,comp,[]).
p_of_f(complementizer(Sc),comp,[sc=Sc]).
p_of_f(conj(_),vg,[]).
p_of_f(left_conj(_),vg,[special=left]).
p_of_f(right_conj(_),vg,[special=right]).
p_of_f(comparative(_),comparative,[]).
p_of_f(etc,vg,[]).
p_of_f(complex_etc,vg,[]).

p_of_f(fixed_part(_),fixed,[]).

p_of_f(particle(_),part,[]).

p_of_f(proper_name(Num),name,[num=Num]).
p_of_f(proper_name(Num,Class),name,[num=Num,neclass=Class]).

p_of_f(tag,tag,[]).
p_of_f(adv_tag,tag,[]).

p_of_f(within_word_conjunct,prefix,[]).

p_of_f(punct(Type),punct,[special=Type]).
p_of_f(longpunct,punct,[special=long]).

p_of_f(max,max,[]).

p_of_f(read_from_treebank(Pos),Pos,[]).

p_of_f('UNKNOWN','UNKNOWN',[]).

p_of_f('--','--',[]).

%% old
p_of_f(postnp_det_mod,A,B) :-
    p_of_f(adjective(meer),A,B).

p_of_f(meas_tmp_noun(A,B,_),X,Y) :-
    p_of_f(tmp_noun(A,B,meas),X,Y).

p_of_f(subject_vp_pred_np_me_adjective,adj,[sc=subject_vp_pred_np]).

%% per request of Gosse, 2008-07-25
sc(ninv(_,A),B) :-
    !,
    A=B.
sc(A,A).

verb_infl(inf,inf,[]).
verb_infl(inf(no_e),inf(no_e),[]).
verb_infl(inf(e),inf(e),[]).
verb_infl(psp,psp,[]).
verb_infl(past(sg),sg,[tense=past]).
verb_infl(past(pl),pl,[tense=past]).
verb_infl(sg,sg,[tense=present]).
verb_infl(pl,pl,[tense=present]).
verb_infl(sg1,sg1,[tense=present]).
verb_infl(sg3,sg3,[tense=present]).
verb_infl(sg_hebt,sg_hebt,[tense=present]).
verb_infl(sg_heeft,sg_heeft,[tense=present]).
verb_infl(modal_not_u,modal_not_u,[tense=present]).
verb_infl(modal_inv,modal_inv,[tense=present]).
verb_infl(subjunctive,subjunctive,[]).
verb_infl(imp(sg),imp(sg),[tense=present]).
verb_infl(imp(sg1),imp(sg1),[tense=present]).
verb_infl(imp(modal_u),imp(modal_u),[tense=present]).
verb_infl(inf_ipp,inf_ipp,[]).

adj_form(stof,        [infl=stof,   aform=base,    vform=adj]).
adj_form(prefix,      [infl=prefix, aform=base,    vform=adj]).
adj_form(e,           [infl=e,      aform=base,    vform=adj]).
adj_form(ge_e,        [infl=e,      aform=base,    vform=psp]).
adj_form(ere,         [infl=e,      aform=compar,  vform=adj]).
adj_form(ste,         [infl=e,      aform=super,   vform=adj]).
adj_form(pred,        [infl=pred,   aform=base,    vform=adj]).
adj_form(both,        [infl=both,   aform=base,    vform=adj]).
adj_form(ge_both,     [infl=both,   aform=base,    vform=psp]).
adj_form(postn_both,  [infl=both,   aform=base,    vform=adj]).
adj_form(postn_pred,  [infl=pred,   aform=base,    vform=adj]).
adj_form(no_e,        [infl=no_e,   aform=base,    vform=adj]).
adj_form(ge_no_e,     [infl=no_e,   aform=base,    vform=psp]).
adj_form(postn_no_e,  [infl=no_e,   aform=base,    vform=adj]).
adj_form(er,          [infl=no_e,   aform=compar,  vform=adj]).
adj_form(het_st,      [infl=pred,   aform=super,   vform=adj]).
adj_form(het_ste,     [infl=pred,   aform=super,   vform=adj]).
adj_form(st,          [infl=no_e,   aform=super,   vform=adj]).
adj_form(ende,        [infl=e,      aform=base,    vform=gerund]).
adj_form(end,         [infl=no_e,   aform=base,    vform=gerund]).

adj_form(anders,      [infl=anders, aform=compar,  vform=adj]).
adj_form(meer,        [infl=meer,   aform=compar,  vform=adj]).



%%%%
% lassy_postag_atts(CgnTag,AttVal,AttValList).

lassy_postag_atts(Tag,Atts0,Atts) :-
    (   lassy_postag_atts_(Tag,Atts0,Atts)
    ->  true
    ;   format(user_error,"postag not recognized: ~w~n",[Tag]),
	Atts0=Atts
    ).

lassy_postag_atts_('TSW(dial)',[pt='tsw',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('N(soort,dial)',[pt='n',ntype='soort',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,dial)',[pt='n',ntype='eigen',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('ADJ(dial)',[pt='adj',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('WW(dial)',[pt='ww',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('TW(hoofd,dial)',[pt='tw',numtype='hoofd',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('TW(rang,dial)',[pt='tw',numtype='rang',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,dial)',[pt='vnw',vwtype='pers',pdtype='pron',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(refl,pron,dial)',[pt='vnw',vwtype='refl',pdtype='pron',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(recip,pron,dial)',[pt='vnw',vwtype='recip',pdtype='pron',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dial)',[pt='vnw',vwtype='bez',pdtype='det',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(vrag,pron,dial)',[pt='vnw',vwtype='vrag',pdtype='pron',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(vrag,det,dial)',[pt='vnw',vwtype='vrag',pdtype='det',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(betr,pron,dial)',[pt='vnw',vwtype='betr',pdtype='pron',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(betr,det,dial)',[pt='vnw',vwtype='betr',pdtype='det',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(excl,pron,dial)',[pt='vnw',vwtype='excl',pdtype='pron',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(excl,det,dial)',[pt='vnw',vwtype='excl',pdtype='det',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,pron,dial)',[pt='vnw',vwtype='aanw',pdtype='pron',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,dial)',[pt='vnw',vwtype='aanw',pdtype='det',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,pron,dial)',[pt='vnw',vwtype='onbep',pdtype='pron',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,dial)',[pt='vnw',vwtype='onbep',pdtype='det',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('LID(bep,dial)',[pt='lid',lwtype='bep',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('LID(onbep,dial)',[pt='lid',lwtype='onbep',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VZ(init,dial)',[pt='vz',vztype='init',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VZ(fin,dial)',[pt='vz',vztype='fin',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VG(neven,dial)',[pt='vg',conjtype='neven',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('VG(onder,dial)',[pt='vg',conjtype='onder',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('BW(dial)',[pt='bw',dial='dial'|AttVal],AttVal).
lassy_postag_atts_('TSW()',[pt='tsw'|AttVal],AttVal).
lassy_postag_atts_('SPEC(afgebr)',[pt='spec',spectype='afgebr'|AttVal],AttVal).
lassy_postag_atts_('SPEC(onverst)',[pt='spec',spectype='onverst'|AttVal],AttVal).
lassy_postag_atts_('SPEC(vreemd)',[pt='spec',spectype='vreemd'|AttVal],AttVal).
lassy_postag_atts_('SPEC(deeleigen)',[pt='spec',spectype='deeleigen'|AttVal],AttVal).
lassy_postag_atts_('SPEC(meta)',[pt='spec',spectype='meta'|AttVal],AttVal).
lassy_postag_atts_('LET()',[pt='let'|AttVal],AttVal).
lassy_postag_atts_('SPEC(comment)',[pt='spec',spectype='comment'|AttVal],AttVal).
lassy_postag_atts_('SPEC(achter)',[pt='spec',spectype='achter'|AttVal],AttVal).
lassy_postag_atts_('SPEC(afk)',[pt='spec',spectype='afk'|AttVal],AttVal).
lassy_postag_atts_('SPEC(symb)',[pt='spec',spectype='symb'|AttVal],AttVal).
lassy_postag_atts_('N(soort,ev,basis,zijd,stan)',[pt='n',ntype='soort',getal='ev',graad='basis',genus='zijd',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('N(soort,ev,basis,onz,stan)',[pt='n',ntype='soort',getal='ev',graad='basis',genus='onz',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('N(soort,ev,dim,onz,stan)',[pt='n',ntype='soort',getal='ev',graad='dim',genus='onz',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('N(soort,ev,basis,gen)',[pt='n',ntype='soort',getal='ev',graad='basis',naamval='gen'|AttVal],AttVal).
lassy_postag_atts_('N(soort,ev,dim,gen)',[pt='n',ntype='soort',getal='ev',graad='dim',naamval='gen'|AttVal],AttVal).
lassy_postag_atts_('N(soort,ev,basis,dat)',[pt='n',ntype='soort',getal='ev',graad='basis',naamval='dat'|AttVal],AttVal).
lassy_postag_atts_('N(soort,mv,basis)',[pt='n',ntype='soort',getal='mv',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('N(soort,mv,dim)',[pt='n',ntype='soort',getal='mv',graad='dim'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,ev,basis,zijd,stan)',[pt='n',ntype='eigen',getal='ev',graad='basis',genus='zijd',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,ev,basis,onz,stan)',[pt='n',ntype='eigen',getal='ev',graad='basis',genus='onz',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,ev,dim,onz,stan)',[pt='n',ntype='eigen',getal='ev',graad='dim',genus='onz',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,ev,basis,gen)',[pt='n',ntype='eigen',getal='ev',graad='basis',naamval='gen'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,ev,dim,gen)',[pt='n',ntype='eigen',getal='ev',graad='dim',naamval='gen'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,ev,basis,dat)',[pt='n',ntype='eigen',getal='ev',graad='basis',naamval='dat'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,mv,basis)',[pt='n',ntype='eigen',getal='mv',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,mv,dim)',[pt='n',ntype='eigen',getal='mv',graad='dim'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,basis,zonder)',[pt='adj',positie='prenom',graad='basis',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,basis,met-e,stan)',[pt='adj',positie='prenom',graad='basis',buiging='met-e',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,basis,met-e,bijz)',[pt='adj',positie='prenom',graad='basis',buiging='met-e',naamval='bijz'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,comp,zonder)',[pt='adj',positie='prenom',graad='comp',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,comp,met-e,stan)',[pt='adj',positie='prenom',graad='comp',buiging='met-e',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,comp,met-e,bijz)',[pt='adj',positie='prenom',graad='comp',buiging='met-e',naamval='bijz'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,sup,zonder)',[pt='adj',positie='prenom',graad='sup',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,sup,met-e,stan)',[pt='adj',positie='prenom',graad='sup',buiging='met-e',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('ADJ(prenom,sup,met-e,bijz)',[pt='adj',positie='prenom',graad='sup',buiging='met-e',naamval='bijz'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,basis,zonder,zonder-n)',[pt='adj',positie='nom',graad='basis',buiging='zonder','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,basis,zonder,mv-n)',[pt='adj',positie='nom',graad='basis',buiging='zonder','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,basis,met-e,zonder-n,stan)',[pt='adj',positie='nom',graad='basis',buiging='met-e','getal-n'='zonder-n',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,basis,met-e,zonder-n,bijz)',[pt='adj',positie='nom',graad='basis',buiging='met-e','getal-n'='zonder-n',naamval='bijz'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,basis,met-e,mv-n)',[pt='adj',positie='nom',graad='basis',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,comp,zonder,zonder-n)',[pt='adj',positie='nom',graad='comp',buiging='zonder','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,comp,met-e,zonder-n,stan)',[pt='adj',positie='nom',graad='comp',buiging='met-e','getal-n'='zonder-n',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,comp,met-e,zonder-n,bijz)',[pt='adj',positie='nom',graad='comp',buiging='met-e','getal-n'='zonder-n',naamval='bijz'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,comp,met-e,mv-n)',[pt='adj',positie='nom',graad='comp',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,sup,zonder,zonder-n)',[pt='adj',positie='nom',graad='sup',buiging='zonder','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,sup,met-e,zonder-n,stan)',[pt='adj',positie='nom',graad='sup',buiging='met-e','getal-n'='zonder-n',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,sup,met-e,zonder-n,bijz)',[pt='adj',positie='nom',graad='sup',buiging='met-e','getal-n'='zonder-n',naamval='bijz'|AttVal],AttVal).
lassy_postag_atts_('ADJ(nom,sup,met-e,mv-n)',[pt='adj',positie='nom',graad='sup',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('ADJ(postnom,basis,zonder)',[pt='adj',positie='postnom',graad='basis',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('ADJ(postnom,basis,met-s)',[pt='adj',positie='postnom',graad='basis',buiging='met-s'|AttVal],AttVal).
lassy_postag_atts_('ADJ(postnom,comp,zonder)',[pt='adj',positie='postnom',graad='comp',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('ADJ(postnom,comp,met-s)',[pt='adj',positie='postnom',graad='comp',buiging='met-s'|AttVal],AttVal).
lassy_postag_atts_('ADJ(vrij,basis,zonder)',[pt='adj',positie='vrij',graad='basis',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('ADJ(vrij,comp,zonder)',[pt='adj',positie='vrij',graad='comp',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('ADJ(vrij,sup,zonder)',[pt='adj',positie='vrij',graad='sup',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('ADJ(vrij,dim,zonder)',[pt='adj',positie='vrij',graad='dim',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('WW(pv,tgw,ev)',[pt='ww',wvorm='pv',pvtijd='tgw',pvagr='ev'|AttVal],AttVal).
lassy_postag_atts_('WW(pv,tgw,mv)',[pt='ww',wvorm='pv',pvtijd='tgw',pvagr='mv'|AttVal],AttVal).
lassy_postag_atts_('WW(pv,tgw,met-t)',[pt='ww',wvorm='pv',pvtijd='tgw',pvagr='met-t'|AttVal],AttVal).
lassy_postag_atts_('WW(pv,verl,ev)',[pt='ww',wvorm='pv',pvtijd='verl',pvagr='ev'|AttVal],AttVal).
lassy_postag_atts_('WW(pv,verl,mv)',[pt='ww',wvorm='pv',pvtijd='verl',pvagr='mv'|AttVal],AttVal).
lassy_postag_atts_('WW(pv,verl,met-t)',[pt='ww',wvorm='pv',pvtijd='verl',pvagr='met-t'|AttVal],AttVal).
lassy_postag_atts_('WW(pv,conj,ev)',[pt='ww',wvorm='pv',pvtijd='conj',pvagr='ev'|AttVal],AttVal).
lassy_postag_atts_('WW(inf,prenom,zonder)',[pt='ww',wvorm='inf',positie='prenom',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('WW(inf,prenom,met-e)',[pt='ww',wvorm='inf',positie='prenom',buiging='met-e'|AttVal],AttVal).
lassy_postag_atts_('WW(inf,nom,zonder,zonder-n)',[pt='ww',wvorm='inf',positie='nom',buiging='zonder','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('WW(inf,vrij,zonder)',[pt='ww',wvorm='inf',positie='vrij',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('WW(vd,prenom,zonder)',[pt='ww',wvorm='vd',positie='prenom',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('WW(vd,prenom,met-e)',[pt='ww',wvorm='vd',positie='prenom',buiging='met-e'|AttVal],AttVal).
lassy_postag_atts_('WW(vd,nom,met-e,zonder-n)',[pt='ww',wvorm='vd',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('WW(vd,nom,met-e,mv-n)',[pt='ww',wvorm='vd',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('WW(vd,vrij,zonder)',[pt='ww',wvorm='vd',positie='vrij',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('WW(od,prenom,zonder)',[pt='ww',wvorm='od',positie='prenom',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('WW(od,prenom,met-e)',[pt='ww',wvorm='od',positie='prenom',buiging='met-e'|AttVal],AttVal).
lassy_postag_atts_('WW(od,nom,met-e,zonder-n)',[pt='ww',wvorm='od',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('WW(od,nom,met-e,mv-n)',[pt='ww',wvorm='od',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('WW(od,vrij,zonder)',[pt='ww',wvorm='od',positie='vrij',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('TW(hoofd,prenom,stan)',[pt='tw',numtype='hoofd',positie='prenom',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('TW(hoofd,prenom,bijz)',[pt='tw',numtype='hoofd',positie='prenom',naamval='bijz'|AttVal],AttVal).
lassy_postag_atts_('TW(hoofd,nom,zonder-n,basis)',[pt='tw',numtype='hoofd',positie='nom','getal-n'='zonder-n',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('TW(hoofd,nom,mv-n,basis)',[pt='tw',numtype='hoofd',positie='nom','getal-n'='mv-n',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('TW(hoofd,nom,zonder-n,dim)',[pt='tw',numtype='hoofd',positie='nom','getal-n'='zonder-n',graad='dim'|AttVal],AttVal).
lassy_postag_atts_('TW(hoofd,nom,mv-n,dim)',[pt='tw',numtype='hoofd',positie='nom','getal-n'='mv-n',graad='dim'|AttVal],AttVal).
lassy_postag_atts_('TW(hoofd,vrij)',[pt='tw',numtype='hoofd',positie='vrij'|AttVal],AttVal).
lassy_postag_atts_('TW(rang,prenom,stan)',[pt='tw',numtype='rang',positie='prenom',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('TW(rang,prenom,bijz)',[pt='tw',numtype='rang',positie='prenom',naamval='bijz'|AttVal],AttVal).
lassy_postag_atts_('TW(rang,nom,zonder-n)',[pt='tw',numtype='rang',positie='nom','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('TW(rang,nom,mv-n)',[pt='tw',numtype='rang',positie='nom','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,vol,1,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='vol',persoon='1',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,nadr,1,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='nadr',persoon='1',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,red,1,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='red',persoon='1',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,vol,1,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='vol',persoon='1',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,nadr,1,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='nadr',persoon='1',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,red,1,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='red',persoon='1',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,vol,2v,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='vol',persoon='2v',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,nadr,2v,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='nadr',persoon='2v',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,red,2v,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='red',persoon='2v',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,nadr,3m,ev,masc)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='nadr',persoon='3m',getal='ev',genus='masc'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,vol,3v,ev,fem)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='vol',persoon='3v',getal='ev',genus='fem'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,nadr,3v,ev,fem)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='nadr',persoon='3v',getal='ev',genus='fem'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,vol,2v,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='vol',persoon='2v',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,nadr,3m,ev,masc)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='nadr',persoon='3m',getal='ev',genus='masc'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,gen,vol,1,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='gen',status='vol',persoon='1',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,gen,vol,1,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='gen',status='vol',persoon='1',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,gen,vol,3m,ev)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='gen',status='vol',persoon='3m',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,1,ev,prenom,zonder,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='1',getal='ev',positie='prenom',buiging='zonder',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,1,mv,prenom,met-e,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='1',getal='mv',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,3v,ev,prenom,zonder,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='3v',getal='ev',positie='prenom',buiging='zonder',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,1,ev,prenom,met-e,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='1',getal='ev',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,1,ev,prenom,met-e,evf)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='1',getal='ev',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,1,mv,prenom,met-e,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='1',getal='mv',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,1,mv,prenom,met-e,evf)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='1',getal='mv',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,2v,ev,prenom,met-e,evf)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='2v',getal='ev',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3v,ev,prenom,met-e,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3v',getal='ev',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3v,ev,prenom,met-e,evf)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3v',getal='ev',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,1,ev,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='1',getal='ev',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,1,mv,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='1',getal='mv',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3m,ev,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3m',getal='ev',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3v,ev,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3v',getal='ev',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(betr,pron,gen,vol,3o,ev)',[pt='vnw',vwtype='betr',pdtype='pron',naamval='gen',status='vol',persoon='3o',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,pron,gen,vol,3m,ev)',[pt='vnw',vwtype='aanw',pdtype='pron',naamval='gen',status='vol',persoon='3m',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,pron,gen,vol,3o,ev)',[pt='vnw',vwtype='aanw',pdtype='pron',naamval='gen',status='vol',persoon='3o',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,dat,prenom,met-e,evmo)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='dat',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,dat,prenom,met-e,evf)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='dat',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,gen,nom,met-e,zonder-n)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='gen',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,dat,nom,met-e,zonder-n)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='dat',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,gen,prenom,met-e,mv)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='gen',positie='prenom',buiging='met-e',npagr='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,dat,prenom,met-e,evmo)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='dat',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,dat,prenom,met-e,evf)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='dat',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,gen,nom,met-e,mv-n)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='gen',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,gen,nom,met-e,mv-n,basis)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='gen',positie='nom',buiging='met-e','getal-n'='mv-n',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('LID(bep,stan,evon)',[pt='lid',lwtype='bep',naamval='stan',npagr='evon'|AttVal],AttVal).
lassy_postag_atts_('LID(bep,stan,rest)',[pt='lid',lwtype='bep',naamval='stan',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('LID(bep,gen,evmo)',[pt='lid',lwtype='bep',naamval='gen',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('LID(bep,dat,evmo)',[pt='lid',lwtype='bep',naamval='dat',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('LID(bep,dat,evf)',[pt='lid',lwtype='bep',naamval='dat',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('LID(bep,dat,mv)',[pt='lid',lwtype='bep',naamval='dat',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('LID(onbep,gen,evf)',[pt='lid',lwtype='onbep',naamval='gen',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VZ(init)',[pt='vz',vztype='init'|AttVal],AttVal).
lassy_postag_atts_('VZ(fin)',[pt='vz',vztype='fin'|AttVal],AttVal).
lassy_postag_atts_('VZ(versm)',[pt='vz',vztype='versm'|AttVal],AttVal).
lassy_postag_atts_('VG(neven)',[pt='vg',conjtype='neven'|AttVal],AttVal).
lassy_postag_atts_('VG(onder)',[pt='vg',conjtype='onder'|AttVal],AttVal).
lassy_postag_atts_('BW()',[pt='bw'|AttVal],AttVal).
lassy_postag_atts_('N(soort,ev,basis,genus,stan)',[pt='n',ntype='soort',getal='ev',graad='basis',genus='genus',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('N(eigen,ev,basis,genus,stan)',[pt='n',ntype='eigen',getal='ev',graad='basis',genus='genus',naamval='stan'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,vol,2b,getal)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='vol',persoon='2b',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,nadr,2b,getal)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='nadr',persoon='2b',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,vol,2,getal)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='vol',persoon='2',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,nadr,2,getal)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='nadr',persoon='2',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,red,2,getal)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='red',persoon='2',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,vol,3,ev,masc)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='vol',persoon='3',getal='ev',genus='masc'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,red,3,ev,masc)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='red',persoon='3',getal='ev',genus='masc'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,red,3p,ev,masc)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='red',persoon='3p',getal='ev',genus='masc'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,vol,3p,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='vol',persoon='3p',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,nomin,nadr,3p,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='nomin',status='nadr',persoon='3p',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,vol,3,ev,masc)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='vol',persoon='3',getal='ev',genus='masc'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,red,3,ev,masc)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='red',persoon='3',getal='ev',genus='masc'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,vol,3,getal,fem)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='vol',persoon='3',getal='getal',genus='fem'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,nadr,3v,getal,fem)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='nadr',persoon='3v',getal='getal',genus='fem'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,red,3v,getal,fem)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='red',persoon='3v',getal='getal',genus='fem'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,vol,3p,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='vol',persoon='3p',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,obl,nadr,3p,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='obl',status='nadr',persoon='3p',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,stan,nadr,2v,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='stan',status='nadr',persoon='2v',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,stan,red,3,ev,onz)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='stan',status='red',persoon='3',getal='ev',genus='onz'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,stan,red,3,ev,fem)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='stan',status='red',persoon='3',getal='ev',genus='fem'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,stan,red,3,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='stan',status='red',persoon='3',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,gen,vol,2,getal)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='gen',status='vol',persoon='2',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,gen,vol,3v,getal)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='gen',status='vol',persoon='3v',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pers,pron,gen,vol,3p,mv)',[pt='vnw',vwtype='pers',pdtype='pron',naamval='gen',status='vol',persoon='3p',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,vol,1,ev)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='vol',persoon='1',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,nadr,1,ev)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='nadr',persoon='1',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,red,1,ev)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='red',persoon='1',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,vol,1,mv)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='vol',persoon='1',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,nadr,1,mv)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='nadr',persoon='1',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,red,2v,getal)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='red',persoon='2v',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,nadr,2v,getal)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='nadr',persoon='2v',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,vol,2,getal)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='vol',persoon='2',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(pr,pron,obl,nadr,2,getal)',[pt='vnw',vwtype='pr',pdtype='pron',naamval='obl',status='nadr',persoon='2',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(refl,pron,obl,red,3,getal)',[pt='vnw',vwtype='refl',pdtype='pron',naamval='obl',status='red',persoon='3',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(refl,pron,obl,nadr,3,getal)',[pt='vnw',vwtype='refl',pdtype='pron',naamval='obl',status='nadr',persoon='3',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(recip,pron,obl,vol,persoon,mv)',[pt='vnw',vwtype='recip',pdtype='pron',naamval='obl',status='vol',persoon='persoon',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(recip,pron,gen,vol,persoon,mv)',[pt='vnw',vwtype='recip',pdtype='pron',naamval='gen',status='vol',persoon='persoon',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,ev,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='ev',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,ev,prenom,met-e,rest)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='ev',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,red,1,ev,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='red',persoon='1',getal='ev',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,mv,prenom,zonder,evon)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='mv',positie='prenom',buiging='zonder',npagr='evon'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,mv,prenom,met-e,rest)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='mv',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,2,getal,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='2',getal='getal',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,2,getal,prenom,met-e,rest)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='2',getal='getal',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,2v,ev,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='2v',getal='ev',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,red,2v,ev,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='red',persoon='2v',getal='ev',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,nadr,2v,mv,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='nadr',persoon='2v',getal='mv',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3,ev,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3',getal='ev',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3m,ev,prenom,met-e,rest)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3m',getal='ev',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3v,ev,prenom,met-e,rest)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3v',getal='ev',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,red,3,ev,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='red',persoon='3',getal='ev',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3,mv,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3',getal='mv',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3p,mv,prenom,met-e,rest)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3p',getal='mv',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,red,3,getal,prenom,zonder,agr)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='red',persoon='3',getal='getal',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,1,ev,prenom,met-e,rest3)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='1',getal='ev',positie='prenom',buiging='met-e',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,1,mv,prenom,met-e,rest3)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='1',getal='mv',positie='prenom',buiging='met-e',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,2,getal,prenom,zonder,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='2',getal='getal',positie='prenom',buiging='zonder',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,2,getal,prenom,met-e,rest3)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='2',getal='getal',positie='prenom',buiging='met-e',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,2v,ev,prenom,met-e,rest3)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='2v',getal='ev',positie='prenom',buiging='met-e',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,3,ev,prenom,zonder,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='3',getal='ev',positie='prenom',buiging='zonder',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,3,ev,prenom,met-e,rest3)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='3',getal='ev',positie='prenom',buiging='met-e',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,3v,ev,prenom,met-e,rest3)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='3v',getal='ev',positie='prenom',buiging='met-e',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,3p,mv,prenom,zonder,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='3p',getal='mv',positie='prenom',buiging='zonder',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,gen,vol,3p,mv,prenom,met-e,rest3)',[pt='vnw',vwtype='bez',pdtype='det',naamval='gen',status='vol',persoon='3p',getal='mv',positie='prenom',buiging='met-e',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,2,getal,prenom,met-e,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='2',getal='getal',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,2,getal,prenom,met-e,evf)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='2',getal='getal',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3,ev,prenom,met-e,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3',getal='ev',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3,ev,prenom,met-e,evf)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3',getal='ev',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3p,mv,prenom,met-e,evmo)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3p',getal='mv',positie='prenom',buiging='met-e',npagr='evmo'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3p,mv,prenom,met-e,evf)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3p',getal='mv',positie='prenom',buiging='met-e',npagr='evf'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,ev,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='ev',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,mv,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='mv',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,2,getal,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='2',getal='getal',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,2v,ev,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='2v',getal='ev',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3m,ev,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3m',getal='ev',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3v,ev,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3v',getal='ev',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3p,mv,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3p',getal='mv',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,ev,nom,met-e,mv-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='ev',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,mv,nom,met-e,mv-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='mv',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,1,mv,nom,met-e,getal-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='1',getal='mv',positie='nom',buiging='met-e','getal-n'='getal-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,2,getal,nom,met-e,mv-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='2',getal='getal',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,2v,ev,nom,met-e,mv-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='2v',getal='ev',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3m,ev,nom,met-e,mv-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3m',getal='ev',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3v,ev,nom,met-e,mv-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3v',getal='ev',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,stan,vol,3p,mv,nom,met-e,mv-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='stan',status='vol',persoon='3p',getal='mv',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,2,getal,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='2',getal='getal',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(bez,det,dat,vol,3p,mv,nom,met-e,zonder-n)',[pt='vnw',vwtype='bez',pdtype='det',naamval='dat',status='vol',persoon='3p',getal='mv',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(vrag,pron,stan,nadr,3o,ev)',[pt='vnw',vwtype='vrag',pdtype='pron',naamval='stan',status='nadr',persoon='3o',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(betr,pron,stan,vol,persoon,getal)',[pt='vnw',vwtype='betr',pdtype='pron',naamval='stan',status='vol',persoon='persoon',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(betr,pron,stan,vol,3,ev)',[pt='vnw',vwtype='betr',pdtype='pron',naamval='stan',status='vol',persoon='3',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(betr,det,stan,nom,zonder,zonder-n)',[pt='vnw',vwtype='betr',pdtype='det',naamval='stan',positie='nom',buiging='zonder','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(betr,det,stan,nom,met-e,zonder-n)',[pt='vnw',vwtype='betr',pdtype='det',naamval='stan',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(betr,pron,gen,vol,3o,getal)',[pt='vnw',vwtype='betr',pdtype='pron',naamval='gen',status='vol',persoon='3o',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,pron,stan,vol,3p,getal)',[pt='vnw',vwtype='vb',pdtype='pron',naamval='stan',status='vol',persoon='3p',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,pron,stan,vol,3o,ev)',[pt='vnw',vwtype='vb',pdtype='pron',naamval='stan',status='vol',persoon='3o',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,pron,gen,vol,3m,ev)',[pt='vnw',vwtype='vb',pdtype='pron',naamval='gen',status='vol',persoon='3m',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,pron,gen,vol,3v,ev)',[pt='vnw',vwtype='vb',pdtype='pron',naamval='gen',status='vol',persoon='3v',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,pron,gen,vol,3p,mv)',[pt='vnw',vwtype='vb',pdtype='pron',naamval='gen',status='vol',persoon='3p',getal='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,adv-pron,obl,vol,3o,getal)',[pt='vnw',vwtype='vb',pdtype='adv-pron',naamval='obl',status='vol',persoon='3o',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(excl,pron,stan,vol,3,getal)',[pt='vnw',vwtype='excl',pdtype='pron',naamval='stan',status='vol',persoon='3',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,det,stan,prenom,zonder,evon)',[pt='vnw',vwtype='vb',pdtype='det',naamval='stan',positie='prenom',buiging='zonder',npagr='evon'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,det,stan,prenom,met-e,rest)',[pt='vnw',vwtype='vb',pdtype='det',naamval='stan',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(vb,det,stan,nom,met-e,zonder-n)',[pt='vnw',vwtype='vb',pdtype='det',naamval='stan',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(excl,det,stan,vrij,zonder)',[pt='vnw',vwtype='excl',pdtype='det',naamval='stan',positie='vrij',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,pron,stan,vol,3o,ev)',[pt='vnw',vwtype='aanw',pdtype='pron',naamval='stan',status='vol',persoon='3o',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,pron,stan,nadr,3o,ev)',[pt='vnw',vwtype='aanw',pdtype='pron',naamval='stan',status='nadr',persoon='3o',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,pron,stan,vol,3,getal)',[pt='vnw',vwtype='aanw',pdtype='pron',naamval='stan',status='vol',persoon='3',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,adv-pron,obl,vol,3o,getal)',[pt='vnw',vwtype='aanw',pdtype='adv-pron',naamval='obl',status='vol',persoon='3o',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,adv-pron,stan,red,3,getal)',[pt='vnw',vwtype='aanw',pdtype='adv-pron',naamval='stan',status='red',persoon='3',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,stan,prenom,zonder,evon)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='stan',positie='prenom',buiging='zonder',npagr='evon'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,stan,prenom,zonder,rest)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='stan',positie='prenom',buiging='zonder',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,stan,prenom,zonder,agr)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='stan',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,stan,prenom,met-e,rest)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='stan',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,gen,prenom,met-e,rest3)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='gen',positie='prenom',buiging='met-e',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,stan,nom,met-e,zonder-n)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='stan',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,stan,nom,met-e,mv-n)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='stan',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(aanw,det,stan,vrij,zonder)',[pt='vnw',vwtype='aanw',pdtype='det',naamval='stan',positie='vrij',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,pron,stan,vol,3p,ev)',[pt='vnw',vwtype='onbep',pdtype='pron',naamval='stan',status='vol',persoon='3p',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,pron,stan,vol,3o,ev)',[pt='vnw',vwtype='onbep',pdtype='pron',naamval='stan',status='vol',persoon='3o',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,pron,gen,vol,3p,ev)',[pt='vnw',vwtype='onbep',pdtype='pron',naamval='gen',status='vol',persoon='3p',getal='ev'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,adv-pron,obl,vol,3o,getal)',[pt='vnw',vwtype='onbep',pdtype='adv-pron',naamval='obl',status='vol',persoon='3o',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,adv-pron,gen,red,3,getal)',[pt='vnw',vwtype='onbep',pdtype='adv-pron',naamval='gen',status='red',persoon='3',getal='getal'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,prenom,zonder,evon)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='prenom',buiging='zonder',npagr='evon'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,prenom,zonder,agr)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='prenom',buiging='zonder',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,prenom,met-e,evz)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='prenom',buiging='met-e',npagr='evz'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,prenom,met-e,mv)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='prenom',buiging='met-e',npagr='mv'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,prenom,met-e,rest)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='prenom',buiging='met-e',npagr='rest'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,prenom,met-e,agr)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='prenom',buiging='met-e',npagr='agr'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,prenom,zonder,agr,basis)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='prenom',buiging='zonder',npagr='agr',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,prenom,met-e,agr,basis)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='prenom',buiging='met-e',npagr='agr',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,prenom,met-e,mv,basis)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='prenom',buiging='met-e',getal='mv',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,prenom,zonder,agr,comp)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='prenom',buiging='zonder',npagr='agr',graad='comp'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,prenom,met-e,agr,sup)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='prenom',buiging='met-e',npagr='agr',graad='sup'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,prenom,met-e,agr,comp)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='prenom',buiging='met-e',npagr='agr',graad='comp'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,nom,met-e,mv-n)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='nom',buiging='met-e','getal-n'='mv-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,nom,met-e,zonder-n)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='nom',buiging='met-e','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,nom,zonder,zonder-n)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='nom',buiging='zonder','getal-n'='zonder-n'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,nom,met-e,zonder-n,basis)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='nom',buiging='met-e','getal-n'='zonder-n',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,nom,met-e,mv-n,basis)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='nom',buiging='met-e','getal-n'='mv-n',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,nom,met-e,zonder-n,sup)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='nom',buiging='met-e','getal-n'='zonder-n',graad='sup'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,nom,met-e,mv-n,sup)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='nom',buiging='met-e','getal-n'='mv-n',graad='sup'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,nom,zonder,mv-n,dim)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='nom',buiging='zonder','getal-n'='mv-n',graad='dim'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,det,stan,vrij,zonder)',[pt='vnw',vwtype='onbep',pdtype='det',naamval='stan',positie='vrij',buiging='zonder'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,vrij,zonder,basis)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='vrij',buiging='zonder',graad='basis'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,vrij,zonder,sup)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='vrij',buiging='zonder',graad='sup'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,vrij,zonder,comp)',[pt='vnw',vwtype='onbep',pdtype='grad',naamval='stan',positie='vrij',buiging='zonder',graad='comp'|AttVal],AttVal).
lassy_postag_atts_('LID(bep,gen,rest3)',[pt='lid',lwtype='bep',naamval='gen',npagr='rest3'|AttVal],AttVal).
lassy_postag_atts_('LID(onbep,stan,agr)',[pt='lid',lwtype='onbep',naamval='stan',npagr='agr'|AttVal],AttVal).

%% later additions for LASSY Small
lassy_postag_atts_('SPEC(enof)',[pt='spec',spectype='enof'|AttVal],AttVal).
lassy_postag_atts_('VNW(onbep,grad,stan,nom,zonder,zonder-n,sup)',[pt=vnw,vwtype=onbep,pdtype=grad,naamval=stan,positie=nom,buiging=zonder,'getal-n'='zonder-n',graad=sup|AttVal],AttVal).

%% CGN lemma

%% verb: sg1 -> inf
%% adj/psp:  -> inf
%% remove _DIM suffix
%% remove _ compound border, reintroduce -s if necc (how?)
%% word = pos-, add '-' add the end
%% "inflected" determiners/pronouns alle -> al, hoeveelste -> hoeveel, ...